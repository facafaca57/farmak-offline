import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: "com.farmakrx2.app",
  appName: "FarmakRX2",
  bundledWebRuntime: false,
  webDir: "dist",
  android: {
    buildOptions: {
      releaseType: 'APK',
      keystorePath: '/home/andy/Android/farmak-keystore.jks',
      keystoreAlias: 'FarmakRX2',
      keystoreAliasPassword: 'farmakrx2',
      keystorePassword: 'farmakrx2'
    },
  },
  plugins: {
    SplashScreen: {
      launchShowDuration: 1000,
      launchAutoHide: true,
      backgroundColor: '#ffffffff',
      androidSplashResourceName: 'splash',
      androidScaleType: 'CENTER_CROP',
      showSpinner: true,
      androidSpinnerStyle: 'large',
      iosSpinnerStyle: 'small',
      spinnerColor: '#999999',
      splashFullScreen: true,
      splashImmersive: true,
      layoutName: 'launch_screen',
      useDialog: true
    },
    // LocalNotifications: {
    //   iconColor: "#488AFF",
    // },
  }
};

export default config;
