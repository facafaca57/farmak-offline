// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    apiUrl: 'https://farmak-rx2.com/api',
    publicFolder: 'https://farmak-rx2.com/public',
    uploadsImg: 'https://farmak-rx2.com/public/uploads/img/',
    uploadsPdf: 'https://farmak-rx2.com/public/uploads/pdf/',
    socketUrl: 'https://farmak-rx2.com',
    version: '1.0.171',
};

// export const environment = {
//     production: false,
//     apiUrl: 'http://localhost:3000/api',

//     // publicFolder: 'http://localhost:3000/public',
//     publicFolder: 'https://farmak-rx2.com/public',
//     // publicFolder: '../../../assets',

//     // uploadsImg: 'http://localhost:3000/public/uploads/img/',
//     uploadsImg: 'https://farmak-rx2.com/public/uploads/img/',

//     // uploadsPdf: 'http://localhost:3000/public/uploads/pdf/',
//     uploadsPdf: 'https://farmak-rx2.com/public/uploads/pdf/',
    
//     socketUrl: 'http://localhost:3000',
//     version: '0.0.1',
// };


// export const environment = {
//     production: true,
//     apiUrl: '/api',
//     publicFolder: '/public',
//     uploadsImg: '/public/uploads/img/',
//     uploadsPdf: '/public/uploads/pdf/',
//     socketUrl: '/',
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
