export const environment = {
    production: true,
    apiUrl: 'https://farmak-rx2.com/api',
    publicFolder: 'https://farmak-rx2.com/public',
    uploadsImg: 'https://farmak-rx2.com/public/uploads/img/',
    uploadsPdf: 'https://farmak-rx2.com/public/uploads/pdf/',
    socketUrl: 'https://farmak-rx2.com',
    version: '1.0.171',
};

// export const environment = {
//     production: false,
//     apiUrl: 'http://localhost:3000/api',
//     publicFolder: 'https://www.farmak-rx2.com/public',
//     uploadsImg: 'https://www.farmak-rx2.com/public/uploads/img/',
//     uploadsPdf: '/public/uploads/pdf/',
//     socketUrl: 'http://localhost:3000',
// };
