﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@capacitor/splash-screen';
import { App } from '@capacitor/app';
import { AuthenticationService, UpdateService } from './_services';
import { User } from './_models';
import { IonRouterOutlet, NavController, Platform } from '@ionic/angular';
import { pagesAnimation } from './_configs/appPagesAnimations';

@Component({
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    currentUser: User;
    lastroute: string[] = [];
    @ViewChild(IonRouterOutlet, { static : true }) routerOutlet: IonRouterOutlet;

    public get new_app_version() {
      return localStorage.getItem('new_app_version') ?? null
    };

    get canGoBack() {
      return this.router.url != '/';
    }

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private update: UpdateService,
        private navCtrl: NavController,
        private platform: Platform
    ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
      // this.authenticationService.network.then(({connected}) => {
      //   if (connected && this.currentUser) {
      //     this.update.checkUpdates();
      //   }
      // });
      // App.addListener('appStateChange', ({ isActive }) => {
      //   if (isActive) {
      //     this.authenticationService.network.then(({connected}) => {
      //       if (connected && this.currentUser) {
      //         this.update.checkUpdates();
      //       }
      //     });
      //   }
      // });

      App.getInfo().then(({ version }) => {
        localStorage.setItem('app_version', version);
      });
    }

    initializeApp() {
      this.platform.ready().then(() => {
        SplashScreen.hide();
      });
    }

    goBack() {
      if(this.routerOutlet.canGoBack()) {
        this.navCtrl.back({ animation: pagesAnimation });
      } else {
        this.navCtrl.navigateRoot('/', { animation: pagesAnimation });
      }
    }

    updateAPP(){
      this.update.getApp();
    }

    checkUpdates(){
      this.update.checkUpdates();
    }

    ngOnInit() {}

    logout() {
      this.authenticationService.logout().subscribe(res => {
        this.router.navigate(['/login'], { replaceUrl: true });
      });
    }
}
