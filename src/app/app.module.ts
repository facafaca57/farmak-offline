﻿import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppComponent } from './app.component';
import { environment } from '@environments/environment';
import { appRoutingModule } from './app.routing';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';

import { HomeComponent } from './pages/home';
import { LoginComponent } from './pages/login';
import { ControlComponent } from './pages/control/';

import { ServiceWorkerModule } from '@angular/service-worker';

import { CarouselModule, NavbarModule, WavesModule, ModalModule, CollapseModule } from 'angular-bootstrap-md';

import {
    UserService, AlertService, VisitService,
    PresentationService, LoyalityPresentationService,
    SpecialtyService, PaperService, SocketService,
    PreparatsService, QuestionService, marketerAcademyService, 
    UserActivityServiceService, StorageService, UpdateService, NotificationService
} from '@app/_services';

import { UsersComponent } from './pages/users';
import { AlertComponent } from './_directives';
import { VisitsComponent } from './pages/visits/visits.component';
import { PresentationsComponent } from './pages/presentations/presentations.component';
import { SpecialtiesComponent } from './pages/specialties/specialties.component';
import { PapersComponent } from './pages/papers/papers.component';
import { EditPaperComponent } from './pages/edit-paper/edit-paper.component';
import { MaterialsComponent } from './pages/materials/materials.component';
import { SpecialtyComponent } from './pages/specialty/specialty.component';
import { PresentationComponent } from './pages/presentation/presentation.component';
import { EditPresentationComponent } from './pages/edit-presentation/edit-presentation.component';
import { ModalComponent } from './shared/modal/modal.component';
import { SearchFilterPipe, SortPipe, SearchUserPipe } from './_pipes';

import { PdfViewerModule } from 'ng2-pdf-viewer';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { EditVisitComponent } from './pages/edit-visit/edit-visit.component';
import { NguCarouselModule } from '@ngu/carousel';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { StudyComponent } from './pages/study/study.component';
import { TestsComponent } from './pages/tests/tests.component';
import { StudiesComponent } from './pages/studies/studies.component';
import { TestsResultsComponent } from './pages/tests-results/tests-results.component';

import { EditStudyComponent } from './pages/edit-study/edit-study.component';
import {EditUserComponent} from './pages/edit-user/edit-user.component';
import { ResultsComponent } from './pages/results/results.component';
import { EducationalMaterialsComponent } from './pages/educational-materials/educational-materials.component';
import { EducationalMaterialComponent } from './pages/educational-material/educational-material.component';
import { EditSpecialtyComponent } from './pages/edit-specialty/edit-specialty.component';
import { SingleTestComponent } from './pages/single-test/single-test.component';
import { PreparatsComponent } from './pages/preparats/preparats.component';
import { PreparatComponent } from './pages/preparat/preparat.component';
import { EditPreparatsComponent } from './pages/edit-preparats/edit-preparats.component';
import { QuestionsComponent } from './pages/questions/questions.component';
import { EditQuestionComponent } from './pages/edit-question/edit-question.component';
import { QuestionPageComponent } from './pages/question-page/question-page.component';
import { QuestionContentComponent } from './pages/question-content/question-content.component';
import { ExplanationsToTheCycleComponent } from './pages/explanations-to-the-cycle/explanations-to-the-cycle.component';
import { HistoryComponent } from './pages/history/history.component';
import { HistoryElementComponent } from './pages/history-element/history-element.component';
import { HistoryPresentationComponent } from './pages/history-presentation/history-presentation.component';
import { UserActivityComponent } from './pages/user-activity/user-activity.component';
import { LoyaltyRogramsComponent } from './pages/loyalty-rograms/loyalty-rograms.component';
import { LoyaltyAdministrateComponent } from './pages/loyalty-administrate/loyalty-administrate.component';
import { LoyaltyAdministrateEditComponent } from './pages/loyalty-administrate-edit/loyalty-administrate-edit.component';
import { LoyalityPresentationComponent } from './pages/loyality-presentation/loyality-presentation.component';
import { MarketingCommentsComponent } from './pages/marketing-comments/marketing-comments.component';
import { MarketingCommentsToPreparatComponent } from './pages/marketing-comments-to-preparat/marketing-comments-to-preparat.component';
import { MarketingCommentsToPreparatEditComponent } from './pages/marketing-comments-to-preparat-edit/marketing-comments-to-preparat-edit.component';
import { ControlMarketingCommentsComponent } from './pages/control-marketing-comments/control-marketing-comments.component';
import { ControlMarketerAcademyComponent } from './pages/control-marketer-academy/control-marketer-academy.component';
import { EditMarketerAcademyComponent } from './pages/edit-marketer-academy/edit-marketer-academy.component';
import { MarketerAcademyComponent } from './pages/marketer-academy/marketer-academy.component';
import { MarketerAcademyPreparatComponent } from './pages/marketer-academy-preparat/marketer-academy-preparat.component';
import { MarketerAcademyPresentationComponent } from './pages/marketer-academy-presentation/marketer-academy-presentation.component'
import { ResponseInterceptor } from './_helpers/response.interceptor';;
import { ToimgPipe } from './_pipes/toimg.pipe';
import { Drivers } from '@ionic/storage';
import { IonicStorageModule } from '@ionic/storage-angular';
import { IonicModule } from '@ionic/angular';
// import { LocalNotifications} from '@ionic-native/local-notifications/ngx'
import { File } from '@ionic-native/file/ngx';
import { HTTP } from '@ionic-native/http/ngx';
// import { PinchZoomModule } from '@meddv/ngx-pinch-zoom';
import { SwiperDirective, ChangeImageDirective, VideoDirective } from './_directives';
import { FilesService } from './_services/files.service';
import { LocalImagePipe } from './_pipes/localImage.pipe';
import { pagesAnimation } from './_configs/appPagesAnimations';
import { UpdatesComponent } from './pages/updates/updates.component';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        appRoutingModule,
        CarouselModule,
        WavesModule,
        CarouselModule.forRoot(),
        WavesModule.forRoot(),
        ModalModule.forRoot(),
        NavbarModule,
        CollapseModule,
        PdfViewerModule,
        DragDropModule,
        CdkAccordionModule,
        NguCarouselModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.production,
            registrationStrategy: 'registerImmediately'
        }),
        YouTubePlayerModule,
        IonicModule.forRoot({ navAnimation: pagesAnimation }), //animated: false,
        IonicStorageModule.forRoot({
            name: 'farmak_db',
            driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
        }), 
        // PinchZoomModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        ControlComponent,
        UsersComponent ,
        AlertComponent ,
        VisitsComponent ,
        PresentationsComponent ,
        SpecialtiesComponent,
        PapersComponent,
        MaterialsComponent,
        SpecialtyComponent,
        PresentationComponent,
        EditPresentationComponent,
        SearchFilterPipe,
        SearchUserPipe,
        SortPipe, UpdatesComponent,
        ModalComponent,
        EditVisitComponent,
        StudyComponent ,
        TestsComponent ,
        StudiesComponent ,
        TestsResultsComponent,
        EditStudyComponent,
        EditUserComponent,
        ResultsComponent,
        EducationalMaterialsComponent,
        EducationalMaterialComponent,
        EditSpecialtyComponent,
        SingleTestComponent,
        PreparatsComponent,
        PreparatComponent ,
        EditPreparatsComponent ,
        QuestionsComponent ,
        EditQuestionComponent ,
        QuestionPageComponent,
        QuestionContentComponent,
        ExplanationsToTheCycleComponent,
        HistoryComponent,
        HistoryElementComponent,
        HistoryPresentationComponent,
        EditPaperComponent,
        UserActivityComponent,
        LoyaltyRogramsComponent ,
        LoyaltyAdministrateComponent ,
        LoyaltyAdministrateEditComponent ,
        LoyalityPresentationComponent ,
        MarketingCommentsComponent,
        MarketingCommentsToPreparatComponent,
        MarketingCommentsToPreparatEditComponent,
        ControlMarketingCommentsComponent,
        ControlMarketerAcademyComponent ,
        EditMarketerAcademyComponent ,
        MarketerAcademyComponent ,
        MarketerAcademyPreparatComponent,
        MarketerAcademyPresentationComponent,
        ToimgPipe, LocalImagePipe,
        SwiperDirective, ChangeImageDirective, VideoDirective
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true },
        UserService,
        AlertService,
        VisitService,
        PresentationService,
        LoyalityPresentationService,
        SpecialtyService,
        PaperService,
        SocketService,
        PreparatsService,
        QuestionService,
        marketerAcademyService,
        StorageService,
        UpdateService,
        NotificationService,
        File, HTTP,
        FilesService,
        // LocalNotifications
    ],
    bootstrap: [AppComponent],
    exports: [ ToimgPipe ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
