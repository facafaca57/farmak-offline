import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {marketerAcademy, Preparats} from "@app/_models";
import {Router} from "@angular/router";
import {AlertService, marketerAcademyService, PreparatsService} from "@app/_services";
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-marketer-academy',
  templateUrl: './control-marketer-academy.component.html',
  styleUrls: ['./control-marketer-academy.component.scss']
})
export class ControlMarketerAcademyComponent implements OnInit {
    public form: FormGroup;
    public slideList: FormArray;
    loading = false;
    marketerAcademys: marketerAcademy[] = [];
    isData: boolean = false;
    selectedPreparat: string;
    preparats: Preparats[] = [];
    files = [];
    uploadsImg = environment.uploadsImg;
    uploadsPdf = environment.uploadsPdf;
    queryString: string;
    types = ['Slider', 'File'];
    type: string = 'Slider';

    constructor(
        private router: Router,
        private marketerAcademyService: marketerAcademyService,
        private preparatService: PreparatsService,
        private fb: FormBuilder,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.loadInitialData();
        this.loadAllData();

        this.form = this.fb.group({
            title: ['', [Validators.required]],
            preparat: [null],
            type: [''],
            imgs: [null],
            slides: this.fb.array([this.createSlide()])
        });

        this.slideList = this.form.get('slides') as FormArray;
    }

    private loadInitialData() {
        this.preparatService.getAll()
            .subscribe(data => {
                this.preparats = data;
                this.selectedPreparat = data[0]['id'];
            });
    }

    private loadAllData() {
        this.marketerAcademyService.getAll()
            .subscribe(data => {
                this.marketerAcademys = data;
                this.isData = true;
            });
    }

    get slideFormGroup() {
        return this.form.get('slides') as FormArray;
    }

    createSlide(): FormGroup {
        return this.fb.group({
            img: [null],
            link: [null],
            linkCenter: [null],
            linkBottom: [null],
            linkBHL: [null],
            linkBHR: [null],
            linkBL: [null],
            linkBC: [null],
            linkBR: [null],
            textLeft: [null],
            textRight: [null],
            dateCountDown: [null]
        });
    }

    addSlide() {
        this.slideList.push(this.createSlide());
    }

    removeSlide(index) {
        this.slideList.removeAt(index);
    }

    getSlidesFormGroup(index): FormGroup {
        const formGroup = this.slideList.controls[index] as FormGroup;
        return formGroup;
    }


    onFileSelect(event, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.files[index] = file;
        }
    }


    public selectChangeHandler(event: any, selectedVal) {
        this[selectedVal] = event.target.value;
    }


    public delete(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.marketerAcademyService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Data successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    drop(event: CdkDragDrop<string[]>, index) {
        if (event.previousIndex !== event.currentIndex) {
            let slides:any = this.marketerAcademys[index]['slides'];
            let marketerAcademy:marketerAcademy = this.marketerAcademys[index];

            moveItemInArray(slides, event.previousIndex, event.currentIndex);

            const slidesSwaped = [...slides];

            const formData: any = new FormData();
            formData.append('title', marketerAcademy.title);
            formData.append('preparat', +marketerAcademy.preparat);
            formData.append('slides', JSON.stringify(slidesSwaped));

            this.loading = true;
            this.marketerAcademyService.update(formData, this.marketerAcademys[index]['id'])
                .subscribe(
                    data => {
                        if (data.id) {
                            this.alertService.success('Action was successfully completed.');
                        } else {
                            this.alertService.error('Error. No response from backend.');
                        }
                        this.loading = false;
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    });
        }
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const formData: any = new FormData();
        formData.append('title', form.controls.title.value);
        formData.append('preparat', this.selectedPreparat);
        formData.append('type', this.type);

        const slides = form.controls.slides.value;
        const sidesReq = [];
        slides.map((slide, i) => {
            sidesReq[i] = {
                'link': slide.link,
                'linkCenter': slide.linkCenter,
                'linkBottom': slide.linkBottom,
                'linkBHL': slide.linkBHL,
                'linkBHR': slide.linkBHR,
                'linkBL': slide.linkBL,
                'linkBC': slide.linkBC,
                'linkBR': slide.linkBR,
                'textLeft': slide.textLeft,
                'textRight': slide.textRight,
                'dateCountDown': slide.dateCountDown
            };
        });

        formData.append(`slides`, JSON.stringify(sidesReq));

        for (const file of this.files) {
            console.log(file, file['name']);
            formData.append(`imgs`, file, file['name']);

        }

        this.loading = true;
        this.marketerAcademyService.create(formData)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.form.reset();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
    public changeType(event: any) {
        this.type = event.target.value;
    }
}
