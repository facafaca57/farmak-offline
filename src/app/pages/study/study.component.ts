import {Component, OnDestroy, OnInit, ViewChild, AfterViewInit, ElementRef, HostListener} from '@angular/core';
import {Subscription} from 'rxjs';
import {debounce, isEmpty} from 'lodash';
import {AuthenticationService, SocketService, StudyService, TestsResultsService, UserService, UserActivityServiceService} from '@app/_services';
import {Study, Test, User} from '@app/_models';
import * as moment from 'moment';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';


const ACTIVITY_MODAL_TIME = 3 * 60 * 1000; // 3 * 60 * 1000
const TODAY = moment(new Date()).format('DD-MM-YYYY');
const IS_USER_ACTIVE_TIME = 15 * 60 * 1000; // 15 * 60 * 1000
const FIRST_BANNER_TIME = 5 * 60 * 1000;


@Component({
  selector: 'app-study',
  templateUrl: './study.component.html',
  styleUrls: ['./study.component.scss']
})
export class StudyComponent implements OnInit, OnDestroy, AfterViewInit {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @ViewChild('basicModal', { static: false }) basicModal: any;
    @ViewChild('forceVideoModal', { static: false }) forceVideoModal: any;
    @ViewChild('testCarousel', { static: false }) testCarousel: NguCarousel<any>;
    @ViewChild('videoWrap', { static: false }) videoWrap: ElementRef;

    wrongTimer = '0-1:0-1:0-1';

    ProgramState = {
        waiting: 'waiting',
        playingVideo: 'playingVideo',
        passingTest: 'passingTest',
        finished: 'finished'
    };

    carouselConfig: NguCarouselConfig = {
        grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
        interval: {timing: 0, initialDelay: 0},
        loop: false,
        touch: false,
        speed: 0,
        velocity: 0
    };

    subscription: Subscription;
    userSubscription: Subscription;
    newUserSubscription: Subscription;
    activitySubscription: Subscription;

    message: string;
    messages: string[] = [];
    users: any[] = [];
    study: Study | null = null;
    test: Test;
    tests: Test[];
    currentUser: User;

    testStartTime: number;
    prevQuestionTime: number;

    finishTestTimer;
    breakTimer;
    timer;

    activityTimer;
    dispatchIsActive = debounce(this.saveUserActivity, ACTIVITY_MODAL_TIME);

    slideIndex = 0;
    detailedStatistics = [];

    showChat = false;

    program: any[] = [];
    programIndex = 0;
    timeLeft: number;
    player: any;

    constructor(
        private socketService: SocketService,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private studyService: StudyService,
        private userActivityServiceService: UserActivityServiceService,
        private testsResultsService: TestsResultsService,
        private elementRef: ElementRef
    ) {
        const {id, username, role} = this.authenticationService.currentUserValue;
        this.currentUser = {id, username, role};

        try {
            this.socketSettings();
        } catch (e) {
            console.log(e);
        }
    }

    ngOnInit() {
        this.loadStudy();
        this.userActivityServiceService.startActivity('Сторінка навчання');
    }

    ngAfterViewInit() {
        this.forceVideoModal.show();
    }

    startVideo(vidCurrentTime = 0) {
        const offsetWidth = +this.videoWrap.nativeElement.offsetWidth - 30;
        const videoId = this.program[this.programIndex].videoInfo.url;
        this.player = new window['YT'].Player('player', {
            videoId,
            width: offsetWidth,
            height: offsetWidth / 16 * 9,
            playerVars: {
                autoplay: 0,
                modestbranding: 1,
                controls: 0,
                disablekb: 0,
                rel: 0,
                showinfo: 0,
                fs: 0,
                playsinline: 0,
                start: vidCurrentTime,
            },
            events: {
                'onStateChange': this.onPlayerStateChange.bind(this),
                'onError': this.onPlayerError.bind(this),
                'onReady': this.onPlayerReady.bind(this)
            }
        });
    }

    onPlayerReady(event) {
        event.target.playVideo();
    }

    onPlayerStateChange(event) {
        switch (event.data) {
            case window['YT'].PlayerState.PLAYING:
                if (this.cleanTime() === 0) {
                    // console.log('started ' + this.cleanTime());
                } else {
                    // console.log('playing ' + this.cleanTime());
                }
                break;
            case window['YT'].PlayerState.PAUSED:
                this.player.playVideo();
                break;
            case window['YT'].PlayerState.ENDED:
                this.programIndex++;

                this.player.destroy();
                this.player = null;

                if (this.programIndex === 2 && this.program[this.programIndex].cycle === 1) {
                    this.startBreak(+this.study.breakDuration);
                } else {
                    this.startTest();
                }
                break;
        }
    }

    cleanTime() {
        if (this.player) {
            return Math.round(this.player.getCurrentTime());
        }
    }

    onPlayerError(event) {
        console.log('break');
    }

    forceVideoPlay() {
        this.checkTimeline(this.program, this.study);
    }

    startStudy() {
        this.forceVideoModal.hide();
        this.forceVideoPlay();
        this.startShowActivityBanner();
    }

    ngOnDestroy() {
        this.socketService.disconnect(this.currentUser.id);
        if (this.subscription) {
            this.subscription.unsubscribe();
        }

        if (this.userSubscription) {
            this.userSubscription.unsubscribe();
        }

        this.activitySubscription ? this.activitySubscription.unsubscribe() : null;
        this.dispatchIsActive.cancel();

        clearInterval(this.finishTestTimer);
        clearInterval(this.breakTimer);
        clearInterval(this.activityTimer);

        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    loadStudy() {
        this.studyService.getAll()
            .subscribe(studies => {
                // const currentStudy = studies
                //     .filter(s => moment(s.startDate).format('DD-MM-YYYY') === TODAY);
                const currentStudy = studies;

                this.study = currentStudy[0] as Study;
                this.tests = this.study.tests as Test[];

                this.program = this.makeProgram(this.study, this.tests);
                this.isShowChat();
            });
    }

    checkTimeline(program = this.program, study = this.study as Study) {
        if (!isEmpty(program)) {
            const currentTime = +new Date(); // +moment(+new Date()).add(1, 'hours');

            let timeLeft;

            if (currentTime > this.program[this.program.length - 1].startAt) {
                this.programIndex = this.program.length - 1;
            } else if (currentTime < this.program[1].startAt) {
                this.programIndex = 0;
                timeLeft = this.program[1].startAt - currentTime;
            } else {
                let prevTime = 0;
                for (let i = 0; i < this.program.length; i++) {
                    if (currentTime >= prevTime && currentTime < this.program[i].startAt) {
                        this.programIndex = i - 1;
                        break;
                    }
                    prevTime = this.program[i].startAt;
                }
            }

            switch (this.program[this.programIndex].state) {
                case this.ProgramState.passingTest:


                    timeLeft = this.program[this.programIndex].startAt + this.program[this.programIndex].test.duration - currentTime;
                    if (timeLeft > 5000) {
                        this.startTest(timeLeft);
                    } else {
                        this.programIndex++;
                        timeLeft = (!timeLeft) ? this.program[this.programIndex].startAt + this.program[this.programIndex].test.breakDuration - currentTime : timeLeft;
                        this.startVideoAfterBreak(timeLeft);
                    }
                    break;
                case this.ProgramState.waiting:
                    if (this.programIndex === 2) {
                        timeLeft = (!timeLeft) ? this.program[this.programIndex].startAt + this.study.breakDuration - currentTime : timeLeft;
                    } else {
                        timeLeft = (!timeLeft) ? this.program[this.programIndex].startAt + this.program[this.programIndex].test.breakDuration - currentTime : timeLeft;
                    }

                    this.startVideoAfterBreak(timeLeft);
                    break;
                case this.ProgramState.playingVideo:
                    timeLeft = this.program[this.programIndex].startAt + this.program[this.programIndex].videoInfo.duration - currentTime;
                    this.startVideoUntilEnd(timeLeft);
                    break;
            }
        }
    }

    startShowActivityBanner() {
        // if (this.program[this.programIndex].state === this.ProgramState.playingVideo) {
        //     setTimeout(() => {
        //         if (this.program[this.programIndex].state === this.ProgramState.playingVideo && this.programIndex > 2) {
        //             this.showActivityBanner();
        //         }
        //     }, FIRST_BANNER_TIME);
        // }

        this.activityTimer = setInterval(() => {
            if (this.program[this.programIndex].state === this.ProgramState.playingVideo && this.programIndex > 1) {
                this.showActivityBanner();
            }
        }, IS_USER_ACTIVE_TIME); // IS_USER_ACTIVE_TIME
    }

    startVideoUntilEnd(timeLeft) {
        setTimeout(() => {
            const vidCurrentTime = Math.floor((this.program[this.programIndex].videoInfo.duration - timeLeft) / 1000);
            this.startVideo(vidCurrentTime);
        }, 0);
    }

    stopInterval(timer) {
        clearInterval(timer);
    }

    millisToMinutesAndSeconds(millis) {
        let day, hour, minute, seconds;
        seconds = Math.floor(millis / 1000);
        minute = Math.floor(seconds / 60);
        seconds = seconds % 60;
        hour = Math.floor(minute / 60);
        minute = minute % 60;
        day = Math.floor(hour / 24);
        hour = hour % 24;
        return  '' + (hour < 10 ? '0' : '') + hour + ':' + (minute < 10 ? '0' : '') + minute + ':' +
            (seconds < 10 ? '0' : '') + seconds;
    }

    startTest(timeLeft = this.program[this.programIndex].test.duration) {
        this.testStartTime = this.getMilliseconds();
        this.prevQuestionTime = this.getMilliseconds();

        timeLeft = timeLeft / 1000;
        this.timer = this.millisToMinutesAndSeconds(timeLeft * 1000);
        this.finishTestTimer = setInterval(() => {
            if (timeLeft > 0) {
                timeLeft--;
                this.timer = this.millisToMinutesAndSeconds(timeLeft * 1000);
            } else {
                this.saveTestResult();
            }
        }, 1000);
    }


    startVideoAfterBreak(breakTime: number) {
        setTimeout(() => {
            this.programIndex++;
            this.startVideo(1);
        }, breakTime);

        this.breakTimer = setTimeout(() => {
            let timeLeft = breakTime / 1000;
            this.timer = this.millisToMinutesAndSeconds(timeLeft * 1000);
            this.breakTimer = setInterval(() => {
                if (timeLeft > 0) {
                    timeLeft--;
                    this.timer = this.millisToMinutesAndSeconds(timeLeft * 1000);
                } else {
                    this.timer = '';

                    // this.programIndex++;
                    // this.startVideo(0);

                    this.stopInterval(this.breakTimer);
                }
            }, 1000);
        });
    }

    startBreak(timeLeft = +this.program[this.programIndex].test.breakDuration) {
        this.startVideoAfterBreak(timeLeft);
    }

    saveTestResult(timeLeftTest = 0) {
        this.stopInterval(this.finishTestTimer);
        this.slideIndex = 0;

        const generalTime = +new Date() - +this.testStartTime;
        let rightAnswers = 0;
        this.detailedStatistics.forEach(answer => (answer.isCorrect) ? rightAnswers++ : null);

        const testResultData = {
            user: this.currentUser.id,
            study: this.study.id,
            test: this.program[this.programIndex].test._id,
            result: rightAnswers / this.program[this.programIndex].test.QA.length * 100,
            time: generalTime,
            detailedStatistics: JSON.stringify([...this.detailedStatistics])
        };

        this.detailedStatistics = [];

        this.testsResultsService.create(testResultData)
            .subscribe()
            .add(() => {
                this.programIndex++;

                if (this.programIndex < this.program.length - 1) {
                    this.startBreak(timeLeftTest + +this.program[this.programIndex - 1].test.breakDuration);
                }
            });

        // this.programIndex++;
        // if (this.programIndex < this.program.length - 1) {
        //     this.startBreak();
        // }
    }

    selectAnswer(question: string, isCorrect: boolean) {
        const aTime = +new Date() - this.prevQuestionTime;
        this.prevQuestionTime = this.getMilliseconds();

        const statistic = {q: question, aTime, isCorrect};
        this.detailedStatistics.push(statistic);

        if (this.slideIndex === this.program[this.programIndex].test.QA.length - 1) {
            const generalTime = +new Date() - +this.testStartTime;
            const timeLeftTest = this.program[this.programIndex].test.duration - generalTime;
            this.saveTestResult(timeLeftTest);
        } else {
            this.testCarousel.moveTo(++this.slideIndex, true);
        }
    }


    private getMilliseconds() {
        return +new Date();
    }

    isShowChat() {
        this.showChat = true;
        if (moment(this.study.startDate).format('DD-MM-YYYY') === TODAY) {
            this.showChat = true;
        }
    }

    makeProgram(study = this.study, tests = this.tests): any[] {
        const program = [];
        let cycle = 0;

        program.push({
            startAt: 0,
            state: this.ProgramState.waiting,
            test: null,
            cycle,
            videoInfo: {
                url: study.introVideo,
                poster: study.introPoster,
                duration: study.introVideoDuration
            }
        });

        let timeLine = +study.startDate;

        program.push({
            startAt: timeLine,
            state: this.ProgramState.playingVideo,
            cycle,
            test: null,
            videoInfo: {
                url: study.introVideo,
                poster: study.introPoster,
                duration: study.introVideoDuration
            }
        });

        cycle++;
        timeLine += parseInt('' + study.introVideoDuration, 0);
        program.push({
            startAt: timeLine,
            state: this.ProgramState.waiting,
            cycle,
            test: tests[0],
            videoInfo: {
                url: tests[0].videoUrl,
                poster: tests[0].poster,
                duration: tests[0].videoDuration
            }
        });


        timeLine += +study.breakDuration;
        program.push({
            startAt: timeLine,
            state: this.ProgramState.playingVideo,
            cycle,
            test: tests[0],
            videoInfo: {
                url: tests[0].videoUrl,
                poster: tests[0].poster,
                duration: tests[0].videoDuration
            }
        });

        tests.forEach((item, index) => {
            if (cycle === 1) {
                timeLine += parseInt('' + tests[0].videoDuration, 0);
                program.push({
                    startAt: timeLine,
                    state: this.ProgramState.passingTest,
                    cycle,
                    test: tests[0],
                    videoInfo: {
                        url: tests[0].videoUrl,
                        poster: tests[0].poster,
                        duration: tests[0].videoDuration
                    }
                });

                cycle++;
            } else {
                timeLine += item.duration;
                program.push({
                    startAt: timeLine,
                    state: this.ProgramState.waiting,
                    cycle,
                    test: tests[index],
                    videoInfo: {
                        url: tests[index].videoUrl,
                        poster: tests[index].poster,
                        duration: tests[index].videoDuration
                    }
                });

                timeLine += item.breakDuration;
                program.push({
                    startAt: timeLine,
                    state: this.ProgramState.playingVideo,
                    cycle,
                    test: tests[index],
                    videoInfo: {
                        url: tests[index].videoUrl,
                        poster: tests[index].poster,
                        duration: tests[index].videoDuration
                    }
                });

                timeLine += parseInt('' + item.videoDuration, 0);
                program.push({
                    startAt: timeLine,
                    state: this.ProgramState.passingTest,
                    cycle,
                    test: tests[index],
                    videoInfo: {
                        url: tests[index].videoUrl,
                        poster: tests[index].poster,
                        duration: tests[index].videoDuration
                    }
                });

                cycle++;
            }
        });

        timeLine += tests[tests.length - 1].duration;
        program.push({
            startAt: timeLine,
            state: this.ProgramState.finished,
            cycle,
            test: null
        });

        return program;
    }

    socketSettings() {
        this.userSubscription = this.socketService
            .getUsers()
            .subscribe((users: any[]) => {
                this.users = users;
            });

        this.socketService.newUser(this.currentUser);

        this.subscription = this.socketService
            .getMessage()
            .subscribe((data: any) => {
                this.messages = data;
            });
    }

    showActivityBanner() {
        this.dispatchIsActive();
        this.basicModal.show();
    }

    userActive() {
        this.saveUserActivity(true);
    }

    saveUserActivity(isActive = false) {
        this.dispatchIsActive.cancel();
        this.basicModal.hide();
        // this.activitySubscription = this.userService.activity(this.currentUser.id, isActive)
        //     .subscribe()
        //     .add(() => {
        //         if (!isActive) {
        //             window.location.href = '/';
        //         }
        //     });
    }

    sendMessage() {
        if (this.message) {
            this.socketService.sendMessage(this.currentUser, this.message);
            this.message = '';
        }
    }

    keyDownFunction(event) {
        if (event['keyCode'] === 13) {
            this.sendMessage();
        }
    }
}
