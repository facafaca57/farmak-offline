import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Preparats} from "@app/_models";
import {AlertService, PreparatsService, SpecialtyService} from "@app/_services";
import {Router} from "@angular/router";
//import { environment } from '@environments/environment';

@Component({
  selector: 'app-preparats',
  templateUrl: './preparats.component.html',
  styleUrls: ['./preparats.component.scss']
})
export class PreparatsComponent implements OnInit, OnDestroy {
    public registrationForm: FormGroup;
    loading = false;
    preparats: Preparats[] = [];
    isData: boolean = false;
    timer: any;
    selectedSpecialty: string;
    preparatModel: Preparats;

    constructor(
        private router: Router,
        private preparatsService: PreparatsService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.loadInitialData();
        this.loadAllData();

        this.registrationForm = this.formBuilder.group({
            preparatName: ['', [Validators.required]],
        });
    }

    ngOnDestroy() {
        clearTimeout(this.timer);
    }

    private loadAllData() {
        this.preparatsService.getAll()
            .subscribe(preparats => {
                this.preparats = preparats;
                this.isData = true;
            });
    }

    private loadInitialData() {
    }

    public selectChangeHandler(event: any, selectedVal) {
        this[selectedVal] = event.target.value;
    }

    public deleteData(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.preparatsService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }
    public postData($event, registrationForm: any) {
        this.preparatModel = {
            preparatName: registrationForm.controls.preparatName.value,
        };

        this.loading = true;

        this.preparatsService.create(this.preparatModel)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Registration was successfully completed.');
                        this.registrationForm.reset();
                        this.loadAllData();
                        this.loadInitialData();
                    } else {
                        this.alertService.error('Error. Please try later.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
