import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Visit} from "@app/_models";
import {Router} from "@angular/router";
import {AlertService, VisitService} from "@app/_services";

@Component({
  selector: 'app-visits',
  templateUrl: './visits.component.html',
  styleUrls: ['./visits.component.scss']
})
export class VisitsComponent implements OnInit {
    registrationForm: FormGroup;
    visitModel: any = {};
    loading = false;
    visits: Visit[] = [];
    isData: boolean = false;

    constructor(
        private router: Router,
        private visitService: VisitService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
    ) {
        this.registrationForm = formBuilder.group({
            id: ['', [Validators.required]],
            serialNumber: ['', [Validators.required]],
            year: ['', [Validators.required]],
            week: ['', []],
        });
    }

    ngOnInit() {
        this.loadAllData();
    }


    public delete(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.visitService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Data successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    private loadAllData() {
        this.visitService.getAll()
            .subscribe(data => {
                this.visits = data;
                this.isData = true;
            });
    }

    public postData(registrationForm: any) {
        this.visitModel = {
            id: registrationForm.controls.id.value,
            serialNumber: registrationForm.controls.serialNumber.value,
            year: registrationForm.controls.year.value,
            week: registrationForm.controls.week.value,
        };

        this.loading = true;
        this.visitService.create(this.visitModel)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.registrationForm.reset();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
