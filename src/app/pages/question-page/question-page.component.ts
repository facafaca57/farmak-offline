import { Component, OnInit, OnDestroy, HostListener, ElementRef } from '@angular/core';
import {Question, Preparats} from "@app/_models";
import { PreparatsService, QuestionService, UserActivityServiceService } from "@app/_services";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-question-page',
  templateUrl: './question-page.component.html',
  styleUrls: ['./question-page.component.scss']
})
export class QuestionPageComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    questions: Question[] = [];
    preparat: Preparats;
    isData: boolean = false;

    questionModel: Question;
    specialtyId: string;
    preparatId: string;

    constructor(
        private questionService: QuestionService,
        private preparatService: PreparatsService,
        private userActivityServiceService: UserActivityServiceService,
        private currentRout: ActivatedRoute,
        private elementRef: ElementRef
    ) {

    }

    ngOnInit() {
        this.preparatId = this.currentRout.snapshot.paramMap.get('preparatId');
        this.loadAllData();
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.preparatService.getById(this.preparatId)
            .subscribe(preparat => {
                this.preparat = preparat;
                this.isData = true;
                this.userActivityServiceService.startActivity(this.preparat.preparatName);
            });

        this.questionService.getByQuery(`preparat=${this.preparatId}`)
            .subscribe(questions => {
                this.questions = questions;
                this.isData = true;
            });
    }
}



