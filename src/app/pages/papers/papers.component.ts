import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Paper } from "@app/_models";
import {AlertService, PaperService } from "@app/_services";
import { environment } from '@environments/environment';


@Component({
  selector: 'app-papers',
  templateUrl: './papers.component.html',
  styleUrls: ['./papers.component.scss']
})
export class PapersComponent implements OnInit {
    @ViewChild('inputFile', { static: false }) inputFile:ElementRef;

    registrationForm: FormGroup;
    loading = false;
    papers: Paper[] = [];
    isData: boolean = false;
    uploadsPdf:string = environment.uploadsPdf;

    constructor(
        private paperService: PaperService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
    ) {
        this.registrationForm = formBuilder.group({
            papers: ['', [Validators.required]],
            theme: [null],
            preparat: [null],
            year: [null]
        });
    }

    ngOnInit() {
        this.loadAllData();
    }

    private loadAllData() {
        this.paperService.getAll()
            .subscribe(data => {
                this.papers = data;
                this.isData = true;
            });
    }

    public delete(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.paperService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Data successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    onFileSelect(event) {
        if (event.target.files.length > 0) {
            const files = event.target.files;
            this.registrationForm.get('papers').setValue(files);
        }
    }

    public postData(registrationForm: any) {
        const formData: any = new FormData();
        const files: Array<File> = registrationForm.controls.papers.value;

        formData.append('theme', registrationForm.controls.theme.value);
        formData.append('preparat', registrationForm.controls.preparat.value);
        formData.append('year', registrationForm.controls.year.value);

        for (let i = 0; i < files.length; i++) {
            formData.append(`papers`, files[i], files[i]['name']);
        }

        this.loading = true;

        this.paperService.create(formData)
            .subscribe(
                data => {
                    if (data) {
                        this.alertService.success('Action was successfully completed.');
                        this.registrationForm.reset();
                        this.inputFile.nativeElement.value = null;
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
