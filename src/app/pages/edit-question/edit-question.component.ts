import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Question, Preparats} from "@app/_models";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, QuestionService, PreparatsService} from "@app/_services";
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss']
})
export class EditQuestionComponent implements OnInit {

    public form: FormGroup;
    public slideList: FormArray;
    loading = false;
    question: Question;
    selectedType: string;
    selectedPreparat: string;
    preparats: Preparats[] = [];
    files = [];
    uploadsImg = environment.uploadsImg;
    questionId: string;

    constructor(
        private router: Router,
        private questionService: QuestionService,
        private preparatService: PreparatsService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.questionId = this.currentRout.snapshot.paramMap.get('questionId');
        this.form = this.fb.group({
            title: [''],

            preparat: [null],
            slides: this.fb.array([])
        });
        this.slideList = this.form.get('slides') as FormArray;

        this.loadInitialData();
        this.loadAllData();
    }


    ngOnInit() {}


    private loadInitialData() {
        this.preparatService.getAll()
            .subscribe(data => {
                this.preparats = data;
            });
    }

    private loadAllData() {
        this.questionService.getById(this.questionId)
            .subscribe(question => {
                this.question = question;

                this.form.get('title').setValue(question.title);
                let oldPreparat = this.preparats.filter(preparat =>
                    question.preparat === preparat.preparatName
                );
                this.selectedPreparat = oldPreparat[0].id;

                for (const slide of question.slides) {

                    this.slideList.push(this.createSlide(slide.img, slide.link, slide.linkCenter, slide.linkBottom,
                        slide.linkBHL, slide.linkBHR, slide.linkBL, slide.linkBC, slide.linkBR, slide.textLeft,
                        slide.textLeftPdf, slide.textRight, slide.textRightPdf, slide.dateCountDown));
                }
            });
    }

    get slideFormGroup() {
        return this.form.get('slides') as FormArray;
    }

    createSlide(img=null, link=null, linkCenter=null, linkBottom=null, linkBHL=null, linkBHR=null,
                linkBL=null, linkBC=null, linkBR=null, textLeft=null, textLeftPdf=null, textRight=null, textRightPdf=null, dateCountDown=null): FormGroup {
        return this.fb.group({
            img: [img],
            link: [link],
            linkCenter: [linkCenter],
            linkBottom: [linkBottom],
            linkBHL: [linkBHL],
            linkBHR: [linkBHR],
            linkBL: [linkBL],
            linkBC: [linkBC],
            linkBR: [linkBR],
            textLeft: [textLeft],
            textLeftPdf: [textLeftPdf],
            textRight: [textRight],
            textRightPdf: [textRightPdf],
            dateCountDown: [dateCountDown]
        });
    }

    addSlide() {
        this.slideList.push(this.createSlide());
    }

    removeSlide(index) {
        this.slideList.removeAt(index);
    }

    onFileSelect(event, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.files[index] = file;
        }
    }

    public selectChangeHandler(event: any, selectedVal) {
        this[selectedVal] = event.target.value;
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const formData: any = new FormData();
        formData.append('title', form.controls.title.value);
        formData.append('preparat', this.selectedPreparat);

        const slides = this.slideList.value;

        const sidesReq = [];
        slides.map((slide, i) => {
            sidesReq[i] = {
                'img': slide.img,
                'link': slide.link,
                'linkCenter': slide.linkCenter,
                'linkBottom': slide.linkBottom,
                'linkBHL': slide.linkBHL,
                'linkBHR': slide.linkBHR,
                'linkBL': slide.linkBL,
                'linkBC': slide.linkBC,
                'linkBR': slide.linkBR,
                'textLeft': slide.textLeft,
                'textLeftPdf': slide.textLeftPdf,
                'textRight': slide.textRight,
                'textRightPdf': slide.textRightPdf,
                'dateCountDown': slide.dateCountDown
            };
        });

        formData.append(`slides`, JSON.stringify(sidesReq));

        for (const file of this.files) {
            if (file) {
                formData.append(`imgs`, file, file['name']);
            } else {
                formData.append(`imgs`, null);
            }
        }

        let filesIndex:any = [];
        if (this.files) {
            filesIndex = Object.keys(this.files);
        }
        formData.append('filesIndex', JSON.stringify(filesIndex));

        this.loading = true;
        this.questionService.update(formData, this.questionId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.form.reset();
                        this.slideList.clear();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
