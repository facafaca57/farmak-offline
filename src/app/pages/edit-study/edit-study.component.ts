import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, StudyService} from "@app/_services";
import * as moment from "moment";

@Component({
  selector: 'app-edit-study',
  templateUrl: './edit-study.component.html',
  styleUrls: ['./edit-study.component.scss']
})
export class EditStudyComponent implements OnInit {
    DATE_FORMAT = 'DD-MM-YYYY HH:mm';

    public form: FormGroup;
    loading = false;
    study: any;
    studyId: string;

    constructor(
        private router: Router,
        private studyService: StudyService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.studyId = this.currentRout.snapshot.paramMap.get('studyId');

        this.form = this.fb.group({
            startDate: [null]
        });

        this.loadInitialData();
    }


    ngOnInit() {}


    private loadInitialData() {
        this.studyService.getById(this.studyId)
            .subscribe(data => {
                this.study = data;
                this.setFormData();
            });
    }

    private setFormData() {
        this.form.get('startDate').setValue(moment(+this.study.startDate).format(this.DATE_FORMAT));
    }


    public postData($event, form: any) {
        $event.preventDefault();

        const formData = {
            startDate: +moment(form.controls.startDate.value, this.DATE_FORMAT),
        };

        this.loading = true;
        this.studyService.updateStartDate(formData, this.studyId)
            .subscribe(
                data => {
                    if (data) {
                        this.alertService.success('Action was successfully completed.');
                        this.study.startDate = +moment(form.controls.startDate.value, this.DATE_FORMAT);
                        this.form.reset();
                        this.setFormData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
