﻿import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {first} from 'rxjs/operators';

import { Specialty, Visit, Presentation, Paper, Question, Preparats, User } from '@app/_models';
import {SpecialtyService, UserActivityServiceService, AuthenticationService, UpdateService} from '@app/_services';
import { environment } from '@environments/environment';
import { AlertController } from '@ionic/angular';


@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    loading = false;
    specialties: Specialty[];
    publicFolder = environment.publicFolder;
    uploadsImg = environment.uploadsImg;
    visits: Visit[];
    presentations: Presentation[];
    papers: Paper[];
    preparats: Preparats[];
    questions: Question[];
    specialty9: Specialty;
    specialty10: Specialty;
    specialty11: Specialty;
    specialty12: Specialty;
    specialty13: Specialty;
    currentUser: User;

    constructor(
        private specialtyService: SpecialtyService,
        private userActivityServiceService: UserActivityServiceService,
        private authenticationService: AuthenticationService,
        private elementRef: ElementRef,
        private update: UpdateService,
        private alertCtrl: AlertController
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit() {
        this.loading = true;
        this.specialtyService.getAll().pipe(first()).subscribe(specialties => {
            this.loading = false;

            this.specialties = specialties.sort((a, b) => a.orderNumber - b.orderNumber)

            this.specialty9 = this.specialties[9];
            this.specialty10 = this.specialties[10];
            this.specialty11 = this.specialties[11];
            this.specialty12 = this.specialties[12] || null;
            this.specialty13 = this.specialties[13] || null;
        });
        this.userActivityServiceService.startActivity('Головна');
    }

    checkUpdates(e: Event){
        e.preventDefault();
        this.authenticationService.network.then(({connected}) => {
            if (this.currentUser && connected) {
                this.update.checkUpdates();
            } else if (!connected) {
                this.noUpdates('Мережа', 'Ви не в мережі')
            }
        })
    }

    async noUpdates(header: string = 'Оновлення', message: string = 'Не знайденно нових оновлень') {
        const alert = await this.alertCtrl.create({
          header,
          message,
          backdropDismiss: true,
          translucent: true,
          animated: true,
          buttons: [
            { text: 'OK', role: 'confirm' },
          ],
        });
    
        await alert.present();
        const { role } = await alert.onDidDismiss();
    
        return role;
    }
  
    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }
}
