import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Visit } from "@app/_models";
import { VisitService, UserActivityServiceService } from "@app/_services";

import { environment } from '@environments/environment';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {

    @ViewChild('carousel', { static: false }) carousel: any;

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @HostListener('swipeleft', ['$event']) public swipePrev(event: any) {
        this.carousel.nextSlide();
    };

    @HostListener('swiperight', ['$event']) public swipeNext(event: any) {
        this.carousel.previousSlide();
    };

    visits: Visit[] = [];
    years = [];

    publicFolder = environment.publicFolder;

    constructor(
        private visitService: VisitService,
        private userActivityServiceService: UserActivityServiceService,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.loadAllData();
        this.userActivityServiceService.startActivity('Сторінка Історія');
    }
    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {

        this.visitService.getAll()
            .subscribe(visits => {
                this.visits = visits.sort((a, b) => b.id - a.id);

                for (let i = 0; i < this.visits.length; i++) {
                    this.years[i] = this.visits[i].year;
                }
                this.years = this.unique(this.years);
                this.years = this.years.sort((a, b) => a - b);
            });

    }
    private unique(arr) {
        let result = [];

        for (let str of arr) {
          if (!result.includes(str)) {
            result.push(str);
          }
        }

        return result;
    };

}
