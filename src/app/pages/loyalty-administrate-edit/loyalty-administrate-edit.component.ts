import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { LoyalityPresentation} from "@app/_models";
import {ActivatedRoute} from "@angular/router";
import {AlertService, LoyalityPresentationService} from "@app/_services";
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-loyalty-administrate-edit',
  templateUrl: './loyalty-administrate-edit.component.html',
  styleUrls: ['./loyalty-administrate-edit.component.scss']
})
export class LoyaltyAdministrateEditComponent implements OnInit {
    public form: FormGroup;
    public slideList: FormArray;
    loading = false;
    files = [];
    uploadsImg = environment.uploadsImg;
    presentationId: string;
    presentation: LoyalityPresentation;

    constructor(
        private presentationService: LoyalityPresentationService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute,
    ) {
        this.presentationId = this.currentRout.snapshot.paramMap.get('loyality-presentationsId');

        this.form = this.fb.group({
            title: [''],
            slides: this.fb.array([])
        });
        this.slideList = this.form.get('slides') as FormArray;

        this.loadAllData();
    }
    ngOnInit() {}

    private loadAllData() {
        this.presentationService.getById(this.presentationId)
            .subscribe(presentation => {
                this.presentation = presentation;

                this.form.get('title').setValue(presentation.title);

                for (const slide of presentation.slides) {

                    this.slideList.push(this.createSlide(slide.img, slide.link, slide.linkCenter, slide.linkBottom,
                        slide.linkBHL, slide.linkBHR, slide.linkBL, slide.linkBC, slide.linkBR, slide.textLeft, slide.textRight));
                }
            });
    }

    get slideFormGroup() {
        return this.form.get('slides') as FormArray;
    }

    createSlide(img=null, link=null, linkCenter=null, linkBottom=null, linkBHL=null, linkBHR=null,
                linkBL=null, linkBC=null, linkBR=null, textLeft=null, textRight=null): FormGroup {
        return this.fb.group({
            img: [img],
            link: [link],
            linkCenter: [linkCenter],
            linkBottom: [linkBottom],
            linkBHL: [linkBHL],
            linkBHR: [linkBHR],
            linkBL: [linkBL],
            linkBC: [linkBC],
            linkBR: [linkBR],
            textLeft: [textLeft],
            textRight: [textRight]
        });
    }

    addSlide() {
        this.slideList.push(this.createSlide());
    }

    removeSlide(index) {
        this.slideList.removeAt(index);
    }

    onFileSelect(event, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.files[index] = file;
        }
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const formData: any = new FormData();
        formData.append('title', form.controls.title.value);

        const slides = this.slideList.value;

        const sidesReq = [];
        slides.map((slide, i) => {
            sidesReq[i] = {
                'img': slide.img,
                'link': slide.link,
                'linkCenter': slide.linkCenter,
                'linkBottom': slide.linkBottom,
                'linkBHL': slide.linkBHL,
                'linkBHR': slide.linkBHR,
                'linkBL': slide.linkBL,
                'linkBC': slide.linkBC,
                'linkBR': slide.linkBR,
                'textLeft': slide.textLeft,
                'textRight': slide.textRight
            };
        });

        formData.append(`slides`, JSON.stringify(sidesReq));

        for (const file of this.files) {
            if (file) {
                formData.append(`imgs`, file, file['name']);
            } else {
                formData.append(`imgs`, null);
            }
        }

        let filesIndex:any = [];
        if (this.files) {
            filesIndex = Object.keys(this.files);
        }
        formData.append('filesIndex', JSON.stringify(filesIndex));

        this.loading = true;
        this.presentationService.update(formData, this.presentationId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.form.reset();
                        this.slideList.clear();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
