import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Specialty} from "@app/_models";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, SpecialtyService} from "@app/_services";

@Component({
  selector: 'app-edit-specialty',
  templateUrl: './edit-specialty.component.html',
  styleUrls: ['./edit-specialty.component.scss']
})
export class EditSpecialtyComponent implements OnInit {

  public form: FormGroup;
    loading = false;
    specialty: Specialty;
    specialtyId: string;

    constructor(
        private router: Router,
        private specialtyService: SpecialtyService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.specialtyId = this.currentRout.snapshot.paramMap.get('specialtyId');

        this.form = this.fb.group({
            specialtyName: [null],
            logo: [null],
            orderNumber: [null],
        });

        this.loadInitialData();
    }


    ngOnInit() {}


    private loadInitialData() {
        this.specialtyService.getById(this.specialtyId)
            .subscribe(data => {
                this.specialty = data;
                this.setFormData();
            });
    }

    private setFormData() {
        this.form.get('specialtyName').setValue(this.specialty.specialtyName);
        this.form.get('logo').setValue(this.specialty.logo);
        this.form.get('orderNumber').setValue(this.specialty.orderNumber);
    }


    public postData($event, form: any) {
        $event.preventDefault();

        const formData: Specialty = {
          specialtyName: form.controls.specialtyName.value,
          logo: form.controls.logo.value,
          orderNumber: form.controls.orderNumber.value,
        };

        this.loading = true;
        this.specialtyService.update(formData, this.specialtyId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.specialty = data;
                        this.form.reset();
                        this.setFormData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
