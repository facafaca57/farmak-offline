import { Component, OnInit, HostListener, OnDestroy, ElementRef, ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MarketingCommentsService, PreparatsService, AuthenticationService, AlertService } from "@app/_services";
import { MarketingComments, Preparats, User } from '@app/_models';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';

@Component({
  selector: 'app-control-marketing-comments',
  templateUrl: './control-marketing-comments.component.html',
  styleUrls: ['./control-marketing-comments.component.scss']
})
export class ControlMarketingCommentsComponent implements OnInit, OnDestroy {

    @ViewChild('dateEnd') dateEnd: ElementRef;
    @ViewChild('dateStart') dateStart: ElementRef;

    preparats: Preparats[] = [];
    preparatNames = [];

    type: string;

    comments: MarketingComments[] = [];
    newComments: any = [];
    thisDate: string;

    constructor(
        private marketingCommentsService: MarketingCommentsService,
        private alertService: AlertService,
    ) {
        this.loadAllData();

        this.thisDate = moment(new Date()).format('Y-MM-DD');
    }

    ngOnInit() {  }
    ngOnDestroy() {}

    private loadAllData() {
        this.marketingCommentsService.getAll()
        .subscribe(comments => {
            this.comments = comments;
            console.log(comments);
            

            this.comments = this.comments.sort((a, b) => <any>new Date(b.date) - <any>new Date(a.date));


            this.convertDataToAssociateArr(this.comments);
        });
    }

    convertDataToAssociateArr(data) {
        let keys = Object.keys(this.newComments);
        for(let i of keys) {
            delete this.newComments[i];
        }
        this.preparatNames = [];

        for (let i = 0; i < data.length; i++) {
            this.preparatNames[i] = data[i].preparat.preparatName ? data[i].preparat.preparatName : data[i].preparat;
        }

        this.preparatNames = this.uniqueAndSort(this.preparatNames);
        for (let i = 0; i < this.preparatNames.length; i++) {
            let newArr = [];
            for(let j = 0; j < data.length; j++) {
                if (this.preparatNames[i] == data[j].preparat.preparatName || data[j].preparat) {
                    if (!data[j].user) {
                        data[j].user = 'deletedUser';
                    }
                    newArr.push(data[j]);
                }
            }

            this.newComments[this.preparatNames[i]] = newArr;

        }
        console.log(this.newComments);
        
    }

    private uniqueAndSort(arr) {
        let result = [];

        for (let str of arr) {
          if (!result.includes(str)) {
            result.push(str);
          }
        }

        result = result.sort((a, b) => a - b);

        return result;
    };

    private checkArrayToSelectedPeriod(dateStart, dateEnd, el) {
        let dateStartMs = Date.parse(dateStart);
        let dateEndMs = Date.parse(dateEnd);
        let mainDateMs = Date.parse(el.date);
        if( mainDateMs >= dateStartMs && mainDateMs <= dateEndMs) return true; else return false;
    }

    filterComments() {
        let newArr = Object.keys(this.newComments).map(key => this.newComments[key]);

        let dateStart = this.dateStart.nativeElement.value;
        let dateEnd = this.dateEnd.nativeElement.value;
        if (dateStart && dateEnd) {
            if(Date.parse(dateStart) && Date.parse(dateEnd)) {
                let newComments = [];

                newArr.forEach((val) => {
                    val.forEach((newVal) => {
                        if(this.checkArrayToSelectedPeriod(dateStart, dateEnd,newVal))
                        newComments.push(newVal);
                    });
                });

                this.convertDataToAssociateArr(newComments);

            } else {
                this.alertService.error('Введено неправильні дані');
            }
        } else {
            this.alertService.error('Заповніть дати для фільтрації');
        }
    }

    clearFilter() {
        this.loadAllData();
    }

}
