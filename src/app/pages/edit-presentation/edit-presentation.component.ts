import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Presentation, Specialty, Visit} from "@app/_models";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, PresentationService, SpecialtyService, VisitService} from "@app/_services";
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-edit-presentation',
  templateUrl: './edit-presentation.component.html',
  styleUrls: ['./edit-presentation.component.scss']
})
export class EditPresentationComponent implements OnInit {

    public form: FormGroup;
    public slideList: FormArray;
    public linkList: FormArray;
    loading = false;
    presentation: Presentation;
    types: string[] = ['forUsers', 'forCustomers'];
    selectedType: string;
    selectedSpecialty: string;
    selectedVisit: number;
    specialties: Specialty[] = [];
    visits: Visit[] = [];
    files = [];
    uploadsImg = environment.uploadsImg;
    presentationId: string;
    imgList: any[] = [];

    constructor(
        private router: Router,
        private presentationService: PresentationService,
        private visitService: VisitService,
        private specialtyService: SpecialtyService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.presentationId = this.currentRout.snapshot.paramMap.get('presentationId');

        this.form = this.fb.group({
            title: [''],
            type: [null],
            specialty: [null],
            visit: [null],
            slides: this.fb.array([])
        });
        this.slideList = this.form.get('slides') as FormArray;
        //this.linkList = this.fb.array([]);
        this.loadInitialData();
        this.loadAllData();
    }

    ngOnInit() {}

    private loadInitialData() {
        this.visitService.getAll()
            .subscribe(data => {
                this.visits = data;
            });

        this.specialtyService.getAll()
            .subscribe(data => {
                this.specialties = data;
            });
    }

    private loadAllData() {
        this.presentationService.getById(this.presentationId)
            .subscribe(presentation => {
                this.presentation = presentation;

                this.form.get('title').setValue(presentation.title);
                this.selectedType = presentation.type;
                this.selectedVisit = presentation.visit;

                let oldSpecialty = this.specialties.filter(specialty =>
                    presentation.specialty === specialty.specialtyName
                );
                this.selectedSpecialty = oldSpecialty[0].id;

                for (const slide of presentation.slides) {
                    if(slide.img || slide.subSlides.length) {
                        this.imgList.push({
                            img: slide.img || null,
                            subSlides: slide.subSlides.map(sl => sl.img)
                        });
                    } else {
                        this.imgList.push(null);
                    }
                    this.slideList.push(this.createSlide(slide.img, slide.link, slide.linkCenter, slide.linkBottom,
                        slide.linkBHL, slide.linkBHR, slide.linkBL, slide.linkBC, slide.linkBR, slide.textLeft, slide.textLeftPdf, slide.textRight, slide.textRightPdf, slide.linkCoordinats, slide.subSlides));
                }
            });
    }

    get slideFormGroup() {
        return this.form.get('slides') as FormArray;
    }

    createLinks(): FormGroup {
        return this.fb.group({
            mainLinkCoordinats: [null],
            LeftC: [null],
            TopC: [null],
            LinkWidth: [null],
            LinkHeight: [null],
        });
    }

    createSlide(img=null, link=null, linkCenter=null, linkBottom=null, linkBHL=null, linkBHR=null,
                linkBL=null, linkBC=null, linkBR=null, textLeft=null, textLeftPdf=null, textRight=null, textRightPdf=null, linkCoordinats=null, subSlides=null): FormGroup {
        let linkList = this.fb.array([]);

        if(linkCoordinats) {
            linkCoordinats.forEach(link => {
                linkList.push(
                    this.fb.group({
                        mainLinkCoordinats: [link.mainLinkCoordinats],
                        LeftC: [link.LeftC],
                        TopC: [link.TopC],
                        LinkWidth: [link.LinkWidth],
                        LinkHeight: [link.LinkHeight],
                    })
                );
            });
        } else {
            linkList = this.fb.array([this.createLinks()]);
        }
        let subslides = this.fb.array([]);
        if (subSlides) {
            subSlides.forEach(sub => {
                subslides.push(
                    this.fb.group({
                        img: sub.img,
                        linkCoordinats: this.fb.array(
                            sub.linkCoordinats.map(slink => this.fb.group(
                                {
                                    mainLinkCoordinats: slink.mainLinkCoordinats,
                                    LeftC: slink.LeftC,
                                    TopC: slink.TopC,
                                    LinkWidth: slink.LinkWidth,
                                    LinkHeight: slink.LinkHeight,
                                }
                            ))
                        ),
                        link: [sub.link],
                        linkCenter: [sub.linkCenter],
                        linkBottom: [sub.linkBottom],
                        linkBHL: [sub.linkBHL],
                        linkBHR: [sub.linkBHR],
                        linkBL: [sub.linkBL],
                        linkBC: [sub.linkBC],
                        linkBR: [sub.linkBR],
                        textLeft: [sub.textLeft],
                        textLeftPdf: [sub.textLeftPdf],
                        textRight: [sub.textRight],
                        textRightPdf: [sub.textRightPdf]
                    })
                );
            })
        }

        return this.fb.group({
            img: [img],
            link: [link],
            linkCenter: [linkCenter],
            linkBottom: [linkBottom],
            linkBHL: [linkBHL],
            linkBHR: [linkBHR],
            linkBL: [linkBL],
            linkBC: [linkBC],
            linkBR: [linkBR],
            textLeft: [textLeft],
            textLeftPdf: [textLeftPdf],
            textRight: [textRight],
            textRightPdf: [textRightPdf],
            linkCoordinats: linkList,
            subSlides: subslides
        });

    }

    addSlide() {
        this.slideList.push(this.createSlide());
    }

    removeSlide(index) {
        this.slideList.removeAt(index);
        this.imgList.splice(index, 1);
    }

    linkFormGroup(i) {
        let slideList = this.form.get('slides') as FormArray;
        return slideList.controls[i].get('linkCoordinats') as FormArray;
    }

    addLinks(i) {
        (this.slideList.controls[i].get('linkCoordinats') as FormArray).push(this.createLinks());
    }

    removeLink(index) {
        let len = (this.slideList.controls[index].get('linkCoordinats') as FormArray).length;
        (this.slideList.controls[index].get('linkCoordinats') as FormArray).removeAt(len-1);
    }


    // SUBSLIDES
    createSubSlide(): FormGroup {
        let slide = this.fb.group({
            img: [null],
            linkCoordinats: this.fb.array([]),
            link: [null],
            linkCenter: [null],
            linkBottom: [null],
            linkBHL: [null],
            linkBHR: [null],
            linkBL: [null],
            linkBC: [null],
            linkBR: [null],
            textLeft: [null],
            textRight: [null]
        });

        this.linkList = slide.get('linkCoordinats') as FormArray;

        return slide;
    }
    subSlideFormGroup(i) {
        return this.slideList.controls[i].get('subSlides') as FormArray;
    }
    subLinkFormGroup(i, j) {
        let subList = this.slideList.controls[i].get('subSlides') as FormArray;
        return subList.controls[j].get('linkCoordinats') as FormArray;
    }
    addSubLinks(i, j) {
        let subList = this.slideList.controls[i].get('subSlides') as FormArray;
        (subList.controls[j].get('linkCoordinats') as FormArray).push(this.createLinks());
    }
    removeSubLink(i, j) {
        let subList = this.slideList.controls[i].get('subSlides') as FormArray;
        let len = (subList.controls[j].get('linkCoordinats') as FormArray).length;
        (subList.controls[j].get('linkCoordinats') as FormArray).removeAt(len-1);
    }

    addSubSlides(i){
        (this.slideList.controls[i].get('subSlides') as FormArray).push(this.createSubSlide());
    }
    removeSubSlides(i, j) {
        if (this.files[i]?.subSlide) this.files[i].subSlide.splice(j, 1);
        if (this.imgList[i]?.subSlides) this.imgList[i]?.subSlides.splice(j, 1);
        (this.slideList.controls[i].get('subSlides') as FormArray).removeAt(j);
    }

    onFileSelect(event, index, isSub = null) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            if (isSub != null) {
                this.files[index]?.subSlide ? this.files[index].subSlide.push(file) : this.files[index] = { ...this.files[index], subSlide: [ file ] };
                // if (this.files[index]?.subSlide) this.files[index].subSlide[isSub] = file;
                // else {
                //     let slides = new Array();
                //     slides[isSub] = file
                //     this.files[index] = { ...this.files[index], subSlide: slides };
                // }
                let subList = this.subSlideFormGroup(index);
                subList.controls[isSub].get('img').setValue(index + file['name'].split('\\').reverse()[0]);
            } else {
                this.files[index] = {...this.files[index], slide: file };
            }
            console.log(this.files);

        }
    }
    //END

    // onFileSelect(event, index) {
    //     if (event.target.files.length > 0) {
    //         const file = event.target.files[0];
    //         this.files[index] = file;
    //     }
    // }

    isImage(url) {
        return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
    }

    public selectChangeHandler(event: any, selectedVal) {
        this[selectedVal] = event.target.value;
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const formData: any = new FormData();
        formData.append('title', form.controls.title.value);
        formData.append('type', this.selectedType);
        formData.append('specialty', this.selectedSpecialty);
        formData.append('visit', +this.selectedVisit);

        const slides = this.slideList.value;
        const sidesReq = [];

        slides.map((slide, i) => {
            sidesReq[i] = {
                'img': slide.img,
                'subSlides': slide.subSlides,
                'link': slide.link,
                'linkCenter': slide.linkCenter,
                'linkBottom': slide.linkBottom,
                'linkBHL': slide.linkBHL,
                'linkBHR': slide.linkBHR,
                'linkBL': slide.linkBL,
                'linkBC': slide.linkBC,
                'linkBR': slide.linkBR,
                'textLeft': slide.textLeft,
                'textLeftPdf': slide.textLeftPdf,
                'textRight': slide.textRight,
                'textRightPdf': slide.textRightPdf,
                'linkCoordinats' : slide.linkCoordinats,
            };
        });
        formData.append(`slides`, JSON.stringify(sidesReq));

        for (const file of this.files) {
            if (file) {
                if (file['slide']) {
                    formData.append(`imgs`, file['slide'], file['slide']['name']);
                }
                else if (file['subSlide']) {
                    for (const subFile of file['subSlide']) {
                        formData.append(`subslides`, subFile, this.files.indexOf(file) + subFile['name']);
                    }
                }
            } else {
                formData.append(`imgs`, null);
            }
        }

        let filesIndex:any = [];
        if (this.files) {
            this.files.forEach(e => filesIndex.push([this.files.indexOf(e), Object.keys(e)]));
            // filesIndex = Object.keys(this.files);
        }
        formData.append('filesIndex', JSON.stringify(filesIndex));

        this.loading = true;
        this.presentationService.update(formData, this.presentationId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.form.reset();
                        this.slideList.clear();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
