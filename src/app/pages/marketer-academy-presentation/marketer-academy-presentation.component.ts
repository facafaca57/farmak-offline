import { Component, OnInit, ViewChild, HostListener, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { marketerAcademy } from "@app/_models";
import { marketerAcademyService, UserActivityServiceService, FilesService } from "@app/_services";
import { environment } from '@environments/environment';
import { NguCarousel, NguCarouselStore, NguCarouselConfig } from '@ngu/carousel';
import { map } from 'rxjs/operators';
import { interval, Subscription } from 'rxjs';
import { swiperConfig } from '@app/_configs/swiper.conf';

@Component({
  selector: 'app-marketer-academy-presentation',
  templateUrl: './marketer-academy-presentation.component.html',
  styleUrls: ['./marketer-academy-presentation.component.scss']
})
export class MarketerAcademyPresentationComponent implements OnInit {

    @ViewChild('carousel', { static: false }) carousel: any;
    @ViewChild('visitsCarousel', { static: false }) visitsCarousel: any;
    @ViewChild('basicModal', { static: false }) basicModal: any;
    link:string = '';

    @ViewChild('myCarousel', { static: false }) myCarousel: NguCarousel<any>;

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @HostListener('swipeleft', ['$event']) public swipePrev(event: any) {
        this.carousel.nextSlide();
        this.visitsCarousel.nextSlide();
    }

    @HostListener('swiperight', ['$event']) public swipeNext(event: any) {
        this.carousel.previousSlide();
        this.visitsCarousel.previousSlide();
    }
    
    public config = swiperConfig;

    marketerAcademyId: string;
    marketerAcademy: marketerAcademy;

    loading = false;
    isData: boolean = false;
    uploadsImg:string = environment.uploadsImg;
    uploadsPdf:string = environment.uploadsPdf;

    presentationLinks: any[] = [];
    slideIndex = 0;
    public carouselConfig: NguCarouselConfig = {
        grid: { xs: 6, sm: 6, md: 6, lg: 6, all: 0 },
        slide: 1,
        speed: 250,
        point: {
            visible: true
        },
        load: 2,
        velocity: 0,
        touch: true,
        vertical: {
            enabled: true,
            height: 606
        },
        easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

    private _trialEndsAt;
    private _diff: number;
    private _days: number;
    private _hours: number;
    private _minutes: number;
    private _seconds: number;
    private days: string;
    private hours: string;
    private minutes: string;
    private seconds: string;
    private interv: Subscription;

  constructor(
      private currentRout: ActivatedRoute,
      private userActivityServiceService: UserActivityServiceService,
      private marketerAcademyService: marketerAcademyService,
      private files: FilesService,
      private elementRef: ElementRef
  ) {
      currentRout.params.subscribe(val => {
          this.marketerAcademyId = this.currentRout.snapshot.paramMap.get('marketerAcademyId');
          this.loadAllData();
      });
  }

    ngOnInit() {}

    ngOnDestroy() {
        if(this.interv) {
            this.interv.unsubscribe();
        }
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    onmoveFn(data: NguCarouselStore) {
    }

    getDays(t) {
        return Math.floor( t / (1000 * 60 * 60 * 24) );
    }

    getHours(t) {
        return Math.floor( (t / (1000 * 60 * 60)) % 24 );
    }

    getMinutes(t) {
        return Math.floor( (t / 1000 / 60) % 60 );
    }

    getSeconds(t) {
        return Math.floor( (t / 1000) % 60 );
    }

    addZeros(n, needLength = null) {
        needLength = needLength || 2;
        n = String(n);
        while (n.length < needLength) {
          n = "0" + n;
        }
        return n
      }

    decriseTime(time) {
        this._trialEndsAt = time;
         this.interv = interval(1000).pipe(
            map((x) => {this._diff = Date.parse(this._trialEndsAt) - Date.parse(new Date().toString());
              })).subscribe((x) => {
                  this._days = this.getDays(this._diff);
                  this._hours = this.getHours(this._diff);
                  this._minutes = this.getMinutes(this._diff);
                  this._seconds = this.getSeconds(this._diff);

                  this.days = this.addZeros(this._days);
                  this.hours = this.addZeros(this._hours);
                  this.minutes = this.addZeros(this._minutes);
                  this.seconds = this.addZeros(this._seconds);

              });

        return '<span>' + this.days + '<span>днів</span></span>:<span>' + this.hours + '<span>годин</span></span>:<span>' + this.minutes + '<span>хвилин</span></span>:<span>' + this.seconds + '<span>секунд</span></span>';
    }

    private loadAllData() {
        this.marketerAcademyService.getById(this.marketerAcademyId)
            .subscribe(marketerAcademy => {
                this.marketerAcademy = marketerAcademy;
                this.isData = true;
                this.userActivityServiceService.startActivity(this.marketerAcademy.title);
            });
    }

    // public getPresentationId(visitId) {
    //     for (let i = 0; i < this.presentations.length; i++) {
    //         if (this.presentations[i]['visit']['id'] === +visitId) {
    //             return this.presentations[i]['id'];
    //         }
    //     }
    //     return null;
    // }

    showModal(e, link) {
        e.preventDefault();
        let fileName = link.split('/').splice(-1);
        this.files.openPDF(fileName);
    }

    checkTheYoutube(link) {
        if(link) {
            if(~link.indexOf('youtu.be') || ~link.indexOf('youtube')) return true
            else return false;
        } else return false;

    }

    openVideo(e, link) {
        e.preventDefault();
        let filename = link.split('/').splice(-1);
        this.files.openVideo(filename);
    }

    getUrlFromYT(url) {
        return `https://farmak-rx2.com/public/uploads/yt/${this.getYoutubeUrl(url)}.mp4`;
    }
    
    getYoutubeUrl(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);

        const id = (match && match[2].length === 11)
          ? match[2]
          : null;

        return id;
    }

}
