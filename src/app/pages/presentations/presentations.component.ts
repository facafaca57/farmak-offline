import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Presentation, Specialty, Visit} from "@app/_models";
import {Router} from "@angular/router";
import {AlertService, PresentationService, SpecialtyService, VisitService} from "@app/_services";
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-presentations',
  templateUrl: './presentations.component.html',
  styleUrls: ['./presentations.component.scss']
})
export class PresentationsComponent implements OnInit {
    public form: FormGroup;
    public slideList: FormArray;
    public linkList: FormArray;
    loading = false;
    presentations: Presentation[] = [];
    isData: boolean = false;
    types: string[] = ['forUsers', 'forCustomers'];
    selectedType: string = this.types[0];
    selectedSpecialty: string;
    selectedVisit: number;
    specialties: Specialty[] = [];
    visits: Visit[] = [];
    files = [];
    uploadsImg = environment.uploadsImg;
    uploadsPdf = environment.uploadsPdf;
    queryString: string;

    constructor(
        private router: Router,
        private presentationService: PresentationService,
        private visitService: VisitService,
        private specialtyService: SpecialtyService,
        private fb: FormBuilder,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.loadInitialData();
        this.loadAllData();

        this.form = this.fb.group({
            title: ['', [Validators.required]],
            type: [this.selectedType],
            specialty: [null],
            visit: [null],
            slides: this.fb.array([this.createSlide()])
        });

        this.slideList = this.form.get('slides') as FormArray;
    }

    private loadInitialData() {
        this.visitService.getAll()
            .subscribe(data => {
                this.visits = data;
                this.selectedVisit = data[0]['id'];
            });

        this.specialtyService.getAll()
            .subscribe(data => {
                this.specialties = data;
                this.selectedSpecialty = data[0]['id'];
            });
    }

    private loadAllData() {
        this.presentationService.getAll()
            .subscribe(data => {
                this.presentations = data;
                this.isData = true;
            });
    }

    get slideFormGroup() {
        let slide = this.form.get('slides') as FormArray;
        return slide;
    }

    // updateQuantityImg(event: any) {
    //     const value = event.target.value;
    //     this.quantityImg = value !== '' ? parseInt(value, 10) : 0;
    // }
    //
    // getRange(): number[] {
    //     return Array.from({ length: this.quantityImg }, (_, index) => index);
    // }

    linkFormGroup(i) {
        let slideList = this.form.get('slides') as FormArray;
        return slideList.controls[i].get('linkCoordinats') as FormArray;
    }

    createSlide(): FormGroup {
        let slide = this.fb.group({
            img: [null],
            subSlides: this.fb.array([]),  // SUBSLIDES
            linkCoordinats: this.fb.array([]),
            link: [null],
            linkCenter: [null],
            linkBottom: [null],
            linkBHL: [null],
            linkBHR: [null],
            linkBL: [null],
            linkBC: [null],
            linkBR: [null],
            textLeft: [null],
            textRight: [null]
        });

        this.linkList = slide.get('linkCoordinats') as FormArray;

        return slide;
    }

    // SUBSLIDES
    createSubSlide(): FormGroup {
        let slide = this.fb.group({
            img: [null],
            linkCoordinats: this.fb.array([]),
            link: [null],
            linkCenter: [null],
            linkBottom: [null],
            linkBHL: [null],
            linkBHR: [null],
            linkBL: [null],
            linkBC: [null],
            linkBR: [null],
            textLeft: [null],
            textRight: [null]
        });

        this.linkList = slide.get('linkCoordinats') as FormArray;

        return slide;
    }
    subSlideFormGroup(i) {
        return this.slideList.controls[i].get('subSlides') as FormArray;
    }
    subLinkFormGroup(i, j) {
        let subList = this.slideList.controls[i].get('subSlides') as FormArray;
        return subList.controls[j].get('linkCoordinats') as FormArray;
    }
    addSubLinks(i, j) {
        let subList = this.slideList.controls[i].get('subSlides') as FormArray;
        (subList.controls[j].get('linkCoordinats') as FormArray).push(this.createLinks());
    }
    removeSubLink(i, j) {
        let subList = this.slideList.controls[i].get('subSlides') as FormArray;
        let len = (subList.controls[j].get('linkCoordinats') as FormArray).length;
        (subList.controls[j].get('linkCoordinats') as FormArray).removeAt(len-1);
    }

    addSubSlides(i){
        (this.slideList.controls[i].get('subSlides') as FormArray).push(this.createSubSlide());
    }
    removeSubSlides(i, j) {
        if (this.files[i]?.subSlide) this.files[i].subSlide.splice(j, 1);
        (this.slideList.controls[i].get('subSlides') as FormArray).removeAt(j);
    }
    //END

    createLinks(): FormGroup {
        return this.fb.group({
            mainLinkCoordinats: [null],
            LeftC: [null],
            TopC: [null],
            LinkWidth: [null],
            LinkHeight: [null],
        });
    }

    addSlide() {
        this.slideList.push(this.createSlide());
    }

    removeSlide(index) {
        this.slideList.removeAt(index);
        this.files.splice(index, 1);
    }

    addLinks(i) {
        (this.slideList.controls[i].get('linkCoordinats') as FormArray).push(this.createLinks());
    }

    removeLink(index) {
        let len = (this.slideList.controls[index].get('linkCoordinats') as FormArray).length;
        (this.slideList.controls[index].get('linkCoordinats') as FormArray).removeAt(len-1);
    }

    getSlidesFormGroup(index): FormGroup {
        const formGroup = this.slideList.controls[index] as FormGroup;
        return formGroup;
    }

    onFileSelect(event, index, isSub = null) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            if (isSub != null) {
                if (this.files[index]?.subSlide) this.files[index].subSlide[isSub] = file;
                else {
                    let slides = new Array();
                    slides[isSub] = file
                    this.files[index] = { ...this.files[index], subSlide: slides };
                }
            } else {
                this.files[index] = {...this.files[index], slide: file };
            }
        }
    }

    public selectChangeHandler(event: any, selectedVal) {
        this[selectedVal] = event.target.value;
    }

    isImage(url) {
        return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
    }

    public delete(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.presentationService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Data successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    // Create data request
    public postData($event, form: any) {
        $event.preventDefault();
        const formData: any = new FormData();
        formData.append('title', form.controls.title.value);
        formData.append('type', this.selectedType);
        formData.append('specialty', this.selectedSpecialty);
        formData.append('visit', this.selectedVisit);

        const slides = form.controls.slides.value;
        const sidesReq = [];

        slides.map((slide, i) => {
            sidesReq[i] = {
                'subSlides': slide.subSlides.map(e => Object.assign(e, { img: e.img ? i + e.img.split('\\').reverse()[0] : null })),
                'link': slide.link,
                'linkCenter': slide.linkCenter,
                'linkBottom': slide.linkBottom,
                'linkBHL': slide.linkBHL,
                'linkBHR': slide.linkBHR,
                'linkBL': slide.linkBL,
                'linkBC': slide.linkBC,
                'linkBR': slide.linkBR,
                'textLeft': slide.textLeft,
                'textRight': slide.textRight,
                'linkCoordinats' : slide.linkCoordinats,
            };
        });

        formData.append(`slides`, JSON.stringify(sidesReq));

        for (const file of this.files) {
            if (file['slide']) formData.append(`imgs`, file['slide'], file['slide']['name']);
            //SUBSLIDES
            if (file['subSlide']) {
                for (const subFile of file['subSlide']) {
                    formData.append(`subslides`, subFile, this.files.indexOf(file) + subFile['name']);
                }
                //END
            }
        }

        this.loading = true;
        this.presentationService.create(formData)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.form.reset();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

    // Drag and drop
    drop(event: CdkDragDrop<string[]>, index) {
        if (event.previousIndex !== event.currentIndex) {
            let slides:any = this.presentations[index]['slides'];
            let presentation:Presentation = this.presentations[index];

            moveItemInArray(slides, event.previousIndex, event.currentIndex);

            const slidesSwaped = [...slides];

            const formData: any = new FormData();
            formData.append('title', presentation.title);
            formData.append('type', presentation.type);
            formData.append('visit', +presentation.visit);

            const searchSpecialtyId = (nameKey, specialtiesArray) => {
                for (let i=0; i < specialtiesArray.length; i++) {
                    if (specialtiesArray[i]['specialtyName'] === nameKey) {
                        return specialtiesArray[i]['id'];
                    }
                }
            };
            const specialtyId = searchSpecialtyId(presentation.specialty, this.specialties);

            formData.append('specialty', specialtyId);
            formData.append('slides', JSON.stringify(slidesSwaped));

            this.loading = true;
            this.presentationService.update(formData, this.presentations[index]['id'])
                .subscribe(
                    data => {
                        if (data.id) {
                            this.alertService.success('Action was successfully completed.');
                        } else {
                            this.alertService.error('Error. No response from backend.');
                        }
                        this.loading = false;
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    });
        }
    }
}
