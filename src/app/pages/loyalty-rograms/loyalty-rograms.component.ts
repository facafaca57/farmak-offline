import { Component, OnInit, OnDestroy, HostListener, ElementRef } from '@angular/core';
import { LoyalityPresentation } from "@app/_models";
import { LoyalityPresentationService, UserActivityServiceService } from "@app/_services";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-loyalty-rograms',
  templateUrl: './loyalty-rograms.component.html',
  styleUrls: ['./loyalty-rograms.component.scss']
})
export class LoyaltyRogramsComponent implements OnInit {
    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    presentations: LoyalityPresentation[] = [];
    isData: boolean = false;

    preparatModel: LoyalityPresentation;
    specialtyId: string;

    constructor(
        private presentationService: LoyalityPresentationService,
        private userActivityServiceService: UserActivityServiceService,
        private currentRout: ActivatedRoute,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.loadAllData();
        this.userActivityServiceService.startActivity('Програми лояльності');
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.presentationService.getAll()
            .subscribe(presentations => {
                this.presentations = presentations.reverse();
                this.isData = true;
            });
    }

}
