import { Component, OnInit, OnDestroy, HostListener, ElementRef } from '@angular/core';
import {Preparats} from "@app/_models";
import { PreparatsService, UserActivityServiceService } from "@app/_services";

@Component({
  selector: 'app-marketing-comments',
  templateUrl: './marketing-comments.component.html',
  styleUrls: ['./marketing-comments.component.scss']
})
export class MarketingCommentsComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    preparats: Preparats[] = [];
    isData: boolean = false;

    preparatModel: Preparats;
    specialtyId: string;

    constructor(
        private preparatsService: PreparatsService,
        private userActivityServiceService: UserActivityServiceService,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.loadAllData();
        this.userActivityServiceService.startActivity('Маркетингові коментарі');
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.preparatsService.getAll()
            .subscribe(preparats => {
                this.preparats = preparats.reverse();
                this.preparats.sort((a, b) => a.preparatName.localeCompare(b.preparatName));
                this.isData = true;
            });
    }

}
