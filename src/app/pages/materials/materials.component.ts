import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Paper} from "@app/_models";
import { FilesService, PaperService, UserActivityServiceService } from "@app/_services";

import { environment } from '@environments/environment';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss']
})
export class MaterialsComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @ViewChild('basicModal', { static: false }) basicModal: any;
    link:string = '';

    loading = false;
    papers: Paper[] = [];
    isData: boolean = false;
    uploadsPdf = environment.uploadsPdf;

  constructor(
      private paperService: PaperService,
      private userActivityServiceService: UserActivityServiceService,
      private elementRef: ElementRef,
      private files: FilesService
  ) { }

    ngOnInit() {
        this.loadAllData();
        this.userActivityServiceService.startActivity('Матеріали');
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.paperService.getAll()
            .subscribe(data => {
                this.papers = data;
                this.isData = true;
            });
    }

    showModal(e, link) {
        e.preventDefault();
        let fileName = link.split('/').splice(-1);
        this.files.openPDF(fileName);
    }
}
