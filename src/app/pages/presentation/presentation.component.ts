import { Component, OnInit, ViewChild, HostListener, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Presentation } from "@app/_models";
import { FilesService, PresentationService, UserActivityServiceService } from "@app/_services";
import { environment } from '@environments/environment';
import { swiperConfig } from '@app/_configs/swiper.conf';
@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit, OnDestroy {
    @ViewChild('basicModal', { static: false }) basicModal: any;
    link:string = '';

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    presentationId: string;
    presentation: Presentation;
    presentations: Presentation[] = [];
    isData: boolean = false;
    uploadsImg:string = environment.uploadsImg;
    uploadsPdf:string = environment.uploadsPdf;
    strStartActivity: string;
    type: string;

    public config = swiperConfig;

  constructor(
      private presentationService: PresentationService,
      private userActivityServiceService: UserActivityServiceService,
      private currentRout: ActivatedRoute,
      private elementRef: ElementRef,
      private files: FilesService,
    ) {
        currentRout.params.subscribe(val => {
            this.presentationId = this.currentRout.snapshot.paramMap.get('presentationId');
            this.loadAllData();
        });
    }

    ngOnInit() {}
    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.presentationService.getById(this.presentationId)
            .subscribe(presentation => {
                this.presentation = presentation;

                this.presentation.slides.forEach((el, i) => {
                    this.presentation.slides[i].subSlides.length ? this.presentation.slides[i].subSlides.unshift(el) : null
                })

                this.isData = true;
                this.strStartActivity = 'Презентація ' + presentation.title + '. Візит ' + presentation.visit.serialNumber + '. Спеціальність ' + presentation.specialty.specialtyName;
                this.userActivityServiceService.startActivity(this.strStartActivity);
            }, (err) => {
                console.log(err);
            });
    }

    setMyStyles(linkCoordinat) {
        let styles = {
          'top': linkCoordinat.TopC + '%',
          'left': linkCoordinat.LeftC + '%',
          'width': linkCoordinat.LinkWidth + '%',
          'height': linkCoordinat.LinkHeight + '%',
        };
        return styles;
    }

    showModal(e, link) {
        e.preventDefault();
        let fileName = link.split('/').splice(-1);
        this.files.openPDF(fileName);
    }

    checkTheYoutube(link) {
        if(link) {
            if(~link.indexOf('youtu.be') || ~link.indexOf('youtube')) return true
            else return false;
        } else return false;

    }

    getUrlFromYT(url) {
        return `https://farmak-rx2.com/public/uploads/yt/${this.getYoutubeUrl(url)}.mp4`;
    }

    getYoutubeUrl(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);

        const id = (match && match[2].length === 11)
          ? match[2]
          : null;
        
        return id;
    }
}
