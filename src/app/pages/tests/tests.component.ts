import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AlertService, TestService} from "@app/_services";
import {Test} from "@app/_models";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.scss']
})
export class TestsComponent implements OnInit, OnDestroy {
    subs: Subscription;
    registrationForm: FormGroup;
    model: any = {};
    loading = false;
    tests: Test[] = [];
    isData = false;
    testExample = `[
    {"question":"Вопрос 1 ?","answers":[{"answer":"Текст 11 варианта","correct":false},{"answer":"Текст 12 варианта","correct":true}]},
    {"question":"Вопрос 2 ?","answers":[{"answer":"Текст 21 варианта","correct":false},{"answer":"Текст 22 варианта","correct":true}]}
    ]`;

    constructor(
        private router: Router,
        private testService: TestService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
    ) {
        this.registrationForm = formBuilder.group({
            title: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
            duration: ['', [Validators.required, Validators.minLength(1)]],
            breakDuration: ['', [Validators.required, Validators.minLength(1)]],
            videoUrl: ['', [Validators.required]],
            videoDuration: ['', [Validators.required]],
            poster: ['', [Validators.required]],
            QA: ['', [Validators.required]],
        });
    }

    ngOnInit() {
        this.loadAll();
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }


    public delete(id) {
        if (window.confirm('Do you really wanna delete?')) {
            this.loading = true;
            this.testService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('successfully deleted');
                        this.loadAll();
                    } else {
                        this.alertService.error('Can\'t delete');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    private loadAll() {
        this.subs = this.testService.getAll()
            .subscribe(tests => {
                const newTests = tests
                    .map(test => {
                        test.QA = JSON.stringify(test.QA);
                        return test;
                    });
                this.tests = newTests;
                this.isData = true;
            });
    }


    public postData(registrationForm: any) {
        this.model = {
            title: registrationForm.controls.title.value,
            duration: +registrationForm.controls.duration.value,
            breakDuration: +registrationForm.controls.breakDuration.value,
            videoUrl: registrationForm.controls.videoUrl.value,
            videoDuration: +registrationForm.controls.videoDuration.value,
            poster: registrationForm.controls.poster.value,
            QA: registrationForm.controls.QA.value,
        };

        this.loading = true;
        this.testService.create(this.model)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Successfully completed.');
                        this.registrationForm.reset();
                        this.loadAll();
                    } else {
                        this.alertService.error('Error. Please try later.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
