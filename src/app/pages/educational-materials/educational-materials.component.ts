import { Component, OnInit, OnDestroy, HostListener, ElementRef } from '@angular/core';
import { UserActivityServiceService } from "@app/_services";

@Component({
  selector: 'app-educational-materials',
  templateUrl: './educational-materials.component.html',
  styleUrls: ['./educational-materials.component.scss']
})
export class EducationalMaterialsComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    constructor(
        private userActivityServiceService: UserActivityServiceService,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.userActivityServiceService.startActivity('Навчальні матеріали');
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

}
