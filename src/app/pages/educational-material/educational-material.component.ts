import {Component, ElementRef, OnInit, ViewChild, AfterViewInit, HostListener, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { UserActivityServiceService } from "@app/_services";

@Component({
  selector: 'app-educational-material',
  templateUrl: './educational-material.component.html',
  styleUrls: ['./educational-material.component.scss']
})
export class EducationalMaterialComponent implements OnInit, AfterViewInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @ViewChild('videoWrap', { static: false }) videoWrap: ElementRef;

    materialId = '';
    player;

    constructor(
        private userActivityServiceService: UserActivityServiceService,
        private currentRout: ActivatedRoute,
        private elementRef: ElementRef
    ) {
        this.materialId = this.currentRout.snapshot.paramMap.get('materialId');
    }

    ngOnInit() {this.userActivityServiceService.startActivity('Навчальний матеріал'); }
    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    ngAfterViewInit() {
        this.startVideo();
    }

    startVideo(vidCurrentTime = 0) {
        const offsetWidth = +this.videoWrap.nativeElement.offsetWidth - 30;
        const videoId = this.materialId;
        this.player = new window['YT'].Player('player', {
            videoId,
            width: 10 + offsetWidth,
            height: offsetWidth / 16 * 9,
                playerVars: { 'autoplay': 0, 'controls': 1 },
            events: {
                'onStateChange': this.onPlayerStateChange.bind(this),
                'onError': this.onPlayerError.bind(this),
                'onReady': this.onPlayerReady.bind(this)
            }
        });
    }

    onPlayerReady(event) {
        // event.target.playVideo();
    }

    onPlayerStateChange(event) {
        switch (event.data) {
            case window['YT'].PlayerState.PLAYING:
                if (this.cleanTime() === 0) {
                    // console.log('started ' + this.cleanTime());
                } else {
                    // console.log('playing ' + this.cleanTime());
                }
                break;
            case window['YT'].PlayerState.PAUSED:
                // this.player.playVideo();
                break;
            case window['YT'].PlayerState.ENDED:
                // this.player = null;
                break;
        }
    }

    cleanTime() {
        if (this.player) {
            return Math.round(this.player.getCurrentTime());
        }
    }

    onPlayerError(event) {
        console.log('break');
    }

}
