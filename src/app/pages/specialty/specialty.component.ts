import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Specialty, Presentation, Visit } from "@app/_models";
import { SpecialtyService, PresentationService, VisitService, UserActivityServiceService } from "@app/_services";
import { environment } from '@environments/environment';

@Component({
  selector: 'app-specialty',
  templateUrl: './specialty.component.html',
  styleUrls: ['./specialty.component.scss']
})
export class SpecialtyComponent implements OnInit, OnDestroy {
    @ViewChild('carousel', { static: false }) carousel: any;

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @HostListener('swipeleft', ['$event']) public swipePrev(event: any) {
        this.carousel.nextSlide();
    };

    @HostListener('swiperight', ['$event']) public swipeNext(event: any) {
        this.carousel.previousSlide();
    };

    loading = false;
    presentations: Presentation[] = [];
    isData: boolean = false;
    uploadsImg = environment.uploadsImg;
    specialtyId: string;
    specialty: Specialty;
    visits: Visit[] = [];
    doubleArrayOfVisits: Visit[][] = [[]];
    visitsSlides:Visit[][] = [[]];
    publicFolder = environment.publicFolder;

    constructor(
        private visitService: VisitService,
        private presentationService: PresentationService,
        private specialtyService: SpecialtyService,
        private userActivityServiceService: UserActivityServiceService,
        private currentRout: ActivatedRoute,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.specialtyId = this.currentRout.snapshot.paramMap.get('specialtyId');
        this.loadAllData();
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
      }

    private loadAllData() {
        this.specialtyService.getById(this.specialtyId)
            .subscribe(specialty => {
                this.specialty = specialty;
                this.userActivityServiceService.startActivity(this.specialty.specialtyName);
            });

        this.presentationService.getByQuery(`specialty=${this.specialtyId}`)
            .subscribe(presentations => {
                this.presentations = presentations;
                this.isData = true;
            });

        this.visitService.getAll()
            .subscribe(visits => {
                // this.visits = visits.sort((a, b) => b.id - a.id);
                this.visits = visits.sort((a, b) => b.id - a.id).slice(0, 1);
                // this.doubleArrayOfVisits = visits.concat(visits).sort((a, b) => b.id - a.id);

                let chunkSize: number = 5;
                for (let i = 0; i < Math.ceil(this.visits.length / chunkSize); i++) {
                    this.visitsSlides[i] = this.visits.slice((i * chunkSize), (i * chunkSize) + chunkSize);
                }

                for (let i = 0; i < this.visitsSlides.length; i++) {
                    this.doubleArrayOfVisits[i] = this.visitsSlides[i].concat(this.visitsSlides[i]).sort((a, b) => b.id - a.id);
                }
            });


    }

    public changeWeekFormat(week) {
        let weekArr = week.split(' | ');

        if(weekArr[1]) {
            week = '<span class="serialNumber">' + weekArr[0] + '</span><br /><span class="serialNumber">' + weekArr[1] + '</span>';
        }

        return week;
    }

    public checkType(index) {
        let type = 'forCustomers';

        if (index % 2 === 0) {
            type = 'forUsers';
        }
        return type;
    }

    public presentationId(visitId, index) {
        const expectType = this.checkType(index);

        for (let i = 0; i < this.presentations.length; i++) {
            if (this.presentations[i]['visit'] === +visitId && this.presentations[i]['type'] === expectType) {
                return this.presentations[i]['id'];
            }
        }
        return null;
    }


}
