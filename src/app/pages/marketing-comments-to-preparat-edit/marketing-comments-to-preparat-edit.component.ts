import { Component, OnInit, HostListener, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MarketingCommentsService, PreparatsService, UserActivityServiceService, AuthenticationService, AlertService } from "@app/_services";
import { MarketingComments, Preparats, User } from '@app/_models';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-marketing-comments-to-preparat-edit',
  templateUrl: './marketing-comments-to-preparat-edit.component.html',
  styleUrls: ['./marketing-comments-to-preparat-edit.component.scss']
})
export class MarketingCommentsToPreparatEditComponent implements OnInit, OnDestroy {

        @HostListener('window:beforeunload')
        beforeunload() {
            this.userActivityServiceService.endActivity();
        }

        currentUser: User;
        loading = false;
        preparat: Preparats;
        preparatId: string;
        isData: boolean = false;
        strStartActivity: string;
        type: string;
        dateStart: Date;
        dateStartStr: string;
        commentForm: FormGroup;
        comment: MarketingComments;
        commentId: string;
        isActivity = false;

        constructor(
            private userActivityServiceService: UserActivityServiceService,
            private marketingCommentsService: MarketingCommentsService,
            private preparatsService: PreparatsService,
            private currentRout: ActivatedRoute,
            private authenticationService: AuthenticationService,
            private alertService: AlertService,
            private formBuilder: FormBuilder,
            private elementRef: ElementRef
        ) {
            this.authenticationService.currentUser.subscribe(x => {
                this.currentUser = x;
            });

            this.commentId = this.currentRout.snapshot.paramMap.get('commentId');
            this.preparatId = this.currentRout.snapshot.paramMap.get('preparatId');

            this.commentForm = this.formBuilder.group({
                comment: [''],
            });

            this.loadAllData();
        }

        ngOnInit() {  }
        ngOnDestroy() {
            this.userActivityServiceService.endActivity();
            this.elementRef.nativeElement.remove();
        }

        private loadAllData() {
            this.marketingCommentsService.getById(this.commentId)
                .subscribe(comment => {
                    this.comment = comment;
                    this.commentForm.get('comment').setValue(comment.comment);
                });
            this.preparatsService.getById(this.preparatId)
                .subscribe(preparat => {
                    this.preparat = preparat;
                    if(!this.isActivity) this.userActivityServiceService.startActivity('Редагування коментаря до препарату ' + preparat.preparatName);
                    this.isActivity = true;
                });
        }

        public postData(commentForm: any) {
            const comment: MarketingComments = new MarketingComments;
            comment.comment = commentForm.controls.comment.value;
            comment.date = this.comment.date;
            comment.user = this.currentUser.id;
            comment.preparat = this.preparatId;

            this.loading = true;
            this.marketingCommentsService.update(comment, this.commentId).subscribe(data => {
                if (data.id) {
                    this.alertService.success('Action was successfully completed.');
                    this.commentForm.reset();
                    this.loadAllData();
                } else {
                    this.alertService.error('Error. No response from backend.');
                }
                this.loading = false;
            }, error => {
                this.alertService.error(error);
                this.loading = false;
            });
        }

    }
