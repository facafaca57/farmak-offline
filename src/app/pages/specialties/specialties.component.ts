import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Specialty} from "@app/_models";
import {AlertService, SpecialtyService} from "@app/_services";
import {Router} from "@angular/router";
import { environment } from '@environments/environment';


@Component({
  selector: 'app-specialties',
  templateUrl: './specialties.component.html',
  styleUrls: ['./specialties.component.scss']
})
export class SpecialtiesComponent implements OnInit, OnDestroy {
    registrationForm: FormGroup;
    loading = false;
    specialties: Specialty[] = [];
    isData: boolean = false;
    timer: any;
    uploadsImg = environment.uploadsImg;

    constructor(
        private router: Router,
        private specialtyService: SpecialtyService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
    ) {
        this.registrationForm = formBuilder.group({
            specialtyName: ['', [Validators.required]],
            logo: ['', [Validators.required]],
            orderNumber: ['', [Validators.required]],
        });
    }

    ngOnInit() {
        this.loadAllData();
    }

    ngOnDestroy() {
        clearTimeout(this.timer);
    }

    private loadAllData() {
        this.specialtyService.getAll()
            .subscribe(specialties => {
                this.specialties = specialties;
                this.isData = true;
            });
    }

    onFileSelect(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.registrationForm.get('logo').setValue(file);
        }
    }

    public deleteData(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.specialtyService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    public postData(registrationForm: any) {
        const formData = new FormData();
        formData.append('logo', registrationForm.get('logo').value);
        formData.append('specialtyName', registrationForm.controls.specialtyName.value);

        const orderN = registrationForm.controls.orderNumber.value;
        const index = this.specialties.findIndex(e => e.orderNumber == orderN)
        if (index != -1) {
            this.alertService.error('Order number already exists!');
            this.registrationForm.get('orderNumber').setErrors({order: 'exists'});
            return;
        }

        formData.append('orderNumber', orderN);

        this.loading = true;
        this.specialtyService.create(formData)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Registration was successfully completed.');
                        this.registrationForm.reset();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. Please try later.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
