import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Presentation, Specialty, Visit} from "@app/_models";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, PresentationService, SpecialtyService, VisitService} from "@app/_services";
import { environment } from '@environments/environment';

@Component({
  selector: 'app-edit-visit',
  templateUrl: './edit-visit.component.html',
  styleUrls: ['./edit-visit.component.scss']
})
export class EditVisitComponent implements OnInit {

    public form: FormGroup;
    loading = false;
    visit: Visit;
    visitId: number;

    constructor(
        private router: Router,
        private visitService: VisitService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.visitId = +this.currentRout.snapshot.paramMap.get('visitId');

        this.form = this.fb.group({
            serialNumber: [null],
            year: [null],
            week: [null],
        });

        this.loadInitialData();
    }


    ngOnInit() {}


    private loadInitialData() {
        this.visitService.getById(this.visitId)
            .subscribe(data => {
                this.visit = data;
                this.setFormData();
            });
    }

    private setFormData() {
        this.form.get('serialNumber').setValue(this.visit.serialNumber);
        this.form.get('year').setValue(this.visit.year);
        this.form.get('week').setValue(this.visit.week);
    }


    public postData($event, form: any) {
        $event.preventDefault();

        const formData: Visit = {
            serialNumber: +form.controls.serialNumber.value,
            year: +form.controls.year.value,
            week: form.controls.week.value,
        };

        this.loading = true;
        this.visitService.update(formData, this.visitId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.visit = data;
                        this.form.reset();
                        this.setFormData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
