import { Component, OnInit, HostListener, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MarketingCommentsService, PreparatsService, UserActivityServiceService, AuthenticationService, AlertService, UpdateService } from "@app/_services";
import { MarketingComments, Preparats, User } from '@app/_models';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-marketing-comments-to-preparat',
  templateUrl: './marketing-comments-to-preparat.component.html',
  styleUrls: ['./marketing-comments-to-preparat.component.scss']
})
export class MarketingCommentsToPreparatComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    currentUser: User;
    loading = false;
    preparat: Preparats;
    preparatId: string;
    preparatName: string;
    isData: boolean = false;
    strStartActivity: string;
    type: string;
    dateStart: Date;
    dateStartStr: string;
    commentForm: FormGroup;
    comments: MarketingComments[] = [];
    isActivity = false;

    constructor(
        private userActivityServiceService: UserActivityServiceService,
        private marketingCommentsService: MarketingCommentsService,
        private preparatsService: PreparatsService,
        private currentRout: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private elementRef: ElementRef,
        private update: UpdateService
    ) {
        this.authenticationService.currentUser.subscribe(x => {
            this.currentUser = x;
        });

        currentRout.params.subscribe(val => {
            this.preparatId = this.currentRout.snapshot.paramMap.get('preparatId');
            this.loadAllData();
        });

        this.commentForm = this.formBuilder.group({
            comment: ['', [Validators.required]],
        });
    }

    ngOnInit() {  }
    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private formatDate(date) {
        var d = new Date(date),
            minutes = '' + d.getMinutes(),
            hour = '' + d.getHours(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        if (hour.length < 2)
            hour = '0' + hour;
        if (minutes.length < 2)
            minutes = '0' + minutes;

        return [year, month, day].join('-') + '  ' + hour + ':' + minutes;
    }

    private loadAllData() {
        this.dateStart = new Date();
        this.dateStartStr = this.formatDate(this.dateStart);

        this.marketingCommentsService.getByQuery(`preparat=${this.preparatId}`)
            .subscribe(comments => {
                this.comments = comments;
            });

        this.preparatsService.getById(this.preparatId)
            .subscribe(preparat => {
                this.preparat = preparat;
                this.preparatName = this.preparat.preparatName ? this.preparat.preparatName : '';
                if(!this.isActivity)
                this.userActivityServiceService.startActivity('Маркетингові коментарі. Препарат ' + this.preparat.preparatName);
                this.isActivity = true;

            });

    }

    public postData(commentForm: any) {
        const comment: MarketingComments = new MarketingComments;
        comment.comment = commentForm.controls.comment.value;
        comment.date = this.dateStartStr;
        comment.user = this.currentUser.id;
        comment.preparat = this.preparatId;

        this.loading = true;
        this.marketingCommentsService.create(comment).subscribe(
            data => {
                if (data.id) {
                    this.alertService.success('Comment was successfully created.');
                    this.commentForm.reset();
                    this.loadAllData();
                    this.authenticationService.network.then(({connected}) => {
                        if (connected) {
                            this.update.uploadNewComments();
                        }
                    });
                } else {
                    this.alertService.error('Error. Please try later.');
                }
                this.loading = false;
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }

    public delete(id) {
        if (window.confirm("Do you really want to delete?")) {
            this.loading = true;
            this.marketingCommentsService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Data successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

}
