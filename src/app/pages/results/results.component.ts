import { Component, ElementRef, HostListener, OnDestroy, OnInit } from '@angular/core';
import {AuthenticationService, UserService, UserActivityServiceService} from "@app/_services";
import {User} from "@app/_models";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit, OnDestroy {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    userId: string | number;
    user: User;

    constructor(
      private authenticationService: AuthenticationService,
      private userActivityServiceService: UserActivityServiceService,
      private userService: UserService,
      private elementRef: ElementRef
    ) {
      const {id} = this.authenticationService.currentUserValue;
      this.userId = id;
    }

    ngOnInit() {
        console.log(this.userId);
        this.userService.getById(this.userId)
            .subscribe(user => {
                this.user = user;
            });

        this.userActivityServiceService.startActivity('Сторінка Результати');
    }

    ngOnDestroy() {
      this.userActivityServiceService.endActivity();
      this.elementRef.nativeElement.remove();
    }

}
