import {Component, OnInit, OnDestroy, ViewChild, HostListener, ElementRef} from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Question } from "@app/_models";
import { FilesService, QuestionService, UserActivityServiceService } from "@app/_services";
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';
import { interval, Subscription } from 'rxjs';
import { swiperConfig } from '@app/_configs/swiper.conf';

@Component({
  selector: 'app-question-content',
  templateUrl: './question-content.component.html',
  styleUrls: ['./question-content.component.scss']
})
export class QuestionContentComponent implements OnInit, OnDestroy {
    link:string = '';

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    questionId: string;
    question: Question;

    loading = false;
    isData: boolean = false;
    uploadsImg:string = environment.uploadsImg;
    uploadsPdf:string = environment.uploadsPdf;

    presentationLinks: any[] = [];
    slideIndex = 0;

    public config = swiperConfig;

    private _trialEndsAt;
    private _diff: number;
    private _days: number;
    private _hours: number;
    private _minutes: number;
    private _seconds: number;
    private days: string;
    private hours: string;
    private minutes: string;
    private seconds: string;
    private interv: Subscription;

  constructor(
      private currentRout: ActivatedRoute,
      private userActivityServiceService: UserActivityServiceService,
      private questionService: QuestionService,
      private elementRef: ElementRef,
      private files: FilesService
  ) {
      currentRout.params.subscribe(val => {
          this.questionId = this.currentRout.snapshot.paramMap.get('questionId');
          this.loadAllData();
      });
  }

    ngOnInit() {}

    ngOnDestroy() {
        if(this.interv) {
            this.interv.unsubscribe();
        }
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    getDays(t) {
        return Math.floor( t / (1000 * 60 * 60 * 24) );
    }

    getHours(t) {
        return Math.floor( (t / (1000 * 60 * 60)) % 24 );
    }

    getMinutes(t) {
        return Math.floor( (t / 1000 / 60) % 60 );
    }

    getSeconds(t) {
        return Math.floor( (t / 1000) % 60 );
    }

    addZeros(n, needLength = null) {
        needLength = needLength || 2;
        n = String(n);
        while (n.length < needLength) {
          n = "0" + n;
        }
        return n
      }

    decriseTime(time) {
        this._trialEndsAt = time;
         this.interv = interval(1000).pipe(
            map((x) => {this._diff = Date.parse(this._trialEndsAt) - Date.parse(new Date().toString());
              })).subscribe((x) => {
                  this._days = this.getDays(this._diff);
                  this._hours = this.getHours(this._diff);
                  this._minutes = this.getMinutes(this._diff);
                  this._seconds = this.getSeconds(this._diff);

                  this.days = this.addZeros(this._days);
                  this.hours = this.addZeros(this._hours);
                  this.minutes = this.addZeros(this._minutes);
                  this.seconds = this.addZeros(this._seconds);

              });

        return '<span>' + this.days + '<span>днів</span></span>:<span>' + this.hours + '<span>годин</span></span>:<span>' + this.minutes + '<span>хвилин</span></span>:<span>' + this.seconds + '<span>секунд</span></span>';
    }

    private loadAllData() {
        this.questionService.getById(this.questionId)
            .subscribe(question => {
                this.question = question;
                this.isData = true;
                this.userActivityServiceService.startActivity(this.question.title);
            });
    }

    showModal(e, link) {
        e.preventDefault();
        let fileName = link.split('/').splice(-1);
        this.files.openPDF(fileName);
    }

    checkTheYoutube(link) {
        if(link) {
            if(~link.indexOf('youtu.be') || ~link.indexOf('youtube')) return true
            else return false;
        } else return false;

    }

    getUrlFromYT(url) {
        return `https://farmak-rx2.com/public/uploads/yt/${this.getYoutubeUrl(url)}.mp4`;
    }

    getYoutubeUrl(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);

        const id = (match && match[2].length === 11)
          ? match[2]
          : null;

        return id;
    }

}
