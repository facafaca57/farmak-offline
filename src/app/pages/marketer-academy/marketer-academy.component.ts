import { Component, OnInit, OnDestroy, HostListener, ElementRef } from '@angular/core';
import {Preparats} from "@app/_models";
import { PreparatsService, UserActivityServiceService } from "@app/_services";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-marketer-academy',
  templateUrl: './marketer-academy.component.html',
  styleUrls: ['./marketer-academy.component.scss']
})
export class MarketerAcademyComponent implements OnInit {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    preparats: Preparats[] = [];
    isData: boolean = false;

    preparatModel: Preparats;
    specialtyId: string;

    constructor(
        private preparatsService: PreparatsService,
        private userActivityServiceService: UserActivityServiceService,
        private currentRout: ActivatedRoute,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.loadAllData();
        this.userActivityServiceService.startActivity('Актуальне');
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.preparatsService.getAll()
            .subscribe(preparats => {
                this.preparats = preparats.sort((a, b) => a.preparatName.localeCompare(b.preparatName));
                this.isData = true;
            });
    }

}
