import {Component, OnInit, ViewChild, HostListener, OnDestroy, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoyalityPresentation} from "@app/_models";
import { FilesService, LoyalityPresentationService, UserActivityServiceService } from "@app/_services";
import { environment } from '@environments/environment';
import { NguCarousel, NguCarouselStore, NguCarouselConfig } from '@ngu/carousel';
import { FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-loyality-presentation',
  templateUrl: './loyality-presentation.component.html',
  styleUrls: ['./loyality-presentation.component.scss']
})
export class LoyalityPresentationComponent implements OnInit, OnDestroy {
    @ViewChild('carousel', { static: false }) carousel: any;
    @ViewChild('visitsCarousel', { static: false }) visitsCarousel: any;
    @ViewChild('basicModal', { static: false }) basicModal: any;
    link:string = '';

    @ViewChild('myCarousel', { static: false }) myCarousel: NguCarousel<any>;

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    @HostListener('swipeleft', ['$event']) public swipePrev(event: any) {
        this.carousel.nextSlide();
        this.visitsCarousel.nextSlide();
    }

    @HostListener('swiperight', ['$event']) public swipeNext(event: any) {
        this.carousel.previousSlide();
        this.visitsCarousel.previousSlide();
    }

    public form: FormGroup;
    loading = false;
    presentationId: string;
    presentation: LoyalityPresentation;
    presentations: LoyalityPresentation[] = [];
    isData: boolean = false;
    uploadsImg:string = environment.uploadsImg;
    uploadsPdf:string = environment.uploadsPdf;
    specialtyId: string;
    doctorsName: string;
    strStartActivity: string;
    presentationLinks: any[] = [];
    slideIndex = 0;
    public carouselConfig: NguCarouselConfig = {
        grid: { xs: 6, sm: 6, md: 6, lg: 6, all: 0 },
        slide: 1,
        speed: 250,
        point: {
            visible: true
        },
        load: 2,
        velocity: 0,
        touch: true,
        vertical: {
            enabled: true,
            height: 606
        },
        easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

  constructor(
      private presentationService: LoyalityPresentationService,
      private userActivityServiceService: UserActivityServiceService,
      private currentRout: ActivatedRoute,
      private fb: FormBuilder,
      private router: Router,
      private elementRef: ElementRef,
      private files: FilesService
  ) {
      currentRout.params.subscribe(val => {
          this.presentationId = this.currentRout.snapshot.paramMap.get('loyality-presentationId');

          this.loadAllData();
      });
  }

    ngOnInit() {
        this.form = this.fb.group({
            doctorsName: ['', [Validators.required]]
        });
    }
    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    onmoveFn(data: NguCarouselStore) {
    }

    /*moveLeft() {
      if (this.slideIndex > 0) {
          this.myCarousel.moveTo(--this.slideIndex, false);
      }
    }

    moveRight() {
        if (this.slideIndex > ) {
            this.slideIndex = 0;
            this.myCarousel.moveTo(this.slideIndex, false);
        } else {
            this.myCarousel.moveTo(++this.slideIndex, false);
        }
    }*/

    private loadAllData() {
        this.presentationService.getById(this.presentationId)
            .subscribe(presentation => {
                this.presentation = presentation;
                this.isData = true;
                this.strStartActivity = 'Програма лояльності. Презентація ' + presentation.title;
                this.userActivityServiceService.startActivity(this.strStartActivity);
            }, (err) => {
                console.log(err);
            });
    }

    showModal(e, link) {
        e.preventDefault();
        let fileName = link.split('/').splice(-1);
        this.files.openPDF(fileName);
    }

    checkTheYoutube(link) {
        if(link) {
            if(~link.indexOf('youtu.be') || ~link.indexOf('youtube')) return true
            else return false;
        } else return false;

    }

    getUrlFromYT(url) {
        return `https://farmak-rx2.com/public/uploads/yt/${this.getYoutubeUrl(url)}.mp4`;
    }

    getYoutubeUrl(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);

        const id = (match && match[2].length === 11)
          ? match[2]
          : null;

        return id;
    }

    presentationTest(event:any, presentationCarousel) {
        event.preventDefault();
        event.target.classList.add('active');
        setTimeout(function(){
            presentationCarousel.nextSlide();
        }, 1000);
        setTimeout(function(){
            event.target.classList.remove('active');
        }, 2000);
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const rout = this.router;
        const doctorsName = form.controls.doctorsName.value;

        const newStrStartActivity = this.strStartActivity + '. Лікар: ' + doctorsName;
        this.userActivityServiceService.changeActivity(newStrStartActivity);
        this.form.reset();
        setTimeout(function(){
            rout.navigate(['/']);
        }, 3000);
    }
}
