import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService, AlertService  } from '@app/_services';
import { User } from '@app/_models';

@Component({
  selector: 'app-user-activity',
  templateUrl: './user-activity.component.html',
  styleUrls: ['./user-activity.component.scss']
})
export class UserActivityComponent implements OnInit, OnDestroy {

    loading = false;
    user: User;
    userID: string;

    constructor(
        private userService: UserService,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.userID = this.currentRout.snapshot.paramMap.get('userID');
    }

    ngOnInit() {this.loadInitialData();}

    ngOnDestroy() {}

    private loadInitialData() {
        this.userService.getById(this.userID)
            .subscribe(data => {
                this.user = data;
                // this.checkTheActivity ();
            });

    }

    // checkTheActivity () {
    //     let activity = this.user.activityPages;
    //     // let j = 0;
    //     // let newActivity = [];
    //     // for(let i = 0; i< activity.length; i++) {
    //     //     if(activity[i].id) {
    //     //         newActivity[j] = activity[i];
    //     //         j++;
    //     //     }
    //     // }
    //     // this.user.activity = newActivity;
    // }
}
