import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { UserService, AlertService  } from '@app/_services';
import { User } from '@app/_models';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
    registrationForm: FormGroup;
    userModel: any = {};
    role: string = 'user';
    loading = false;
    users: User[] = [];
    isUsers: boolean = false;
    timer: any;
    dates: string[] = [];
    thisDate: string;

    constructor(
        private router: Router,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
    ) {
        this.registrationForm = formBuilder.group({
            username: ['', Validators.compose([Validators.required, Validators.maxLength(30),
                Validators.minLength(1)])],
            login: ['', [Validators.required, Validators.minLength(1)]],
            password: ['', [Validators.required, Validators.minLength(4)]],
            role: ['', [Validators.required]]
        });

        this.thisDate = moment(new Date()).format('Y-MM-DD');

    }

    ngOnInit() {
        this.setRoleToForm();
        this.loadAllUsers();
    }

    ngOnDestroy() {
        clearTimeout(this.timer);
    }


    public deleteUser(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.userService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('User successfully deleted');
                        this.loadAllUsers();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    private loadAllUsers() {
        this.userService.getAll()
            .subscribe(users => {
                const usersNoAdmin = users.filter(user => user.role === 'user')
                    .map(user => {
                        if (user.activityPages !== undefined && user.activityPages.length) {
                            user.activityPages.forEach((el) => {
                                let newDate = moment(el.dateStart).format('Y-MM-DD');
                                el.dateStart = this.checkArrayToOneMonth(newDate);
                            });
                            this.unique(user.activityPages);
                        }
                        return user;
                    });
                this.users = usersNoAdmin;
                this.isUsers = true;
            });


    }

    private unique(arr) {
        let result = [];
        let dateStarts = [];

        arr.forEach((el) => {
            dateStarts.push(el.dateStart);
        });

        for (let str of dateStarts) {
          if (!result.includes(str)) {
            result.push(str);
          }
        }
        dateStarts = result;
        dateStarts.forEach((el, i) => {
            arr[i].dateStart = el;
        });

        arr.splice(dateStarts.length, (arr.length - dateStarts.length));
    };

    private checkArrayToOneMonth(el) {
        let oneMonthEarly = Date.parse(this.thisDate) - 2592000000;
        if(Date.parse(el) > oneMonthEarly) return el; else return null;
    }

    private setRoleToForm() {
        this.registrationForm.patchValue({
            role: this.role
        });
    }

    public postData(registrationForm: any) {
        this.userModel = {
            username: registrationForm.controls.username.value,
            login: registrationForm.controls.login.value,
            password: registrationForm.controls.password.value,
            role: registrationForm.controls.role.value,
        };

        this.loading = true;
        this.userService.create(this.userModel)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Registration was successfully completed.');
                        this.registrationForm.reset();
                        this.setRoleToForm();
                        this.loadAllUsers();
                    } else {
                        this.alertService.error('Error. Please try later.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
