import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, UserService} from "@app/_services";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
    public form: FormGroup;
    loading = false;
    user: any;
    userID: string;

    constructor(
        private router: Router,
        private userService: UserService,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.userID = this.currentRout.snapshot.paramMap.get('userID');

        this.form = this.fb.group({
            resultsImg: [null]
        });

        this.loadInitialData();
    }


    ngOnInit() {}


    private loadInitialData() {
        this.userService.getById(this.userID)
            .subscribe(data => {
                this.user = data;
                this.setFormData();
            });
    }

    private setFormData() {
        this.form.get('resultsImg').setValue(this.user.resultsImg);
    }


    public postData($event, form: any) {
        $event.preventDefault();

        const formData = {
            resultsImg: form.controls.resultsImg.value,
        };

        this.loading = true;
        this.userService.updateResultsImg(formData, this.userID)
            .subscribe(
                data => {
                    if (data) {
                        this.alertService.success('Action was successfully completed.');
                        this.user.resultsImg = form.controls.resultsImg.value;
                        this.form.reset();
                        this.setFormData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
