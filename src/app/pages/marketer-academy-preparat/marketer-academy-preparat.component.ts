import { Component, OnInit, HostListener, ViewChild, ElementRef} from '@angular/core';
import { marketerAcademy, Preparats} from "@app/_models";
import { PreparatsService, marketerAcademyService, UserActivityServiceService, FilesService } from "@app/_services";
import { ActivatedRoute } from '@angular/router';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-marketer-academy-preparat',
  templateUrl: './marketer-academy-preparat.component.html',
  styleUrls: ['./marketer-academy-preparat.component.scss']
})
export class MarketerAcademyPreparatComponent implements OnInit {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }
    @ViewChild('basicModal', { static: false }) basicModal: any;
    link:string = '';

    marketerAcademys: marketerAcademy[] = [];
    preparat: Preparats;
    isData: boolean = false;

    marketerAcademyModel: marketerAcademy;
    specialtyId: string;
    preparatId: string;
    type: string;
    uploadsPdf:string = environment.uploadsPdf;

    constructor(
        private marketerAcademyService: marketerAcademyService,
        private preparatService: PreparatsService,
        private userActivityServiceService: UserActivityServiceService,
        private currentRout: ActivatedRoute,
        private files: FilesService,
        private elementRef: ElementRef
    ) {}

    ngOnInit() {
        this.preparatId = this.currentRout.snapshot.paramMap.get('preparatId');
        this.loadAllData();
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    private loadAllData() {
        this.preparatService.getById(this.preparatId)
            .subscribe(preparat => {
                this.preparat = preparat;
                this.isData = true;
                this.userActivityServiceService.startActivity(this.preparat.preparatName);
            });

        this.marketerAcademyService.getByQuery(`preparat=${this.preparatId}`)
            .subscribe(marketerAcademys => {
                this.marketerAcademys = marketerAcademys;
                this.isData = true;
            });
    }

    showModal(e, link) {
        e.preventDefault();
        let fileName = link.split('/').splice(-1);
        this.files.openPDF(fileName);
    }
}
