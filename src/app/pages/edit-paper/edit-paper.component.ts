import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Paper } from "@app/_models";
import {AlertService, PaperService } from "@app/_services";
import { environment } from '@environments/environment';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-edit-paper',
  templateUrl: './edit-paper.component.html',
  styleUrls: ['./edit-paper.component.scss']
})
export class EditPaperComponent implements OnInit {

    @ViewChild('inputFile', { static: false }) inputFile:ElementRef;

    registrationForm: FormGroup;
    loading = false;
    paper: Paper;
    isData: boolean = false;
    uploadsPdf:string = environment.uploadsPdf;
    paperId: string;

    constructor(
        private paperService: PaperService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute
    ) {
        this.paperId = this.currentRout.snapshot.paramMap.get('paperId');

        this.registrationForm = formBuilder.group({
            papers: ['', [Validators.required]],
            theme: [null],
            preparat: [null],
            year: [null]
        });

        this.paperService.getById(this.paperId)
            .subscribe(data => {
                this.paper = data;
                this.setFormData();

            });
    }

    private setFormData() {
        this.registrationForm.get('theme').setValue(this.paper.theme);
        this.registrationForm.get('preparat').setValue(this.paper.preparat);
        this.registrationForm.get('year').setValue(this.paper.year);
        this.registrationForm.get('papers').setValue(this.paper.paperTitle);
    }

    ngOnInit() {
    }

    public postData(registrationForm: any) {

        const formData: Paper = {
            theme: registrationForm.controls.theme.value,
            preparat: registrationForm.controls.preparat.value,
            year: registrationForm.controls.year.value,
            paperTitle: registrationForm.controls.papers.value,
        };

        this.loading = true;
        this.paperService.update(formData, this.paperId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.paper = data;
                        this.registrationForm.reset();
                        this.setFormData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
