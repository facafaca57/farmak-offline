import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Preparats} from "@app/_models";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService, SpecialtyService, PreparatsService} from "@app/_services";

@Component({
  selector: 'app-edit-specialty',
  templateUrl: './edit-preparats.component.html',
  styleUrls: ['./edit-preparats.component.scss']
})
export class EditPreparatsComponent implements OnInit {

  public form: FormGroup;
    loading = false;
    preparatId: string;
    preparat: Preparats;

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private alertService: AlertService,
        private currentRout: ActivatedRoute,
        private preparatsService: PreparatsService
    ) {
        this.preparatId = this.currentRout.snapshot.paramMap.get('preparatId');

        this.form = this.fb.group({
            preparatName: ['', [Validators.required]],
        });

        this.loadInitialData();
        this.loadAllData();
    }

    ngOnInit() {}


    private loadInitialData() {
    }

    private loadAllData() {
        this.preparatsService.getById(this.preparatId)
            .subscribe(preparat => {
                this.preparat = preparat;
                this.form.get('preparatName').setValue(preparat.preparatName);
            });
    }

    private setFormData() {
        this.form.get('preparatName').setValue(this.preparat.preparatName);
    }

    public selectChangeHandler(event: any, selectedVal) {
        this[selectedVal] = event.target.value;
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const formData: Preparats = {
            preparatName: form.controls.preparatName.value,
        };

        this.loading = true;
        this.preparatsService.update(formData, this.preparatId)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.preparat = data;
                        this.form.reset();
                        this.setFormData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
