import {Component, OnDestroy, ViewChild, AfterViewInit, HostListener, ElementRef} from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthenticationService, StudyService, TestsResultsService, UpdateService, UserActivityServiceService} from '@app/_services';
import {Study, Test, User, TestsResults} from '@app/_models';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-single-test',
  templateUrl: './single-test.component.html',
  styleUrls: ['./single-test.component.scss']
})
export class SingleTestComponent implements OnDestroy, AfterViewInit {

    @HostListener('window:beforeunload')
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

  @ViewChild('testModal', { static: false }) testModal: any;
  @ViewChild('testCarousel', { static: false }) testCarousel: NguCarousel<any>;

  carouselConfig: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 1, lg: 1, all: 0 },
    interval: {timing: 0, initialDelay: 0},
    loop: false,
    touch: false,
    speed: 0,
    velocity: 0
};

  wrongTimer = '0-1:0-1:0-1';
  loading = true;
  isEmpty = null;
  currentUser: User;
  study: Study;
  test: Test;
  testStartTime: number;
  prevQuestionTime: number;
  timer;
  subscription: Subscription;
  testsResultsSubscription: Subscription;
  saveTestResultsSubscription: Subscription;
  nextQuestionTimer;
  detailedStatistics = [];
  slideIndex = 0;
  userPassedTest = true;
  thisUri: string;

  testsResults: TestsResults[];
  isServerError = false;
  isServerErrorMessage = '';
  finishTestTime: number;

  constructor(
    private authenticationService: AuthenticationService,
    private studyService: StudyService,
    private userActivityServiceService: UserActivityServiceService,
    private testsResultsService: TestsResultsService,
    private currentRout: Router,
    private alertCtrl: AlertController,
    private elementRef: ElementRef,
    private update: UpdateService
  ) {
    const {id, username, role} = this.authenticationService.currentUserValue;
    this.currentUser = {id, username, role};

    this.thisUri = currentRout.url;
    this.thisUri = this.thisUri.replace('\/', '');

    this.userActivityServiceService.startActivity(this.thisUri);
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Вітаємо!',
      backdropDismiss: false,
      translucent: true,
      animated: true,
      buttons: [
        { text: 'Почати тест', role: 'confirm' },
      ],
    });

    await alert.present();
    const { role } = await alert.onDidDismiss();

    return role;
  }

  ngAfterViewInit() {
    this.presentAlert().then(res => {
      this.loadStudy();
    });
    // this.testModal.show();
  }

  shuffle(array) {
    /*var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }*/

    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * array.length );
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }

    return array;
  }

  loadStudy() {
    this.testModal.hide();
    this.subscription = this.studyService.getByURL(this.thisUri)
        .subscribe(studies => {
            let test: Test | undefined;
            if (studies) {
              this.study = studies as Study;
              test = this.study.tests[0] as Test;
              this.test = test;
              this.shuffle(this.test['QA']);
            } else {
              this.isEmpty = 'Тест не знайдено';
            }
        }, error => {
            console.log(error);
            this.isEmpty = 'Тест не знайдено';
        }).add(() => {
            if (this.test) {
                this.testsResultsSubscription = this.testsResultsService.getByUserId(this.currentUser.id)
                    .subscribe(results => {
                    if (results && results.length) {
                        const userPassedTest = results.some(result => {
                        if (result.user && result.test) {
                          return (result.user._id === this.currentUser.id || result.user.id === this.currentUser.id) && result.test._id === this.test._id
                        }

                        return false;
                        })
                        this.userPassedTest = userPassedTest;
                    } else {
                        this.userPassedTest = false
                    }
                    this.loading = false;
                });
            }
        }).add(() => {
          if (this.test) {
            this.startTest()
          }
        })
  }

  getTests(study) {
    const objArray = study;
    const getProp = prop => obj => obj[prop];
    const getFoo = getProp('QA');
    const tests = objArray.map(getFoo).reduce((total, amount) => {
      return total.concat(amount);
    }, []);
    return tests;
  }

  private getMilliseconds() {
    return +new Date();
  }

  private millisToMinutesAndSeconds(millis) {
    let day, hour, minute, seconds;
    seconds = Math.floor(millis / 1000);
    minute = Math.floor(seconds / 60);
    seconds = seconds % 60;
    hour = Math.floor(minute / 60);
    minute = minute % 60;
    day = Math.floor(hour / 24);
    hour = hour % 24;
    const result = '' + (hour < 10 ? '0' : '') + hour + ':' + (minute < 10 ? '0' : '') + minute + ':' +
      (seconds < 10 ? '0' : '') + seconds;
    return  result === this.wrongTimer ? '00:00:00' : result;
  }

  stopInterval(timer) {
    if (timer) {
        clearInterval(timer);
    }
  }

  trySaveResults() {
    this.saveTestResult();
  }

  saveTestResult() {
    this.stopInterval(this.nextQuestionTimer);
    if (!this.finishTestTime) {
        this.finishTestTime = +new Date();
    }

    const generalTime = this.finishTestTime - +this.testStartTime;
    let rightAnswers = 0;
    this.detailedStatistics.forEach(answer => (answer.isCorrect) ? rightAnswers++ : null);

    const testResultData = {
        user: this.currentUser,
        study: this.study,
        test: this.test,
        result: rightAnswers / this.test.QA.length * 100,
        time: generalTime,
        detailedStatistics: this.detailedStatistics
    };

    if (this.isServerError) {
        console.log(testResultData);
    }

    this.userPassedTest = true

    this.saveTestResultsSubscription = this.testsResultsService.create(testResultData)
        .subscribe(
            res => {
                if (!res || !res.id) {
                    this.isServerError = true;
                } else {
                    this.isServerError = false;
                }
                this.authenticationService.network.then(({connected}) => {
                  if (connected) {
                    this.update.uploadNewTestResults();
                  }
              });
            },
            error => {
                this.isServerErrorMessage = String(error);
                console.log(error);
                this.isServerError = true;
            }
        )
  }

  startTest() {
    this.testStartTime = this.getMilliseconds();
    this.runTimer(this.test.duration)
  }

  selectAnswer(question: string, isCorrect: boolean, answer: string) {
    if (this.nextQuestionTimer) {
      this.stopInterval(this.nextQuestionTimer)
    }

    const aTime = +new Date() - this.prevQuestionTime;
    this.prevQuestionTime = this.getMilliseconds();

    const statistic = {q: question, aTime, isCorrect, a: answer};
    this.detailedStatistics.push(statistic);

    if (this.slideIndex === this.test.QA.length - 1) {
        this.saveTestResult();
    } else {
        this.testCarousel?.moveTo(++this.slideIndex, true);
        this.runTimer(this.test.duration)
    }
  }


  runTimer(questionTime: number) {
    this.timer = this.millisToMinutesAndSeconds(questionTime);

    this.prevQuestionTime = this.getMilliseconds();
    let timeLeft = questionTime / 1000;

    this.nextQuestionTimer = setInterval(() => {
        if (timeLeft > 0) {
            timeLeft--;
            this.timer = this.millisToMinutesAndSeconds(timeLeft * 1000);
        } else {
            this.selectAnswer(this.test.QA[this.slideIndex]['question'], false, '');
        }
    }, 1000);
  }

  ngOnDestroy() {
    if (this.subscription) {
        this.subscription.unsubscribe();
    }
    if (this.testsResultsSubscription) {
      this.testsResultsSubscription.unsubscribe();
    }

    if (this.saveTestResultsSubscription) {
        this.saveTestResultsSubscription.unsubscribe();
    }

    this.finishTestTime = 0;
    this.detailedStatistics = [];

    this.stopInterval(this.nextQuestionTimer);

    this.userActivityServiceService.endActivity();
    this.elementRef.nativeElement.remove();
  }
}
