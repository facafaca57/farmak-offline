import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Study, Test} from "@app/_models";
import {Router} from "@angular/router";
import {AlertService, StudyService, TestService} from "@app/_services";
import {Observable, Subscription} from "rxjs";
import * as moment from 'moment';


@Component({
  selector: 'app-studies',
  templateUrl: './studies.component.html',
  styleUrls: ['./studies.component.scss']
})
export class StudiesComponent implements OnInit, OnDestroy {
    routes = [
        {name: 'ТЕСТ УНІВЕРАЛИ', url: 'test-univ'},
        {name: 'ТЕСТ ОФТ-ТЕР',   url: 'test-oft-ter'},
        {name: 'ТЕСТ ГІН-ДЕРМ',  url: 'test-gyn-derm'},
    ];
    DATE_FORMAT = 'DD-MM-YYYY HH:mm';
    subs: Subscription;
    registrationForm: FormGroup;
    model: any = {};
    loading = false;
    studies: Study[] = [];
    tests: Observable<Test[]>;
    selected = [];
    isData = false;
    testsExample = `["5f4b550e64310e24780b331b", "5f4b550e64310e24780b331b"]`;

    constructor(
        private router: Router,
        private studyService: StudyService,
        private formBuilder: FormBuilder,
        private alertService: AlertService,
        private testService: TestService,
    ) {
        this.registrationForm = formBuilder.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(30),
                Validators.minLength(1)])],
            startDate: ['', [Validators.required, Validators.minLength(1)]],
            introPoster: ['', [Validators.required]],
            introVideoDuration: ['', [Validators.required]],
            introVideo: ['', [Validators.required]],
            breakDuration: ['', [Validators.required]],
            finalImage: ['', [Validators.required]],
            tests: ['', [Validators.required]],
            route: null
        });
    }

    getUrl(v) {
        return this.routes.filter(w => w.url === v)[0].name
    }

    ngOnInit() {
        this.loadTests();
        this.loadAll();
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    selectTest(id) {
        this.selected = [id];
        this.registrationForm.get('tests').setValue(JSON.stringify(this.selected))
    }

    testSelect(state, id) {
        if (state && !this.selected.includes(id)) {
            this.selected.unshift(id);
        } else {
            let i = this.selected.indexOf(id);
            this.selected.splice(i, 1);
        }
        let data = this.selected.length ? JSON.stringify(this.selected): '';
        this.registrationForm.get('tests').setValue(data);
    }

    changeUrl(route) {
        this.registrationForm.get('route').setValue(route, {
          onlySelf: true,
        });
      }

    private loadTests() {
        this.tests = this.testService.getAll();
    }

    public delete(id) {
        if (window.confirm('Do you really wanna delete?')) {
            this.loading = true;
            this.studyService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('successfully deleted');
                        this.loadAll();
                    } else {
                        this.alertService.error('Can\'t delete');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    private loadAll() {
        this.subs = this.studyService.getAll()
            .subscribe(studies => {
                const newStudies = studies
                    .map(study => {
                        study.startDate = moment(study.startDate).format(this.DATE_FORMAT);
                        study.tests = JSON.stringify(study.tests);
                        study.breakDuration = +study.breakDuration / 1000 / 60;
                        return study;
                    });
                this.studies = newStudies;
                this.isData = true;
            });
    }

    public postData(registrationForm: any) {
        this.model = {
            title: registrationForm.controls.title.value,
            startDate: +moment(registrationForm.controls.startDate.value, this.DATE_FORMAT),
            introVideo: registrationForm.controls.introVideo.value,
            introVideoDuration: +registrationForm.controls.introVideoDuration.value,
            introPoster: registrationForm.controls.introPoster.value,
            breakDuration: +registrationForm.controls.breakDuration.value,
            finalImage: registrationForm.controls.finalImage.value,
            tests: registrationForm.controls.tests.value,
            route: registrationForm.controls.route.value
        };

        this.loading = true;
        this.studyService.create(this.model)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Successfully completed.');
                        this.registrationForm.reset();
                        this.loadAll();
                        this.selected = [];
                    } else {
                        this.alertService.error('Error. Please try later.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
