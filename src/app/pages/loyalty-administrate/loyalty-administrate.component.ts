import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoyalityPresentation } from "@app/_models";
import { Router } from "@angular/router";
import { AlertService, LoyalityPresentationService } from "@app/_services";
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-loyalty-administrate',
  templateUrl: './loyalty-administrate.component.html',
  styleUrls: ['./loyalty-administrate.component.scss']
})
export class LoyaltyAdministrateComponent implements OnInit {
    public form: FormGroup;
    public slideList: FormArray;
    loading = false;
    loyalityPresentations: LoyalityPresentation[] = [];
    isData: boolean = false;
    files = [];
    uploadsImg = environment.uploadsImg;
    uploadsPdf = environment.uploadsPdf;
    queryString: string;

    constructor(
        private router: Router,
        private loyalityPresentationService: LoyalityPresentationService,
        private fb: FormBuilder,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.loadAllData();

        this.form = this.fb.group({
            title: ['', [Validators.required]],
            slides: this.fb.array([this.createSlide()])
        });

        this.slideList = this.form.get('slides') as FormArray;
    }

    private loadAllData() {
        this.loyalityPresentationService.getAll()
            .subscribe(data => {
                this.loyalityPresentations = data;
                this.isData = true;
            });
    }

    get slideFormGroup() {
        return this.form.get('slides') as FormArray;
    }

    createSlide(): FormGroup {
        return this.fb.group({
            img: [null],
            link: [null],
            linkCenter: [null],
            linkBottom: [null],
            linkBHL: [null],
            linkBHR: [null],
            linkBL: [null],
            linkBC: [null],
            linkBR: [null],
            textLeft: [null],
            textRight: [null]
        });
    }

    addSlide() {
        this.slideList.push(this.createSlide());
    }

    removeSlide(index) {
        this.slideList.removeAt(index);
    }

    getSlidesFormGroup(index): FormGroup {
        const formGroup = this.slideList.controls[index] as FormGroup;
        return formGroup;
    }

    onFileSelect(event, index) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.files[index] = file;
        }
    }

    public delete(id) {
        if (window.confirm("Do you really delete?")) {
            this.loading = true;
            this.loyalityPresentationService.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('Data successfully deleted');
                        this.loadAllData();
                    } else {
                        this.alertService.error('Can\'t delete user');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    drop(event: CdkDragDrop<string[]>, index) {
        if (event.previousIndex !== event.currentIndex) {
            let slides:any = this.loyalityPresentations[index]['slides'];
            let presentation:LoyalityPresentation = this.loyalityPresentations[index];

            moveItemInArray(slides, event.previousIndex, event.currentIndex);

            const slidesSwaped = [...slides];

            const formData: any = new FormData();

            formData.append('slides', JSON.stringify(slidesSwaped));

            this.loading = true;
            this.loyalityPresentationService.update(formData, this.loyalityPresentations[index]['id'])
                .subscribe(
                    data => {
                        if (data.id) {
                            this.alertService.success('Action was successfully completed.');
                        } else {
                            this.alertService.error('Error. No response from backend.');
                        }
                        this.loading = false;
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    });
        }
    }

    public postData($event, form: any) {
        $event.preventDefault();
        const formData: any = new FormData();
        formData.append('title', form.controls.title.value);

        const slides = form.controls.slides.value;
        const sidesReq = [];
        slides.map((slide, i) => {
            sidesReq[i] = {
                'link': slide.link,
                'linkCenter': slide.linkCenter,
                'linkBottom': slide.linkBottom,
                'linkBHL': slide.linkBHL,
                'linkBHR': slide.linkBHR,
                'linkBL': slide.linkBL,
                'linkBC': slide.linkBC,
                'linkBR': slide.linkBR,
                'textLeft': slide.textLeft,
                'textRight': slide.textRight
            };
        });

        formData.append(`slides`, JSON.stringify(sidesReq));

        for (const file of this.files) {
            formData.append(`imgs`, file, file['name']);
        }

        this.loading = true;
        this.loyalityPresentationService.create(formData)
            .subscribe(
                data => {
                    if (data.id) {
                        this.alertService.success('Action was successfully completed.');
                        this.form.reset();
                        this.loadAllData();
                    } else {
                        this.alertService.error('Error. No response from backend.');
                    }
                    this.loading = false;
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
