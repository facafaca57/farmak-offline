import { Component, OnInit, OnDestroy, HostListener, ElementRef } from '@angular/core';
import { environment } from '@environments/environment';
import { UserActivityServiceService, FilesService } from "@app/_services";
import { ExplanationsService } from '@app/_services';
import { Explanations } from '@app/_models';

@Component({
  selector: 'app-explanations-to-the-cycle',
  templateUrl: './explanations-to-the-cycle.component.html',
  styleUrls: ['./explanations-to-the-cycle.component.scss']
})
export class ExplanationsToTheCycleComponent implements OnInit, OnDestroy {
    @HostListener('window:beforeunload') 
    beforeunload() {
        this.userActivityServiceService.endActivity();
    }

    publicFolder = environment.publicFolder;
    // youtubeList = [
    //     'https://www.youtube.com/watch?v=y0yxQEpYvpk',
    //     'https://www.youtube.com/watch?v=VZDUL0fE7lA',
    //     'https://www.youtube.com/watch?v=FVVj3a1-n6w',
    //     'https://www.youtube.com/watch?v=4yYkhgDrm_g',
    //     'https://www.youtube.com/watch?v=VqTs--nH6Ow',
    //     'https://www.youtube.com/watch?v=-nO6Umbi41g'
    // ]
    videoList = [
        "https://farmak-rx2.com/public/uploads/yt/y0yxQEpYvpk.mp4",
        "https://farmak-rx2.com/public/uploads/yt/VZDUL0fE7lA.mp4",
        "https://farmak-rx2.com/public/uploads/yt/FVVj3a1-n6w.mp4",
        "https://farmak-rx2.com/public/uploads/yt/4yYkhgDrm_g.mp4",
        "https://farmak-rx2.com/public/uploads/yt/VqTs--nH6Ow.mp4",
        "https://farmak-rx2.com/public/uploads/yt/-nO6Umbi41g.mp4"
    ]

    videos: Explanations[] = [];
    images: Explanations[] = [];

    image = this.publicFolder + '/uploads/img/poyasnennya_do_cziklovoyi.jpg';

    constructor(
        private userActivityServiceService: UserActivityServiceService,
        private explService: ExplanationsService,
        private files: FilesService,
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.userActivityServiceService.startActivity('Пояснення до циклової');
        this.explService.getAll().subscribe(data => {
            this.images = data.filter(i => i.type == 'image');
            this.videos = data.filter(v => v.type == 'video' || v.type == 'youtube');
        });
    }

    ngOnDestroy() {
        this.userActivityServiceService.endActivity();
        this.elementRef.nativeElement.remove();
    }

    openVideo(e, link) {
        e.preventDefault();
        let filename = link.split('/').splice(-1);
        this.files.openVideo(filename);
    }

    checkTheYoutube(link) {
        if(link) {
            if(~link.indexOf('youtu.be') || ~link.indexOf('youtube')) return true
            else return false;
        } else return false;
    }

    getUrlFromYT(url) {
        return `https://farmak-rx2.com/public/uploads/yt/${this.getYoutubeUrl(url)}.mp4`;
    }

    getYoutubeUrl(url) {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
        const match = url.match(regExp);

        const id = (match && match[2].length === 11)
          ? match[2]
          : null;

        return id;
    }
}
