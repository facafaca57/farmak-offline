import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {TestsResults} from "@app/_models";
import {Router} from "@angular/router";
import {AlertService, TestsResultsService} from "@app/_services";

@Component({
  selector: 'app-tests-results',
  templateUrl: './tests-results.component.html',
  styleUrls: ['./tests-results.component.scss']
})
export class TestsResultsComponent implements OnInit, OnDestroy {
    DATE_FORMAT = 'DD-MM-YYYY HH:mm';
    subs: Subscription;
    model: any = {};
    loading = false;
    results: TestsResults[] = [];
    queryString: string;

    constructor(
        private router: Router,
        private testsResults: TestsResultsService,
        private alertService: AlertService,
    ) {}

    ngOnInit() {
        this.loadAll();
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }

    millisToMinutesAndSeconds(millis) {
        let day, hour, minute, seconds;
        seconds = Math.floor(millis / 1000);
        minute = Math.floor(seconds / 60);
        seconds = seconds % 60;
        hour = Math.floor(minute / 60);
        minute = minute % 60;
        day = Math.floor(hour / 24);
        hour = hour % 24;
        return  '' + (minute < 10 ? '0' : '') + minute + ':' +
            (seconds < 10 ? '0' : '') + seconds;
    }


    public delete(id) {
        if (window.confirm('Do you really wanna delete?')) {
            this.loading = true;
            this.testsResults.delete(id)
                .subscribe(res => {
                    if (res.status === 'ok') {
                        this.alertService.success('successfully deleted');
                        this.loadAll();
                    } else {
                        this.alertService.error('Can\'t delete');
                    }
                    this.loading = false;
                }, error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
        }
    }

    private loadAll() {
        this.subs = this.testsResults.getAll()
            .subscribe(testsResults => {
                const newTestsResults = testsResults
                    .map(test => {
                        test.user = (test.user && test.user.username) ? test.user.username : 'null';
                        test.study = (test.study) ? test.study.title : "";
                        test.test = (test.test && test.test.title) ? test.test.title : "";
                        test.time = this.millisToMinutesAndSeconds(test.time);
                        test.detailedStatistics = test.detailedStatistics.map(el => {
                            const detailedStatistic = el;
                            detailedStatistic.aTime = this.millisToMinutesAndSeconds(el.aTime);
                            return detailedStatistic;
                        });
                        return test;
                    });

                this.results = newTestsResults;
            });
    }
}
