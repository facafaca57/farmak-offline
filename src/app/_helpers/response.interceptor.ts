import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler, HttpInterceptor,
  HttpRequest, HttpResponse
} from '@angular/common/http';
import { StorageService } from '@app/_services';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
    cacheStorage: Cache;
    constructor(
        private storage: StorageService
    ) {
        if ('caches' in window) {
            caches.open('farmakx2').then((res) => (this.cacheStorage = res));
        }
    }

    async put(req: string, body: any) {
        body = JSON.stringify(body);
        await this.storage.saveData(req, body);
    }

    async get(req: string): Promise<HttpResponse<any>>{
        let response = await this.storage.getData(req);
        if (!response) throw Error('response not found');
        let body = response;
        return new HttpResponse({ body, status: 200, statusText: 'ok' });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any> | any> {
        // make sure that this browser support caches
        if (!('caches' in window)) return next.handle(req);
        const cacheRequest = req.urlWithParams;

        // const url = cacheRequest.split('/api')[1];
        // const query = url.split('/').pop();
        // console.log(url);
        // console.log(query);

        let observable = next.handle(req).pipe(
            // tap((res: HttpResponse<any>) => {
            //     if (res instanceof HttpResponse) {
            //         this.put(url, res.body);
            //     }
            // })
        );

        // return from(this.get(url)).pipe(catchError((_) => observable ));
        return observable;
   }
}
