export function getImageType(filename: string) {
  if (filename.endsWith(".jpg")) {
    return "image/jpeg" 
  } else if (filename.endsWith(".mp4")) {
    return "video/mp4" 
  } else if (filename.endsWith(".pdf")) {
    return "application/pdf" 
  } else {
    return null;
  }
}