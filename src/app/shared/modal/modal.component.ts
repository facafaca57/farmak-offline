import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

const ZOOM_STEP: number = 0.25;
const DEFAULT_ZOOM: number = 1;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() fileLink;
  @Input() fileName;

	public pdfZoom: number = DEFAULT_ZOOM;

  constructor(private modalCtrl: ModalController) {}

  ngOnInit() { }

  dismissModal(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  
	zoomIn(e) {
    e.stopPropagation();
		this.pdfZoom += ZOOM_STEP;
	}

	zoomOut(e) {
    e.stopPropagation();
		if (this.pdfZoom > DEFAULT_ZOOM) {
			this.pdfZoom -= ZOOM_STEP;
		}
	}

	resetZoom(e) {
    e.stopPropagation();
		this.pdfZoom = DEFAULT_ZOOM;
	}
}
