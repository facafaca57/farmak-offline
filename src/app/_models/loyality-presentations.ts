export class LoyalityPresentation {
    id: string;
    title: string;
    slides: any[] | string[];
}
