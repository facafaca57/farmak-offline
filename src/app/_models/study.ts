﻿
import {Test} from './test';

export class Study {
    id: string;
    title: string;
    startDate: number | string;
    introVideo: string;
    introVideoDuration: number|string;
    breakDuration: number|string;
    introPoster?: string;
    finalImage: string;
    tests: Test[] | string;
}
