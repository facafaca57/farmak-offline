﻿export class Specialty {
    id?: string;
    specialtyName: string;
    logo: string | any;
    orderNumber?: number;
}
