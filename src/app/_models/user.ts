﻿export interface Activity {
    activityDate?: number | string;
    isActive?: boolean;
}

export interface ActivityPages {
    id?: number;
    pageName?: string;
    dateStart?: string;
    dateEnd?: string;
    duration?: string;
}

export class User {
    id: string;
    username: string;
    login?: string;
    password?: string;
    role: string;
    token?: string;
    activity?: Activity[];
    activityPages?: ActivityPages[];
    resultsImg?: string;
}
