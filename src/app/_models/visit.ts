﻿export class Visit {
    id?: number;
    serialNumber: number | null;
    year: number | null;
    week: string | null;
}
