import { Preparats, User } from '@app/_models';

export class MarketingComments {
    id?: string;
    comment: string;
    date:string;
    user: string|User;
    preparat:string | any;
}
