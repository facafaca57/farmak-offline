﻿export class Presentation {
    id: string;
    title: string;
    type: string;
    specialty: string;
    visit: number;
    slides: any[] | string[];
}
