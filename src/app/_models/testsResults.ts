﻿
export class TestsResults {
    id?: string;
    user: any;
    study: any;
    test: any;
    result: number;
    time: number | string;
    detailedStatistics: any;
}
