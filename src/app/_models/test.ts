﻿
export class Test {
    ['_id']?: string;
    id: string;
    title: string;
    duration: number;
    breakDuration: number;
    videoUrl: string;
    videoDuration: number | string;
    poster?: string;
    QA: any[] | string;
}
