import { SwiperOptions } from 'swiper/types/swiper-options';

export const swiperConfig: SwiperOptions = {
    slidesPerView: 1.00001,
    spaceBetween: 10,
    watchOverflow: false,
    navigation: true,
    updateOnWindowResize: true,
    resizeObserver: true,
    observer: true,
    zoom: {
        maxRatio: 2
    },
    on: {
        slideChangeTransitionEnd: function () {
          this.zoom.in();
        }
    }
    // slidesOffsetBefore: 40,
    // slidesOffsetAfter: 40
}