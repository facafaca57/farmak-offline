import { Pipe, PipeTransform } from '@angular/core';
import { Paper } from "@app/_models";


@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(array: Array<Paper>, ...args: any[]): any {
      array.sort((a: any, b: any) => {
          if (a.paperTitle < b.paperTitle) {
              return -1;
          } else if (a.paperTitle > b.paperTitle) {
              return 1;
          } else {
              return 0;
          }
      });
      return array;
  }
}
