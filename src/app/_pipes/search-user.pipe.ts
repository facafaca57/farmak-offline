import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchUserPipe'
})
export class SearchUserPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            input = input.toLowerCase();

            return value.filter((el: any) => {
                return el.user.toLowerCase().indexOf(input) > -1;
            });
        }
        return value;
    }
}
