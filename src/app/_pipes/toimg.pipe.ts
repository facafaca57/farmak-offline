import { Pipe, PipeTransform } from '@angular/core';
import html2canvas from 'html2canvas';

@Pipe({
  name: 'toimg'
})
export class ToimgPipe implements PipeTransform {

    stringToHTML = function (str) {
        // return new DOMParser().parseFromString(str, "text/html").body;
        const placeholder = document.createElement("div");
        placeholder.innerHTML = str;
        document.body.appendChild(placeholder).setAttribute('style', 'position: absolute; top: -1000%');
        return placeholder
    };

    transform(value: HTMLElement): any {
        return html2canvas(this.stringToHTML(value)).then(canvas => {
            return canvas.toDataURL('image/png');
        }).catch(error => console.log(error) );
    }
}
