import { Pipe, PipeTransform } from '@angular/core';
import { Observable, from } from 'rxjs';
import { FilesService } from '@app/_services';
import { File } from '@ionic-native/file/ngx';

@Pipe({
  name: 'toLocalImage'
})
export class LocalImagePipe implements PipeTransform {
  constructor(
    private file: File ,
    private manager: FilesService
  ) { }

  transform(url: string): Observable<any> {
    let filename = url.split('/').reverse()[0];
    return from(this.manager.getFile(filename));
    // return from(this.file.readAsDataURL(this.file.externalDataDirectory, 'url'));
      // Http.getImage({
      //   headers: {
      //     Authorization: `Bearer ${this.getToken()}`, // or whatever additional headers that needs to be passed in
      //   },
      //   url: url,
      //   method: 'GET',
      // })
    // )
  }
}
