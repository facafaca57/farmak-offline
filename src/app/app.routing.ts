﻿import { Routes, RouterModule } from '@angular/router';

import { AuthGuard, AdminGuard } from './_helpers';

import { HomeComponent } from './pages/home';
import { LoginComponent } from './pages/login';
import { ControlComponent } from './pages/control/';
import { UsersComponent } from './pages/users';
import { UserActivityComponent } from './pages/user-activity/user-activity.component';
import { VisitsComponent } from './pages/visits/visits.component';
import { PresentationsComponent } from './pages/presentations/presentations.component';
import { SpecialtiesComponent } from './pages/specialties/specialties.component';
import { PapersComponent } from './pages/papers/papers.component';
import { EditPaperComponent } from './pages/edit-paper/edit-paper.component';
import { MaterialsComponent } from './pages/materials/materials.component';
import { SpecialtyComponent } from './pages/specialty/specialty.component';
import { PresentationComponent } from './pages/presentation/presentation.component';
import { EditPresentationComponent } from './pages/edit-presentation/edit-presentation.component';
import { EditVisitComponent } from './pages/edit-visit/edit-visit.component';
import { PreparatsComponent } from './pages/preparats/preparats.component';
import { PreparatComponent } from './pages/preparat/preparat.component';
import { EditPreparatsComponent } from './pages/edit-preparats/edit-preparats.component';
import { QuestionsComponent } from './pages/questions/questions.component';
import { EditQuestionComponent } from './pages/edit-question/edit-question.component';
import { QuestionPageComponent } from './pages/question-page/question-page.component';
import { QuestionContentComponent } from './pages/question-content/question-content.component';

import { StudyComponent } from './pages/study/study.component';
import { StudiesComponent } from './pages/studies/studies.component';
import { TestsComponent } from './pages/tests/tests.component';
import { TestsResultsComponent } from './pages/tests-results/tests-results.component';
import {EditStudyComponent} from './pages/edit-study/edit-study.component';
import {EditUserComponent} from './pages/edit-user/edit-user.component';
import { ResultsComponent } from './pages/results/results.component';
import { EducationalMaterialsComponent } from './pages/educational-materials/educational-materials.component';
import { EducationalMaterialComponent } from './pages/educational-material/educational-material.component';
import { EditSpecialtyComponent } from './pages/edit-specialty/edit-specialty.component';
import { SingleTestComponent } from './pages/single-test/single-test.component';
import { ExplanationsToTheCycleComponent } from './pages/explanations-to-the-cycle/explanations-to-the-cycle.component';
import { HistoryComponent } from './pages/history/history.component';
import { HistoryElementComponent } from './pages/history-element/history-element.component';
import { HistoryPresentationComponent } from './pages/history-presentation/history-presentation.component';
import { LoyaltyRogramsComponent } from './pages/loyalty-rograms/loyalty-rograms.component';
import { LoyaltyAdministrateComponent } from './pages/loyalty-administrate/loyalty-administrate.component';
import { LoyaltyAdministrateEditComponent } from './pages/loyalty-administrate-edit/loyalty-administrate-edit.component';
import { LoyalityPresentationComponent } from './pages/loyality-presentation/loyality-presentation.component';
import { MarketingCommentsComponent } from './pages/marketing-comments/marketing-comments.component';
import { MarketingCommentsToPreparatComponent } from './pages/marketing-comments-to-preparat/marketing-comments-to-preparat.component';
import { MarketingCommentsToPreparatEditComponent } from './pages/marketing-comments-to-preparat-edit/marketing-comments-to-preparat-edit.component';
import { ControlMarketingCommentsComponent } from './pages/control-marketing-comments/control-marketing-comments.component';
import { ControlMarketerAcademyComponent } from './pages/control-marketer-academy/control-marketer-academy.component';
import { EditMarketerAcademyComponent } from './pages/edit-marketer-academy/edit-marketer-academy.component';
import { MarketerAcademyComponent } from './pages/marketer-academy/marketer-academy.component';
import { MarketerAcademyPreparatComponent } from './pages/marketer-academy-preparat/marketer-academy-preparat.component';
import { MarketerAcademyPresentationComponent } from './pages/marketer-academy-presentation/marketer-academy-presentation.component';
import { UpdatesComponent } from './pages/updates/updates.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'update', component: UpdatesComponent },
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'control', component: ControlComponent, canActivate: [AdminGuard] },
    { path: 'control/marketing-comments', component: ControlMarketingCommentsComponent, canActivate: [AdminGuard] },
    { path: 'control/users', component: UsersComponent, canActivate: [AdminGuard] },
    { path: 'control/users/:userID', component: EditUserComponent, canActivate: [AdminGuard] },
    { path: 'control/users/activity/:userID', component: UserActivityComponent, canActivate: [AdminGuard] },
    { path: 'control/visits', component: VisitsComponent, canActivate: [AdminGuard] },
    { path: 'control/presentations', component: PresentationsComponent, canActivate: [AdminGuard] },
    { path: 'control/specialties', component: SpecialtiesComponent, canActivate: [AdminGuard] },
    { path: 'control/papers', component: PapersComponent, canActivate: [AdminGuard] },
    { path: 'control/papers/:paperId', component: EditPaperComponent, canActivate: [AdminGuard] },
    { path: 'control/presentations/:presentationId', component: EditPresentationComponent, canActivate: [AdminGuard] },
    { path: 'control/visits/:visitId', component: EditVisitComponent, canActivate: [AdminGuard] },
    { path: 'control/specialties/:specialtyId', component: EditSpecialtyComponent, canActivate: [AdminGuard] },
    { path: 'control/tests', component: TestsComponent, canActivate: [AdminGuard] },
    { path: 'control/studies', component: StudiesComponent, canActivate: [AdminGuard] },
    { path: 'control/studies/:studyId', component: EditStudyComponent, canActivate: [AdminGuard] },
    { path: 'control/testsResults', component: TestsResultsComponent, canActivate: [AdminGuard] },
    { path: 'control/preparats', component: PreparatsComponent, canActivate: [AdminGuard] },
    { path: 'control/preparats/:preparatId', component: EditPreparatsComponent, canActivate: [AdminGuard] },
    { path: 'control/questions', component: QuestionsComponent, canActivate: [AdminGuard] },
    { path: 'control/questions/:questionId', component: EditQuestionComponent, canActivate: [AdminGuard] },
    { path: 'control/loyality-presentations', component: LoyaltyAdministrateComponent, canActivate: [AdminGuard] },
    { path: 'control/loyality-presentations/:loyality-presentationsId', component: LoyaltyAdministrateEditComponent, canActivate: [AdminGuard] },
    { path: 'control/marketerAcademy', component: ControlMarketerAcademyComponent, canActivate: [AdminGuard] },
    { path: 'control/marketerAcademy/:marketerAcademyId', component: EditMarketerAcademyComponent, canActivate: [AdminGuard] },

    { path: 'test-oft-ter', component: SingleTestComponent, canActivate: [AuthGuard] },
    { path: 'test-gyn-derm', component: SingleTestComponent, canActivate: [AuthGuard] },
    { path: 'test-univ', component: SingleTestComponent, canActivate: [AuthGuard] },

    { path: 'marketerAcademy', component: MarketerAcademyComponent, canActivate: [AuthGuard] },
    { path: 'marketerAcademy/:preparatId', component: MarketerAcademyPreparatComponent, canActivate: [AuthGuard] },
    { path: 'marketerAcademy/:preparatId/:marketerAcademyId', component: MarketerAcademyPresentationComponent, canActivate: [AuthGuard] },

    { path: 'marketing-comments', component: MarketingCommentsComponent, canActivate: [AuthGuard] },
    { path: 'marketing-comments/:preparatId', component: MarketingCommentsToPreparatComponent, canActivate: [AuthGuard] },
    { path: 'marketing-comments/:preparatId/:commentId', component: MarketingCommentsToPreparatEditComponent, canActivate: [AuthGuard] },

    { path: 'materials', component: MaterialsComponent, canActivate: [AuthGuard] },
    { path: 'educational-materials', component: EducationalMaterialsComponent, canActivate: [AuthGuard] },
    { path: 'educational-materials/:materialId', component: EducationalMaterialComponent, canActivate: [AuthGuard] },
    { path: 'study', component: StudyComponent, canActivate: [AuthGuard] },
    { path: 'results', component: ResultsComponent, canActivate: [AuthGuard] },
    { path: 'explanations', component: ExplanationsToTheCycleComponent, canActivate: [AuthGuard] },

    { path: 'loyalty-programs', component: LoyaltyRogramsComponent, canActivate: [AuthGuard] },
    { path: 'loyalty-programs/:loyality-presentationId', component: LoyalityPresentationComponent, canActivate: [AuthGuard] },
    { path: 'relevant', component: PreparatComponent, canActivate: [AuthGuard] },
    { path: 'relevant/:preparatId', component: QuestionPageComponent, canActivate: [AuthGuard] },
    { path: 'relevant/:preparatId/:questionId', component: QuestionContentComponent, canActivate: [AuthGuard] },
    { path: ':specialtyId', component: SpecialtyComponent, canActivate: [AuthGuard] },
    { path: ':specialtyId/history', component: HistoryComponent, canActivate: [AuthGuard] },
    { path: ':specialtyId/history/:yearId', component: HistoryElementComponent, canActivate: [AuthGuard] },
    { path: ':specialtyId/history/:yearId/:presentationId', component: HistoryPresentationComponent, canActivate: [AuthGuard] },
    { path: ':specialtyId/:presentationId', component: PresentationComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
