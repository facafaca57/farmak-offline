import { Directive, ElementRef, Input } from '@angular/core';
import { FilesService } from './../_services/files.service';
import { Platform } from '@ionic/angular';

@Directive({
  selector: '[src]'
})
export class ChangeImageDirective {

  private readonly imageElement: HTMLElement;
  @Input('src') src?: string;

  constructor(
    private element: ElementRef,
    private manager: FilesService,
    private platform: Platform
  ) {
    this.imageElement = this.element.nativeElement;
  }

  ngAfterViewInit() {
    let filename = this.src.split('/').reverse()[0];
    if (this.platform.is('android') && (this.src && !this.src.startsWith('data:image') && !this.src.endsWith('.pdf'))) {
      this.manager.getFile(filename).then(async image_url => {
        this.imageElement.setAttribute('src', image_url);
      });
    } else {
      this.imageElement.setAttribute('src', this.src);
    }
  }
}
