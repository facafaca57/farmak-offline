import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';
import { SwiperOptions } from "swiper/types/swiper-options";
import { register } from 'swiper/element/bundle';
import { SwiperContainer } from 'swiper/element';

register();

@Directive({
  selector: '[swiperElement]'
})
export class SwiperDirective implements AfterViewInit {

  private swiperElement: SwiperContainer;
  @Input('config') config?: SwiperOptions;

  constructor(private element: ElementRef<SwiperContainer>) {
    this.swiperElement = element.nativeElement;
  }

  ngAfterViewInit(): void {
    Object.assign(this.swiperElement, this.config);
    this.swiperElement.initialize();
    this.swiperElement.addEventListener('swiper-zoomchange', (event) => {
      this.swiperElement.swiper.allowTouchMove = event['detail'][1] === 1;
      this.swiperElement.swiper.allowSlideNext = event['detail'][1] === 1;
      this.swiperElement.swiper.allowSlidePrev = event['detail'][1] === 1;
    });
    this.swiperElement.addEventListener('swiper-slidechange', () => {
      this.swiperElement.swiper.zoom.out();
    });
  }
}
