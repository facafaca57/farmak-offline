import { Directive, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { FilesService } from './../_services/files.service';
import videojs from 'video.js';
import { Platform } from '@ionic/angular';
import Player from 'video.js/dist/types/player';

@Directive({
  selector: '.video-js'
})
export class VideoDirective {

  private readonly videoElement: HTMLElement;
  @Input('src') src?: string;

  player: Player
  config = {
    disablePictureInPicture: true,
    controls: true,
    preload: 'none',
    // width: window.innerWidth - 200,
    // height: window.innerHeight - 220,
    poster: 'assets/img/poster.webp'
  }
  
  constructor(
    private element: ElementRef,
    private manager: FilesService,
    private platform: Platform
  ) {
    this.videoElement = this.element.nativeElement;
  }

  ngAfterViewInit() {
    let filename = this.src.split('/').reverse()[0];
    if (this.platform.is('android')) {
      this.manager.getFile(filename).then(image_url => {
        this.player = videojs(this.videoElement, Object.assign(this.config, {
          sources: [{ src: image_url, type: 'video/mp4' }]
        }), () => { });
      });
    } else {
      this.player = videojs(this.videoElement,  Object.assign(this.config, {
        sources: [{ src: this.src, type: 'video/mp4' }]
      }), () => { });
    }
  }

}
