import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Question } from '@app/_models';
import { StorageService } from './storage.service';
import { from } from 'rxjs';

const fieldDB = 'Question';
const entity = 'questions';

@Injectable({ providedIn: 'root' })
export class QuestionService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<Question[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<Question>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getByQuery(query: string) {
        let field = query.substring(0, query.indexOf("="));
        let value = query.substring(query.indexOf("=") + 1);
        return from(this.storage.getDatabyQuery(fieldDB, field, value));
        // return this.http.get<Question[]>(`${environment.apiUrl}/${entity}/?${query}`);
    }

    create(data: Question) {
        return this.http.post<Question>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: any, id: string) {
        return this.http.put<Question>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
