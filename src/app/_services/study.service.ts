﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Study } from '@app/_models';
import { StorageService } from './storage.service';
import { from } from 'rxjs';

const fieldDB = 'Study';
const entity = 'studies';

@Injectable({ providedIn: 'root' })
export class StudyService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<Study[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<Study>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getByURL(url: string) {
        return from(this.storage.getbyQuery(fieldDB, 'route', url));
        // return this.http.get<Study>(`${environment.apiUrl}/${entity}/url/${url}`);
    }

    create(data: Study) {
        return this.http.post<Study>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: Study, id: string) {
        return this.http.put<Study>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    updateStartDate(data: any, id: string) {
        return this.http.put<any>(`${environment.apiUrl}/${entity}/startDate/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
