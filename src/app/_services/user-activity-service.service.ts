import { Injectable } from '@angular/core';
import { UserService, AuthenticationService, AlertService } from '@app/_services';
import { User, ActivityPages } from '@app/_models';


@Injectable({
  providedIn: 'root'
})
export class UserActivityServiceService {

    currentUser: User;
    currentUserBd: User;
    activityString: string;
    dateStart: Date;
    dateEnd: Date;
    dateStartStr: string;
    dateEndStr: string;
    activityId: number;
    activityModel: ActivityPages;

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService,
    ) {
        this.authenticationService.currentUser.subscribe(x => {
            this.currentUser = x;
            // this.con(this.currentUser.login);
        });
    }

    startActivity(nameStart: string) {
        if(this.currentUser?.role === 'user') {
            this.dateStart = new Date();
            this.dateStartStr = this.formatDate(this.dateStart);
            this.activityString = nameStart + '; Вхід: ';

            this.userService.getById(this.currentUser.id)
                .subscribe(data => {
                    this.currentUserBd = data;
                    if (!this.currentUserBd?.activityPages){
                        this.activityId = 1;
                    } else {
                        this.activityId = this.currentUserBd.activityPages[this.currentUserBd.activityPages.length - 1].id + 1;
                    };

                    this.activityModel = {
                        id: this.activityId,
                        pageName: nameStart,
                        dateStart: this.dateStartStr,
                        dateEnd: null,
                        duration: null
                    }

                    // this.userService.startActivityPages(this.activityModel, this.currentUser.id).subscribe(
                    //     data => {
                    //         if (data) {
                    //             this.con('Action was successfully completed.');
                    //         } else {
                    //             this.con('Error. No response from backend.');
                    //         }
                    //     },
                    //     error => {
                    //         this.con(error);
                    // });
            });

        }
    }

    endActivity() {
        if(this.currentUser?.role === 'user') {
            this.dateEnd = new Date();
            this.dateEndStr = this.formatDate(this.dateEnd);
            let difDate =  this.dateEnd.getTime() - this.dateStart.getTime();
            if (this.activityModel) {
                this.activityModel.dateEnd = this.dateEndStr;
                this.activityModel.duration = this.difDate(difDate);

                let allActivities = this.currentUserBd.activityPages ? 
                    this.currentUserBd.activityPages.concat(this.activityModel) : [this.activityModel];

                this.userService.activityPages(allActivities, this.currentUser.id).subscribe(
                    data => {
                        if (data) {
                            this.con('Action was successfully completed.');
                        } else {
                            this.con('Error. No response from backend.');
                        }
                    },
                    error => {
                        this.con(error);
                });
            }
            
        }
    }
    changeActivity(newNameStart) {
        if(this.currentUser.role === 'user') {

            this.activityModel.pageName = newNameStart;
            // this.userService.activityPages(this.activityModel, this.currentUser.id).subscribe(
            //     data => {
            //         if (data) {
            //             this.alertService.success('Дані успішно збережені');
            //         } else {
            //             this.alertService.success('Помилка! Немає відповіді з сервера.');
            //         }
            //     },
            //     error => {
            //         this.alertService.success(error);
            // });
        }
    }

    formatDate(date) {
        var d = new Date(date),
            minutes = '' + d.getMinutes(),
            hour = '' + d.getHours(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        if (hour.length < 2)
            hour = '0' + hour;
        if (minutes.length < 2)
            minutes = '0' + minutes;

        return [year, month, day].join('-') + '  ' + hour + ':' + minutes;
    }

    difDate (duration) {
        var seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24),
            _hours = (hours < 10) ? "0" + hours : hours,
            _minutes = (minutes < 10) ? "0" + minutes : minutes,
            _seconds = (seconds < 10) ? "0" + seconds : seconds;

        return _hours + ":" + _minutes + ":" + _seconds;
    }

    con(str) {
        console.log(str);
    }
}
