﻿import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Network } from '@capacitor/network';
import { User } from '@app/_models';
import { UserService } from './user.service';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public networkStatus = new BehaviorSubject<any>(false);

    constructor( private userService: UserService ) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        Network.addListener('networkStatusChange', ({ connected }) => {
            this.networkStatus.next(connected);
        });     
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public get network() {
        return Network.getStatus();
    }

    public get networkObs() {
        return this.networkStatus.asObservable();
    }

    login(login: string, password: string) {
        return this.userService.login(login, password).pipe(map(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
            return user;
        }));
    }

    logout() {
        // remove user from local storage to log user out
        return this.userService.logout(this.currentUserValue.id).pipe(tap(() => {
            localStorage.removeItem('currentUser');
            this.currentUserSubject.next(null);
        }));
    }
}
