﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { TestsResults } from '@app/_models';
import { StorageService } from './storage.service';
import { from } from 'rxjs';

const fieldDB = 'TestsResults';
const entity = 'testsResults';

@Injectable({ providedIn: 'root' })
export class TestsResultsService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<TestsResults[]>(`${environment.apiUrl}/${entity}`);
    }

    getByUserId(userId: string) {
        return from(this.storage.getDatabyQuery(fieldDB, 'user', userId));
        // return this.http.get<TestsResults[]>(`${environment.apiUrl}/${entity}/user/${userId}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<TestsResults>(`${environment.apiUrl}/${entity}/${id}`);
    }

    create(data: TestsResults) {
        return from(this.storage.addData(fieldDB, data));
        // return this.http.post<TestsResults>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: TestsResults, id: string) {
        return this.http.put<TestsResults>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
