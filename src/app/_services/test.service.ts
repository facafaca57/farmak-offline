﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Test } from '@app/_models';


const entity = 'tests';

@Injectable({ providedIn: 'root' })
export class TestService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Test[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return this.http.get<Test>(`${environment.apiUrl}/${entity}/${id}`);
    }

    create(data: Test) {
        return this.http.post<Test>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: Test, id: string) {
        return this.http.put<Test>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
