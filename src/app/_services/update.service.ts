import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
// import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { StorageService, AuthenticationService, FilesService } from '@app/_services';
import { AlertController, LoadingController } from '@ionic/angular';
import { catchError } from 'rxjs/operators';
import { ActivityPages, User } from '@app/_models';
import { interval } from 'rxjs';
import { Router } from '@angular/router';

const entity = 'app';
const lastUpdate = 'last_update';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {
  dbLists = [
    'Question', 'MarketerAcademy', 
    'MarketingComments', 'Presentation', 
    'Paper', 'Preparats', 'Specialty', 
    'Study', 'Test', 'TestsResults', 
    'Visit', 'LoyaltyPresentation', 'User', 'Explanations'
  ];

  curentUser: User;

  hours = 1; //hours
  schedule = 1000 * 60 * 60;
  checkUpdatesObs = interval(this.schedule * this.hours);

  constructor(
    private http: HttpClient,
    private storage: StorageService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private filesService: FilesService,
    private authService: AuthenticationService,
    private router: Router,
    // private localNotifications: LocalNotifications
  ) {
    this.authService.currentUser.subscribe(user => {
      this.curentUser = user;
    });
    // this.scheduleUpdate();
    // this.networkListener();
  }

  async showLoading(message: string = 'Please wait...') {
    return this.loadingCtrl.create({
      message,
      showBackdrop: true
    });
  }

  async presentAlert(header: string = 'Оновлення', message: string = 'Please wait...', action: boolean = false) {
    const alert = await this.alertCtrl.create({
      header,
      message,
      buttons: action ? [
        { text: 'Cancel', role: 'cancel' },
        { text: 'OK', role: 'confirm' },
      ] : [{ text: 'OK', role: 'confirm' }],
    });

    await alert.present();
    const { role } = await alert.onDidDismiss();

    return role;
  }

  scheduleUpdate() {
    this.checkUpdatesObs.subscribe(() => {
      this.authService.network.then(({connected}) => {
        if (this.curentUser && connected) {
          this.checkUpdates();
        }
      })
    });
  }

  networkListener(){
    this.authService.networkObs.subscribe(state => {
     if (state) {
      let updateTime = new Date(localStorage.getItem(lastUpdate)).getTime();
      if (new Date().getTime() - updateTime > this.schedule) {
        this.checkUpdates();
      }
     }
    })
  }

  checkUpdates() {
    // this.getUpdateApp();
    this.storage.getData('Update').then(async res => {
      await this.checkLocalDB();
      await this.uploadNewComments();
      await this.uploadNewTestResults();
      await this.uploadNewActivity();

      if (res) {
        let lastUpdate = res.sort((a, b) => new Date(b.createAt).getTime() - new Date(a.createAt).getTime())[0];
        this.getUpdates(lastUpdate.createAt).subscribe(upd => {
          if (upd) {
            this.storage.saveData('Update', res.concat(upd));
            this.getUpdatesFromServer(lastUpdate);
          } else {
            this.presentAlert('Оновлення', 'Не знайденно нових оновлень');
          }
        });
      } else this.getUpdates().subscribe(async res => {
        if (res) this.storage.saveData('Update', res);
      });
      localStorage.setItem(lastUpdate, new Date().toString());
      // this.showNotification();
    });
  }

  private getUpdatesFromServer(lastUpdate: any) {
    this.showLoading('Завантаження оновлень...').then((loader) => {
      loader.present();
      this.getUpdatesData(lastUpdate.createAt).subscribe((e) => {
        let deleted = e['deletedData'];
        let newData = e['newData'];

        if (newData) {
          newData.forEach(async (data) => {
            let table = Object.keys(data)[0];
            if (data[table].length) {
              await this.storage.getData(table).then(async fromdb => {
                data[table].forEach(updt => {
                    let i = fromdb.findIndex(db => db.id == updt.id);
                    if(i != -1) fromdb[i] = updt;
                    else fromdb.push(updt);
                });
                await this.storage.saveData(table, fromdb);
              });
            }
          });
        }
        if (deleted) {
          deleted.forEach(async (data) => {
            let table = data.table;
            let ids = data.ids;
              await this.storage.getData(table).then(async fromdb => {
                await this.storage.saveData(table, fromdb.filter(el => !ids.includes(el.id)));
              });
          });
        }
        loader.dismiss().then(() => {
          this.filesService.downloadFiles();
        });
      });
    });
  }

  private async checkLocalDB() {
    await this.storage.getKeysFromDb().then(el => {
      if (!this.dbLists.every(a => el.map(e => e.key).includes(a))) {
        this.router.navigateByUrl('/update').then(() => {
          this.showLoading('Оновлення бази данних...').then((loader) => {
            loader.present();
            this.getAllData().subscribe(serverDB => {
              Object.values(serverDB).forEach(async (data, i) => {
                let table = Object.keys(data)[0];
                await this.storage.saveData(table, data[table]);
                if (Object.values(serverDB).length == i+1) loader.dismiss().then(() => {
                  this.filesService.downloadFiles();
                });
              });
            });
          });
        });
      }
    });
  }

  public async resetDataUser() {
    let tables = ['MarketingComments', 'TestsResults'];
    await this.storage.removeData(tables);
  }

  public async uploadNewComments() {
    let table = 'MarketingComments'
    await this.storage.getData(table).then(result => {
      if (result) {
        let newComm = result.filter(e => e['new']);
        if (newComm.length) this.uploadComments(newComm).subscribe(res => {
          if(Object.values(res).length) this.storage.getData(table).then(fromdb => {
            newComm.forEach((updt, index) => {
                let i = fromdb.findIndex(db => db.id == updt.id);
                updt['new'] = false;
                fromdb[i] = res[index];
            });
            this.storage.saveData(table, fromdb);
          });
        });
      }
    });
  }

  public async uploadNewTestResults() {
    let table = 'TestsResults'
    await this.storage.getData(table).then(result => {
      if (result) {
        let newRes = result.filter(e => e['new']);
        if (newRes.length) this.uploadResults(newRes).subscribe(res => {
          if(Object.values(res).length) this.storage.getData(table).then(fromdb => {
            newRes.forEach((updt, index) => {
                let i = fromdb.findIndex(db => db.id == updt.id);
                updt['new'] = false;
                fromdb[i] = res[index];
            });
            this.storage.saveData(table, fromdb);
          });
        });
      }
    });
  }

  public async uploadNewActivity() {
    let table = 'User';
    if (this.curentUser) {
      await this.storage.getDatabyID(table, this.curentUser.id).then(result => {
        if (result.activityPages) {
          let newRes: ActivityPages[] = result.activityPages;
          if (newRes.length) this.uploadActivity(newRes).subscribe(res => {
            if(res['id']) {
              result.activityPages = null;
              this.storage.updateData(table, this.curentUser.id, result);
            };
          });
        }
      });
    }
  }

  // showNotification() {
  //   this.localNotifications.hasPermission().then(granted => {
  //     if (granted) {
  //       this.localNotifications.schedule({
  //         title: 'FarmakRX2',
  //         text: 'Data synchronized',
  //         smallIcon: 'res://mipmap-hdpi/ic_launcher'
  //       });
  //     } else {
  //       this.localNotifications.requestPermission().then(res => {
  //         if (res) {
  //           this.localNotifications.schedule({
  //             title: 'FarmakRX2',
  //             text: 'Data synchronized',
  //             smallIcon: 'res://mipmap-hdpi/ic_launcher'
  //           });
  //         }
  //       })
  //     }
  //   });
  // }

  //? Gets requests
  getAllData() {
    let url = `${environment.apiUrl}/${entity}/all`;
    return this.http.get(url).pipe();
  }

  getUpdates(date: string = null) {
    let url = date ? `${environment.apiUrl}/${entity}/update?date=${date}` : `${environment.apiUrl}/${entity}/update`;
    return this.http.get(url).pipe(
      catchError(err => {
        console.log(err);
        return [false]
      })
    );
  }

  getUpdatesData(date: string = null) {
    let url = date ? `${environment.apiUrl}/${entity}/update/get?date=${date}` : `${environment.apiUrl}/${entity}/update/get`;
    return this.http.get(url).pipe();
  }

  getUpdateApp() {
    let version = localStorage.getItem('app_version');
    if (version) {
      let url = `${environment.apiUrl}/${entity}/update/version?version=${version}`;
      this.http.get(url).pipe(
        catchError(err => {
          localStorage.removeItem('new_app_version');
          return [false];
        })
      ).subscribe(res => {
        if (res['url']) {
          localStorage.setItem('new_app_version', res['url']);
        }
      });
    }
  }

  getApp() {
    this.showLoading('Завантаження додатка...').then((loader) => {
      loader.present();
      let link = localStorage.getItem('new_app_version');
      this.filesService.downloadFile(link).then(app => {
        loader.dismiss().then(() => {
          let filename = link.split('/').reverse()[0];
          this.filesService.openAPK(filename);
        });
      }).finally(() => {
        loader.dismiss();
      });
    });
  }

  //? Uploads
  uploadComments(data) {
    let url = `${environment.apiUrl}/${entity}/marketingComments`;
    let header = new HttpHeaders({ 'Content-Type': 'application/json' });

    let comments = {
      comments: data.map(c => {
        return {
            "comment": c.comment,
            "date": c.date,
            "user": c.user,
            "preparat": c.preparat.id
        }
      })
    };
    return this.http.post(url, comments, { headers: header }).pipe();
  }

  uploadResults(data) {
    let url = `${environment.apiUrl}/${entity}/testsResults`;
    let header = new HttpHeaders({ 'Content-Type': 'application/json' });
    let tests = {
      tests: data.map(c => {
        return {
            "study": c.study.id || c.test._id,
            "test": c.test.id || c.test._id,
            "user": c.user.id || c.test._id,
            "result": c.result,
            "time": c.time,
            "detailedStatistics": c.detailedStatistics
        }
      })
    };
    return this.http.post(url, tests, { headers: header }).pipe();
  }

  uploadActivity(data) {
    let url = `${environment.apiUrl}/${entity}/users/activityPages/${this.curentUser.id}`;
    let header = new HttpHeaders({ 'Content-Type': 'application/json' });
    let activities = {
      activities: data.map(actvt => {
        return {
            "id": actvt.id,
            "pageName": actvt.pageName,
            "dateStart": actvt.dateStart,
            "dateEnd": actvt.dateEnd,
            "duration": actvt.duration
        }
      })
    };
    return this.http.post(url, activities, { headers: header }).pipe();
  }

}
