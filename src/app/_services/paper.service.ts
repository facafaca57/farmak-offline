﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Paper } from '@app/_models';


const entity = 'papers';

@Injectable({ providedIn: 'root' })
export class PaperService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Paper[]>(`${environment.apiUrl}/${entity}`);
    }

    create(data: any) {
        return this.http.post<Paper>(`${environment.apiUrl}/${entity}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getById(id: string) {
        return this.http.get<Paper>(`${environment.apiUrl}/${entity}/${id}`);
    }

    update(data: Paper, id: string) {
        return this.http.put<Paper>(`${environment.apiUrl}/${entity}/${id}`, data);
    }
}
