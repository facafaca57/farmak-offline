﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import {User, Visit} from '@app/_models';
import { from } from 'rxjs';
import { StorageService } from './storage.service';


const fieldDB = 'Visit';
const entity = 'visits';

@Injectable({ providedIn: 'root' })
export class VisitService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<Visit[]>(`${environment.apiUrl}/${entity}`);
    }
    
    getById(id: number) {
        return from(this.storage.getDatabyID(fieldDB, id.toString()));
        // return this.http.get<Visit>(`${environment.apiUrl}/${entity}/${id}`);
    }

    update(data: Visit, id: number) {
        return this.http.put<Visit>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    create(data: Visit) {
        return this.http.post<Visit>(`${environment.apiUrl}/${entity}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
