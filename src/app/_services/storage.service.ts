import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private _storage: Storage | null = null;

  constructor(
    private storage: Storage
  ) {
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
  }

  public async getData(key: string) {
    if(!this._storage) await this.init();
    return await this._storage?.get(key);
  }

  public async getDatabyID(key: string, id: string) {
    if(!this._storage) await this.init();
    let data = await this._storage.get(key);
    let index = data.findIndex(item => item.id === id || item._id === id);
    return data[index];
  }

  public async getbyQuery(key: string, field: string, value: string) {
    if(!this._storage) await this.init();
    let data = await this._storage.get(key);
    let index = data.findIndex(item => item[field] === value);
    return data[index];
  }

  public async getDatabyQuery(key: string, field: string, value: string) {
    if(!this._storage) await this.init();
    let data = await this._storage.get(key);
    let result = data ? data.filter(item => item[field].id === value || item[field]._id === value) : [];
    return result;
  }

  public async saveData(key: string, value: any) {
    if (typeof value === 'string') {
      value = JSON.parse(value);
    }
    if(!this._storage) await this.init();
    return await this._storage?.set(key, value);
  }

  public async addData(key: string, data: any, find: { field: string, value: string, id: string }[] = null) {
    if(!this._storage) await this.init();
    if (find) {
      find.forEach(async fld => {
        let res = await this._storage.get(fld.field);
        let index = res.findIndex(item => item.id === fld.id || item._id === fld.id);
        if (index != -1) {
          data[fld.value] = res[index];
        }
      })
    }

    let generateID = uuid.v4().split('-').join('').slice(0, 24);
    data = Object.assign({ id: generateID, new: true }, data);

    await this.getData(key).then(async fromdb => {
      await this.saveData(key, fromdb ? fromdb.concat([data]) : [data]);
    });

    return data;
  }

  public async updateData(key: string, id: string, data: any) {
    if(!this._storage) await this.init();
    await this.getData(key).then(async fromdb => {
      let result = fromdb.find(el => el.id === id || el._id === id);
      if (result) result = Object.assign(result, data);
      data = result;
      await this.saveData(key, fromdb);
    });
    return data;
  }

  public async deleteData(key: string, id: string) {
    if(!this._storage) await this.init();
    await this.getData(key).then(async fromdb => {
      await this.saveData(key, fromdb.filter(el => el.id !== id || el._id === id));
    });
    return { status: 'ok' };
  }

  public async removeData(keys: string[]) {
    if(!this._storage) await this.init();
    keys.forEach(async key => {
      await this._storage.remove(key);
    });
    return { status: 'ok' };
  }

  public async getKeysFromDb() {
    if(!this._storage) await this.init();
    let keys = []
    await this._storage.forEach((value, key, _) => {
      keys.push({ key, length: value.length });
    });
    return keys;
  }


  // public saveData(key: string, value: string) {
  //   localStorage.setItem(key, value);
  // }

  // public getData(key: string) {
  //   return localStorage.getItem(key)
  // }

  // public removeData(key: string) {
  //   localStorage.removeItem(key);
  // }

  // public clearData() {
  //   localStorage.clear();
  // }
  
}
