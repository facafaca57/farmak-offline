import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { MarketingComments } from '@app/_models';
import { StorageService } from './storage.service';
import { from } from 'rxjs';

const fieldDB = 'MarketingComments';
const entity = 'marketingComments';

@Injectable({ providedIn: 'root' })
export class MarketingCommentsService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<MarketingComments[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<MarketingComments>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getByQuery(query: string) {
        let field = query.substring(0, query.indexOf("="));
        let value = query.substring(query.indexOf("=") + 1);
        return from(this.storage.getDatabyQuery(fieldDB, field, value)).pipe();
        // return this.http.get<MarketingComments[]>(`${environment.apiUrl}/${entity}/?${query}`);
    }

    create(data: MarketingComments) {
        let find = [{ 
            field: 'Preparats', 
            value: 'preparat', 
            id: data.preparat 
        }];
        return from(this.storage.addData(fieldDB, data, find));
        // return this.http.post<MarketingComments>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: any, id: string) {
        // return from(this.storage.updateData(fieldDB, id, {comment: data.comment}));
        return this.http.put<MarketingComments>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        // return from(this.storage.deleteData(fieldDB, id));
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
