import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Preparats } from '@app/_models';
import { from } from 'rxjs';
import { StorageService } from './storage.service';

const fieldDB = 'Preparats';
const entity = 'preparats';

@Injectable({ providedIn: 'root' })
export class PreparatsService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<Preparats[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<Preparats>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getByQuery(query: string) {
        return this.http.get<Preparats[]>(`${environment.apiUrl}/${entity}/?${query}`);
    }

    create(data: Preparats) {
        return this.http.post<Preparats>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: any, id: string) {
        return this.http.put<Preparats>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
