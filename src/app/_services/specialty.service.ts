﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Specialty } from '@app/_models';
import { tap } from 'rxjs/operators';
import { StorageService } from './storage.service';
import { from } from 'rxjs';


const fieldDB = 'Specialty';
const entity = 'specialties';

@Injectable({ providedIn: 'root' })
export class SpecialtyService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<Specialty[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<Specialty>(`${environment.apiUrl}/${entity}/${id}`);
    }

    create(data: any) {
        return this.http.post<Specialty>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: Specialty, id: string) {
        return this.http.put<Specialty>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
