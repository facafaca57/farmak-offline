import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { File } from '@ionic-native/file/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { FileOpener } from "@ionic-native/file-opener";
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { unzip, setOptions } from 'unzipit';
import bluebird from 'bluebird';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { ModalComponent } from '@app/shared/modal/modal.component';
import { Router } from '@angular/router';
// import { forEach } from 'async-foreach';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  private path: string = null;

  constructor(
    private router: Router,
    private file: File,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private https: HttpClient,
    private http: HTTP,
    private modalController: ModalController
  ) {
    setOptions({
      workerURL: 'https://unpkg.com/unzipit@0.1.9/dist/unzipit-worker.module.js',
      numWorkers: 2,
    });
    this.path = this.file.externalDataDirectory;
  }

  async showLoading(message: string = 'Please wait...') {
    return this.loadingCtrl.create({
      message,
      showBackdrop: true
    });
  }

  async presentAlert(header: string = 'Files', message: string = 'Please wait...', action: boolean = false) {
    const alert = await this.alertCtrl.create({
      header,
      message,
      buttons: action ? [
        { text: 'Cancel', role: 'cancel' },
        { text: 'OK', role: 'confirm' },
      ] : [{ text: 'OK', role: 'confirm' }],
    });

    await alert.present();
    const { role } = await alert.onDidDismiss();

    return role;
  }

  async downloadFilesZip(url = 'https://farmak-rx2.com/api/app/zip') { //'http://localhost:3000/api/app/zip'
    this.showLoading('Download files from server...').then(async (loader) => {
      loader.present();
      await this.http.sendRequest(url, { method: "get", responseType: "arraybuffer" }).then(
        async httpResponse => {
          console.log("File dowloaded: " + url);
          let filename= 'files.zip';
          await this.createFile(filename).then(async (_) => {
            await this.writeToFile(filename, httpResponse.data).then(asd => {
              loader.dismiss().then((_) => {
                this.presentAlert('Files!', 'Downloaded success! Wont to extract it?', true).then(res => {
                  if (res == 'confirm') {
                    this.extractFiles(httpResponse.data);
                  }
                });
              });
            });
          })
        }
      ).catch(err => {
        console.log('downloadFile');
        console.error(err);
      });
    });
  }

  async downloadFiles(url = 'https://farmak-rx2.com/api/app/files') {
    await this.showLoading('Перевірка файлів з сервера...').then((loader) => {
      loader.present();
      this.https.get(url, { responseType: 'json' }).pipe().subscribe(httpResponse => {
        loader.dismiss().then((_) => {
          this.checkFiles(httpResponse);
        });
      });
      // this.https.get(url, { responseType: 'arraybuffer' }).pipe().subscribe(httpResponse => {
      //   loader.dismiss().then((_) => this.extractFiles(httpResponse));
      // })
    });
  }

  readFile(filename) {
    return this.file.readAsDataURL(this.path, filename)
      .catch(e => this.presentAlert('Files manager', 'Error opening File'));
  }

  getFile(filename) {
    return this.file.resolveDirectoryUrl(this.path).then(entry => {
      return this.file.getFile(entry, filename, { create: false }).then(fl => {
        return Capacitor.convertFileSrc(fl.nativeURL);
      }).catch(e => this.presentAlert('Files manager', 'Error opening file'));
    }).catch(e => this.presentAlert('Files manager', 'Error opening path'));
  }

  async getFiles(filename) {
    return await Filesystem.requestPermissions().then(res => {
      return Filesystem.getUri({
        directory: Directory.Data,
        path: filename
      })
      .then(file => file.uri)
      .catch(e => this.presentAlert('Files manager', 'Error opening file'));
    })
  }

  openFile(filename) {
    return FileOpener.open(
      this.path + filename,
      "application/pdf"
    ).then(() => true)
     .catch(e => this.presentAlert('Files manager', 'Error opening file'));
  }

  openVideo(filename) {
    return FileOpener.open(
      this.path + filename,
      "video/mp4"
    ).then(() => true)
     .catch(e => this.presentAlert('Files manager', 'Error opening file'));
  }

  openPDF(fileName) {
    fileName = decodeURI(fileName);
    this.getFile(fileName).then(fileLink => {
      if (fileLink.endsWith('.pdf')) {
        this.modalController.create({
          component: ModalComponent,
          cssClass: 'modal-component',
          componentProps: {
              fileLink,
              fileName
          }
        }).then(mdl => mdl.present());
      }
    });
  }

  openAPK(filename) {
    return FileOpener.open(
      this.path + filename,
      'application/vnd.android.package-archive'
    ).then(() => true)
     .catch(e => this.presentAlert('Files manager', 'Error opening file'));
  }

  async extractFiles(data) {
    this.showLoading('Extracting files...').then((ldr) => {
      ldr.present();
      new Promise(async (resolve, reject) => {
        const { entries } = await unzip(data);
        Object.entries(entries).forEach(async ([name, entry], index) => {
          const arrayBuffer = await entry.arrayBuffer();
          await this.createFile(name).then(async (_) => {
            await this.writeToFile(name, arrayBuffer);
          });
          if (index + 1 == Object.entries(entries).length) {
            ldr.dismiss().then((_) => {
              resolve(true);
              this.presentAlert('Files!', 'Extracting success!');
            });
          }
        });
      })
    });
  }

  async createFile(filename) {
    return this.file.createFile(this.path, filename, false).catch(err => {
      console.error(err);
    });
  }

  writeToFile(filename, downloadedFile) {
    return this.file.writeFile(
      this.path, filename,
      downloadedFile, { replace: true, append: false }
    ).catch(err => {
      console.error(err);
    });
  }

  checkFiles(files) {
    this.showLoading('Завантаження файлів...').then((ldr) => {
      ldr.present().then(async _ => {
        let index = 1;
        await this.file.listDir(this.path, '').then(async list => {
          let lists = list.map(entry => entry.name);
          files = files.filter(item => !lists.includes(item.split('/').reverse()[0]));
          await bluebird.map(files, async (url) => {
            // let filename = url.split('/').reverse()[0];
            // await this.file.checkFile(this.path, filename).then(async stat => {
            //   ldr.message = ((index / files.length) * 100).toFixed(0) + '% Files check';
            // }).catch(async (_) => {
              ldr.message = ((index / files.length) * 100).toFixed(0) + '% Завантаження';
              index++;
              return await this.downloadFile(url);
            // });
            // index++;
          }, { concurrency: 5 }).then(result => {
            ldr.dismiss();
            // this.presentAlert('Оновлення!', 'Завантаження завершено!').then(res => {
              // if (res == 'confirm') {
              if (this.router.url == '/update') {
                location.replace('/');
              }
              // }
            // });
            // this.presentAlert('Files manager', 'Downloaded success!');
          }).catch(err => {
            ldr.dismiss();
            this.presentAlert('Files manager', 'Download error!');
          });
        }).catch(err => {
          ldr.dismiss();
          this.presentAlert('Files manager', 'Download error!');
        });

        // for (let item in files) {
        //   // await this.processItem(files[parseInt(item)]);
        //   let url = files[item];
        //   let i = parseInt(item)
        //   let filename = url.split('/').reverse()[0];
        //   await this.file.checkFile(this.path, filename).then(async stat => {
        //     console.log('Fille already exist! - ' + filename);
        //   }).catch(async (_) => {
        //     ldr.message = 'Download files: ' + (i+1) + ' of ' + files.length;
        //     return await this.downloadFile(url);
        //   });
        //   if (i+1 == files.length) ldr.dismiss();
        // }
      });
    });
  }

  async downloadFile(url) {
    // For direct download
    let filename = decodeURI(url.split('/').reverse()[0]).split(' ').join('%20');
    return await this.http.downloadFile(url, {}, {}, this.path + filename).catch(err => {
      console.error(err);
    });

    // For create and frite data at file
    // return await this.http.sendRequest(url, { method: "get", responseType: "blob" }).then(
    //   async httpResponse => {
    //     console.log("File dowloaded: " + url);
    //     let filename = url.split('/').reverse()[0];
    //     let downloadedFile = new Blob([httpResponse.data], { type: getImageType(filename) });
    //     return await this.createFile(filename).then(async (_) => {
    //       return await this.writeToFile(filename, downloadedFile);
    //     });
    //   }
    // ).catch(err => {
    //   console.error(err);
    // });
  }
}
