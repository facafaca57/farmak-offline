﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Explanations } from '@app/_models';

const entity = 'explanations';

@Injectable({ providedIn: 'root' })
export class ExplanationsService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Explanations[]>(`${environment.apiUrl}/${entity}`);
    }

    create(data: any, id?: any) {
        if (id) return this.update(data, id);
        return this.http.post<Explanations>(`${environment.apiUrl}/${entity}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getById(id: string) {
        return this.http.get<Explanations>(`${environment.apiUrl}/${entity}/${id}`);
    }

    update(data: Explanations, id: string) {
        return this.http.put<Explanations>(`${environment.apiUrl}/${entity}/${id}`, data);
    }
}
