import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';


@Injectable({
  providedIn: 'root'
})

export class SocketService {
    private url = environment.socketUrl;
    private socket;

    constructor() {
        this.socket = io(this.url);
    }

    public sendMessage(user, message) {
        this.socket.emit('message', {user, message});
    }

    public getMessage(): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.on('new message', (data: any) => observer.next(data.reverse()));
        });
    }

    public newUser(user) {
        this.socket.emit('new user', user);
    }

    public getUsers(): Observable<any[]> {
        return new Observable<any[]>(observer => {
            this.socket.on('get users', (data: any[]) => observer.next(data));
        });
    }

    public disconnect(userId: string | number) {
        this.socket.emit('disconnect', userId);
    }
}
