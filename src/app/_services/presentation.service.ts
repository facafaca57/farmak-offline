﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { Presentation } from '@app/_models';
import { StorageService } from './storage.service';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';


const fieldDB = 'Presentation';
const entity = 'presentations';

@Injectable({ providedIn: 'root' })
export class PresentationService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return from(this.storage.getData(fieldDB));
        // return this.http.get<Presentation[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return from(this.storage.getDatabyID(fieldDB, id));
        // return this.http.get<Presentation>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getByQuery(query: string) {
        let field = query.substring(0, query.indexOf("="));
        let value = query.substring(query.indexOf("=") + 1);
        return from(this.storage.getDatabyQuery(fieldDB, field, value)).pipe(map(res => 
            res.map(el => 
                Object.assign(el, { specialty: el.specialty.specialtyName, visit: el.visit.serialNumber }))
        ));
        // return this.http.get<Presentation[]>(`${environment.apiUrl}/${entity}/?${query}`);
    }

    create(data: Presentation) {
        return this.http.post<Presentation>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: any, id: string) {
        return this.http.put<Presentation>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
