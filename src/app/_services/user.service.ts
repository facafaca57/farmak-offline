﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { User, Activity, ActivityPages } from '@app/_models';
import { StorageService } from './storage.service';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

const fieldDB = 'User';
const entity = 'users';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(
        private http: HttpClient,
        private storage: StorageService
    ) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string | number) {
        return from(this.storage.getDatabyID(fieldDB, id.toString()));
        // return this.http.get<User>(`${environment.apiUrl}/${entity}/${id}`);
    }

    create(data: User) {
        return this.http.post<User>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: User, id: string) {
        return this.http.put<User>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }

    activity(data: Activity, id: string) {
        return this.http.post<User>(`${environment.apiUrl}/${entity}/activity/${id}`, data);
    }

    startActivityPages(data: ActivityPages[], id: string) {
        let activity = { activityPages: data };
        return from(this.storage.updateData(fieldDB, id, activity));
        // return this.http.post<User>(`${environment.apiUrl}/${entity}/startActivityPages/${id}`, data);
    }

    activityPages(data: ActivityPages[], id: string) {
        let activity = { activityPages: data };
        return from(this.storage.updateData(fieldDB, id, activity));
        // return this.http.post<User>(`${environment.apiUrl}/${entity}/activityPages/${id}`, data);
    }

    updateResultsImg(data: any, id: string) {
        return this.http.put<any>(`${environment.apiUrl}/${entity}/resultsImg/${id}`, data);
    }

    login(login: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/login`, { login, password }).pipe(map((user) => {
            user.activityPages = null;
            this.storage.addData(fieldDB, user);
            return user;
        }));
    }

    logout(id: string) {
        return this.http.post(`${environment.apiUrl}/logout`, {}).pipe(map(async (user) => {
            let tables = ['MarketingComments', 'TestsResults'];
            await this.storage.deleteData(fieldDB, id);
            await this.storage.removeData(tables);
            return user;
        }));
    }
}
