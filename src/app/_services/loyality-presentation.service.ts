import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { LoyalityPresentation } from '@app/_models';


const entity = 'loyaltyPresentation';

@Injectable({ providedIn: 'root' })
export class LoyalityPresentationService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<LoyalityPresentation[]>(`${environment.apiUrl}/${entity}`);
    }

    getById(id: string) {
        return this.http.get<LoyalityPresentation>(`${environment.apiUrl}/${entity}/${id}`);
    }

    getByQuery(query: string) {
        return this.http.get<LoyalityPresentation[]>(`${environment.apiUrl}/${entity}/?${query}`);
    }

    create(data: LoyalityPresentation) {
        return this.http.post<LoyalityPresentation>(`${environment.apiUrl}/${entity}`, data);
    }

    update(data: any, id: string) {
        return this.http.put<LoyalityPresentation>(`${environment.apiUrl}/${entity}/${id}`, data);
    }

    delete(id: string) {
        return this.http.delete<any>(`${environment.apiUrl}/${entity}/${id}`);
    }
}
