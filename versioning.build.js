var fs = require('fs');
var path = require('path');
var replaceFile = require('replace-in-file');
var package = require('./package.json');
var angular = require('./angular.json');
var buildVersion = package.version;
var buildPath = '/';
var defaultProject = angular.defaultProject;
var appendUrl = '?v=' + buildVersion;

const getNestedObject = (nestedObj, pathArr) => {
    return pathArr.reduce((obj, key) =>
    (obj && obj[key] !== 'undefined') ? obj[key] : undefined, nestedObj);
}

const relativePath = getNestedObject(angular, ['projects',defaultProject,'architect','build','options','outputPath']); // to identify relative build path when angular make build
buildPath += relativePath; //.replace(/[/]/g, '\\');
console.log(relativePath);
var indexPath = __dirname + buildPath + '/' + 'index.html';


console.log('Angular build path:', __dirname, buildPath);
console.log('Change by buildVersion:', buildVersion);

fs.readdir(__dirname + buildPath, (err, files) => {

    files.forEach(file => {
        if (file.match(/^(main-es5|main-es2015|polyfills-es2015|polyfills-es5|runtime-es5|runtime-es2015|es2015-polyfills|main|polyfills|runtime|scripts|styles)+.+([a-z0–9.\-])*(js|css)$/g)) { // regex is identified by build files generated
            console.log('Current Filename:',file);
            const currentPath = file;
            const changePath = file + appendUrl;
            changeIndex(currentPath, changePath);
        }
    });
    const optionsManifest = {
        files: indexPath,
        from: '<base href="./">',
        to: '<base href="https://farmak-rx2.com/">',
        allowEmptyPaths: false,
    };
    try {
        let changeManifest = replaceFile.sync(optionsManifest);
        if (changeManifest == 0) {
            console.log('Changed base href');
        } else if (changeManifest[0].hasChanged === false) {
            console.log('Index File already updated');
        }
    }
    catch (error) {
        console.error('Error occurred:', error);
        throw error
    }
});
function changeIndex(currentfilename, changedfilename) {
    const options = {
        files: indexPath,
        from: "" + currentfilename + "",
        to: "" + changedfilename + "",
        allowEmptyPaths: false,
    };

    try {
        let changedFiles = replaceFile.sync(options);
        if (changedFiles == 0) {

        } else if (changedFiles[0].hasChanged === false) {
            console.log('File already updated');
        }
        console.log('Changed Filename:',changedfilename);
    }
    catch (error) {
        console.error('Error occurred:', error);
        throw error
    }
}
